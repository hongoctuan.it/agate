<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'main';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//default

$route['tim-kiem'] = 'main/search';
$route['chuong-trinh'] = 'default/class_agate';
$route['doi-ngu'] = 'default/staff_agate';
$route['agate-gift'] = 'default/gift_agate';
$route['agate-gift-detail/(:any)'] = 'default/gift_agate/detail/$1';
$route['dang-ky-thong-tin'] = 'default/reg_info';
$route['agate-mag/rating'] = 'default/mag_agate/rating';
$route['agate-mag-detail/(:any)'] = 'default/mag_agate/detail/$1';
$route['agate-mag/(:any)'] = 'default/mag_agate/category/$1';
$route['search'] = 'default/mag_agate/search';
$route['agate-mag-comment'] = 'default/mag_agate/comment';
$route['agate-savemag'] = 'default/mag_agate/savemag';
$route['agate-unsavemag'] = 'default/mag_agate/unsavemag';

$route['dang-nhap'] = 'default/account';
$route['account-info/(:any)'] = 'default/account/info/$1';
$route['update-info'] = 'default/account/update';
$route['update-password'] = 'default/account/updatepassword';

$route['add-post'] = 'default/account/addpost';
$route['dang-xuat'] = 'default/account/logout';
$route['dang-ky'] = 'default/account/register';
$route['quen-mat-khau'] = 'default/account/forgot';
$route['hashtag/(:any)'] = 'default/mag_agate/hashtag/$1';
$route['tracking'] = 'default/tracking';
$route['prelaunching'] = 'main/ladipage';
$route['communityparents'] = 'main/redirectfb';
$route['teencommunity'] = 'main/teencommunity';
$route['account-update-banner'] = 'default/account/update_info_banner';
$route['account-update-avatar'] = 'default/account/update_info_avatar';
$route['info-review'] = 'default/account/inforeview';
$route['info_process'] = 'default/account/processview';






//admin
$route['admin'] = 'cms/product';
$route['login'] = 'auth';
$route['logout'] = 'auth/logout';
$route['admin/(:any)'] = 'cms/$1';
$route['(:any)'] = 'main/redirect/$1';