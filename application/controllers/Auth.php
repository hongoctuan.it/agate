<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('obisys');
		$this->load->model('M_myweb');
	}
	
	public function index()
	{
		if($this->input->post()){
			$this->login();
		}
		$this->load->view('cms/auth/home');
	}

	public function logout(){
		$_SESSION['system_msg'] = messageDialog('div', 'success', 'Đã đăng xuất khỏi hệ thống');
		unset($_SESSION['system']);
		return redirect(site_url("login"));
	}

	public function login(){
		$data = $this->input->post();

		$get = $this->M_myweb->set('email',$data['email'])->set('password',hashpass($data['password']))->set_table('user')->get();
		echo hashpass($data['password']);
		if($get){
			$session = array(
				'id'			=> $get->id,
				'logged_in'		=>	true,
				'img'			=>	$get->thumbnail,
				'img2'			=>	$get->banner,
				'phone'			=>	$get->phone,
				'name'			=>	$get->full_name,
				'position'		=>	$get->position,
				'email'			=>	$get->email,
				'role'			=>	$get->role,
				'logid'			=>	$get->id,
				'token'			=>	randomString(30)
			);
			$_SESSION['system'] = (object)$session;
			return redirect(site_url('admin/home'));
		}else{
			$_SESSION['system_msg'] = messageDialog('div', 'error', 'Tên đăng nhập hoặc tài khoản không chính xác');
			return redirect(site_url("login"));
		}
	}
}