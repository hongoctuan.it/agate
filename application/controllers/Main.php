<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->Model = $this->M_myweb->set_table('home');
		$this->load->model('default/m_mag');

	}
	public function index()
	{
		$this->data['home'] = $this->Model->sets(array('deleted' => 0, 'active' => 1))->gets();
		$this->data['staff_01'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['home'][19]->value))->get();
		$this->data['staff_02'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['home'][20]->value))->get();
		$this->data['staff_03'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['home'][21]->value))->get();
		
		$this->data['mag_01'] = $this->m_mag->getMagId($this->data['home'][22]->value);
		$this->data['mag_02'] = $this->m_mag->getMagId($this->data['home'][23]->value);

		$this->data['class'] = $this->M_myweb->set_table('class')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['home'][27]->value))->get();
		$this->data['partner_01'] = $this->M_myweb->set_table('partner')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['home'][25]->value))->get();
		$this->data['partner_02'] = $this->M_myweb->set_table('partner')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['home'][26]->value))->get();
		$this->data['gift'] = $this->M_myweb->set_table('gift')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['home'][24]->value))->get();
		$this->data['classimg'] = $this->M_myweb->set_table('class_img')->sets(array('deleted' => 0, 'home'=> 0))->set_orderby('id','desc')->gets();
		
		$this->data['comment_title'] = $this->data['home'][50]->value;
		$this->data['comment_description'] = $this->data['home'][51]->value;

		$this->data['comment_01'] = $this->M_myweb->set_table('comment')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['home'][35]->value))->get();
		$this->data['comment_02'] = $this->M_myweb->set_table('comment')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['home'][36]->value))->get();
		$this->data['comment_03'] = $this->M_myweb->set_table('comment')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['home'][49]->value))->get();

		$this->data['comment_user_01'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['comment_01']->user_id))->get();
		$this->data['comment_user_02'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['comment_02']->user_id))->get();
		$this->data['comment_user_03'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['comment_03']->user_id))->get();


		$this->data['title']	= "Trang Chủ";
		$this->data['subview'] 	= 'default/index/V_index';
		$this->load->view('default/_main_page', $this->data);		
	}

	public function ladipage()
	{
		$this->data['subview'] 	= 'default/index/V_index';
		$this->load->view('default/ladipage/ladipage');
		
	}
    public function redirectfb()
	{
		redirect("https://www.facebook.com/groups/3044588509159433");
		
	}
	
	public function redirect($slug)
	{
		$data = $this->M_myweb->set_table('red_up')->set('short_url', $slug)->get();
		if($data->type == "1"){
			redirect(site_url($data->file_name));
		}else{
			redirect($data->url);
		}	
	}
	
	public function teencommunity()
	{
		redirect("https://www.facebook.com/groups/429041988732557");
		
	}

}

