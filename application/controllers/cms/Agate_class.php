<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Agate_class extends CMS_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->Model = $this->M_myweb->set_table('class_config');
		$this->load->model('cms/m_class');
	}

	public function index()
	{
		switch ($this->act) {
			case "upd_banner":
				$this->class_banner();
				break;
			case "upd_benefit":
				$this->class_benefit();
				break;
			case "upd_comment":
				$this->class_comment();
				break;
			case "upd_basic":
				$this->class_basic();
				break;	
			case "upd_baseline":
				$this->class_baseline();
				break;	
			case "upd_classinfo":
				$this->class_classinfo();
				break;	
			case "upd_removeimg":
				$this->class_removeimg();
				break;	
			case "upd_addimg":
				$this->class_addimg();
				break;		
			default:
				$this->class();
				break;
		}
	}

	private function class(){
		$this->data['class'] = $this->Model->gets();
		$this->data['comments'] = $this->M_myweb->set_table('comment')->sets(array('deleted' => 0, 'active' => 1))->gets();
		$this->data['comment_01'] = $this->M_myweb->set_table('comment')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['class'][10]->value))->get();
		$this->data['comment_02'] = $this->M_myweb->set_table('comment')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['class'][11]->value))->get();
		$this->data['comment_03'] = $this->M_myweb->set_table('comment')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['class'][28]->value))->get();

		$this->data['comment_user_01'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['comment_01']->user_id))->get();
		$this->data['comment_user_02'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['comment_02']->user_id))->get();
		$this->data['comment_user_03'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['comment_03']->user_id))->get();

		$this->data['classimg'] = $this->M_myweb->set_table('class_img')->sets(array('deleted' => 0, 'class_id'=> 1))->set_orderby('id','desc')->gets();
		$this->data['subview'] = 'cms/class/V_index';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function class_banner()
	{
		$data = $this->input->post();
		$temp['value'] = $data['banner_title'];
		$this->Model->sets($temp)->setPrimary(1)->save();
		$temp['value'] = $data['banner_description'];
		$this->Model->sets($temp)->setPrimary(2)->save();
		if(isset($_FILES['banner_img']['name']) && $_FILES['banner_img']['name']!=""){
			$image = do_upload('avatar','banner_img');	
			$temp['value'] = $image;	
			$this->Model->sets($temp)->setPrimary(33)->save();			
		}
		redirect(site_url('admin/agate_class'));
	}

	private function class_benefit()
	{
		$data = $this->input->post();
		$temp['value'] = $data['benefit_item_name_01'];
		$this->Model->sets($temp)->setPrimary(3)->save();
		$temp['value'] = $data['benefit_item_name_02'];
		$this->Model->sets($temp)->setPrimary(4)->save();
		$temp['value'] = $data['benefit_item_name_03'];
		$this->Model->sets($temp)->setPrimary(5)->save();
		$temp['value'] = $data['benefit_item_name_04'];
		$this->Model->sets($temp)->setPrimary(6)->save();
		if(isset($_FILES['benefit_item_img_01']['name']) && $_FILES['benefit_item_img_01']['name']!=""){
			$image_01 = do_upload('avatar','benefit_item_img_01');	
			$temp['value'] = $image_01;
			$this->Model->sets($temp)->setPrimary(7)->save();			
		}
		if(isset($_FILES['benefit_item_img_02']['name']) && $_FILES['benefit_item_img_02']['name']!=""){
			$image_02 = do_upload('avatar','benefit_item_img_02');	
			$temp['value'] = $image_02;
			$this->Model->sets($temp)->setPrimary(8)->save();			
		}
		if(isset($_FILES['benefit_item_img_03']['name']) && $_FILES['benefit_item_img_03']['name']!=""){
			$image_03 = do_upload('avatar','benefit_item_img_03');	
			$temp['value'] = $image_03;
			$this->Model->sets($temp)->setPrimary(9)->save();				
		}
		if(isset($_FILES['benefit_item_img_04']['name']) && $_FILES['benefit_item_img_04']['name']!=""){
			$image_04 = do_upload('avatar','benefit_item_img_04');	
			$temp['value'] = $image_04;
			$this->Model->sets($temp)->setPrimary(10)->save();				
		}
		$temp['value'] = $data['benefit_title'];
		$this->Model->sets($temp)->setPrimary(34)->save();
		$temp['value'] = $data['benefit_des_01'];
		$this->Model->sets($temp)->setPrimary(35)->save();
		$temp['value'] = $data['benefit_des_02'];
		$this->Model->sets($temp)->setPrimary(36)->save();
		$temp['value'] = $data['benefit_des_03'];
		$this->Model->sets($temp)->setPrimary(37)->save();
		$temp['value'] = $data['benefit_des_04'];
		$this->Model->sets($temp)->setPrimary(38)->save();
		print_r('Cập nhật thành công');
	}

	private function class_comment()
	{
		$data = $this->input->post();
		$banner_title['value'] = $data['comment_title'];
		$this->Model->sets($banner_title)->setPrimary(39)->save();
		$temp['value'] = $data['comment_description'];
		$this->Model->sets($temp)->setPrimary(40)->save();
		$temp['value'] = $data['comment_list'][0];
		$this->Model->sets($temp)->setPrimary(11)->save();
		$temp['value'] = $data['comment_list'][1];
		$this->Model->sets($temp)->setPrimary(12)->save();
		$temp['value'] = $data['comment_list'][2];
		$this->Model->sets($temp)->setPrimary(29)->save();
		print_r('Cập nhật thành công');
	}

	private function class_basic()
	{
		$data = $this->input->post();
		$temp['value'] = $data['basic_name_01'];
		$this->Model->sets($temp)->setPrimary(13)->save();
		$temp['value'] = $data['basic_name_02'];
		$this->Model->sets($temp)->setPrimary(14)->save();
		$temp['value'] = $data['basic_name_03'];
		$this->Model->sets($temp)->setPrimary(15)->save();
		$temp['value'] = $data['basic_name_04'];
		$this->Model->sets($temp)->setPrimary(30)->save();
		$temp['value'] = $data['basic_description_01'];
		$this->Model->sets($temp)->setPrimary(16)->save();
		$temp['value'] = $data['basic_description_02'];
		$this->Model->sets($temp)->setPrimary(17)->save();
		$temp['value'] = $data['basic_description_03'];
		$this->Model->sets($temp)->setPrimary(18)->save();
		$temp['value'] = $data['basic_description_04'];
		$this->Model->sets($temp)->setPrimary(31)->save();
		$temp['value'] = $data['basic_title'];
		$this->Model->sets($temp)->setPrimary(41)->save();
		$temp['value'] = $data['basic_des'];
		$this->Model->sets($temp)->setPrimary(42)->save();
		print_r('Cập nhật thành công');
	}

	private function class_classinfo()
	{
		$data = $this->input->post();
		$temp['value'] = $data['classinfo_title'];
		$this->Model->sets($temp)->setPrimary(22)->save();
		$temp['value'] = $data['classinfo_description'];
		$this->Model->sets($temp)->setPrimary(24)->save();
		$temp['value'] = $data['classinfo_description_mb'];
		$this->Model->sets($temp)->setPrimary(23)->save();
		redirect(site_url('admin/agate_class#classinfo'));
	}

	private function class_baseline()
	{
		$data = $this->input->post();
		$temp['value'] = $data['baseline_item_01'];
		$this->Model->sets($temp)->setPrimary(25)->save();
		$temp['value'] = $data['baseline_item_02'];
		$this->Model->sets($temp)->setPrimary(26)->save();
		$temp['value'] = $data['baseline_item_03'];
		$this->Model->sets($temp)->setPrimary(27)->save();
		if(isset($_FILES['baseline_img_pc']) && $_FILES['baseline_img_pc']['name']!=""){
			$image = do_upload('avatar','baseline_img_pc');	
			$temp['value'] = $image;		
			$this->Model->sets($temp)->setPrimary(28)->save();	
		}
		if(isset($_FILES['baseline_img_mb']) && $_FILES['baseline_img_mb']['name']!=""){
			$image = do_upload('avatar','baseline_img_mb');	
			$temp['value'] = $image;			
			$this->Model->sets($temp)->setPrimary(43)->save();		
		}
		print_r('Cập nhật thành công');
	}

	private function class_removeimg()
	{
		$data = $this->input->post();
		$temp['deleted'] = 1;
		$this->M_myweb->set_table('class_img')->sets($temp)->setPrimary($data['id'])->save();
		print_r('Cập nhật thành công');
	}

	private function class_addimg()
	{
		$data = $this->input->post();
		if($_FILES['classimglist_img']['name']!=""){
			$image = do_upload('avatar','classimglist_img');	
			$data['img'] = $image;				
		}
		$data['class_id']=1;
		$this->M_myweb->set_table('class_img')->sets($data)->save();
		print_r('Cập nhật thành công');
	}

	

}
