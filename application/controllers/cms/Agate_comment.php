<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Agate_comment extends CMS_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->Model = $this->M_myweb->set_table('comment');
		$this->load->model('cms/m_comment');

	}

	public function index()
	{
		switch ($this->act) {
			case "upd":
				$this->update();
				break;
			case "add":
				$this->add();
				break;
			case "lock":
				$this->locked();
				break;
			case "unlock":
				$this->unlocked();
				break;
			case "edit":
				$this->edit();
				break;
			case "del":
				$this->delete();
				break;
			default:
				$this->comment();
				break;
		}
	}

	private function comment(){
		$this->data['comments'] = $this->m_comment->getComment();
		$this->data['subview'] = 'cms/comment/commentlist';
		$this->load->view('cms/_main_page',$this->data);
		
	}

	private function add(){
		$data = $this->input->post();
		if(!empty($data['id'])){
			$this->data['comment'] = $this->m_comment->getCommentId($data['id']);
		}
		$this->data['users'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1))->gets();
		$this->data['subview'] = 'cms/comment/createcomment';
		$this->load->view('cms/_main_page',$this->data);
	}


	private function edit(){
		$data = $this->input->get('id');
		if(!empty($data['id'])){
			$this->data['comment'] = $this->m_comment->getCommentId($data)[0];
		}else{
			$this->data['comment'] = $this->m_comment->getCommentId($data)[0];
		}
		$this->data['users'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1))->gets();
		$this->data['subview'] = 'cms/comment/updatecomment';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function update(){
		$data = $this->input->post();
		if(!empty($data['id'])){
			$data['time']=time();
			if(isset($_FILES['comment_img_01']) && $_FILES['comment_img_01']['name']!=""){
				$image = do_upload('avatar','comment_img_01');	
				$data['img'] = $image;	
			}
			$this->Model->sets($data)->setPrimary($data['id'])->save();
		}else{
			$data['time']=time();
			if(isset($_FILES['comment_img_01']) && $_FILES['comment_img_01']['name']!=""){
				$image = do_upload('avatar','comment_img_01');	
				$data['img'] = $image;	
			}
			$this->Model->sets($data)->save();
		}
		redirect(site_url('admin/agate_comment'));
	}

	private function locked(){
		$data = $this->input->get('id');
		$temp['active'] = 0;
		$this->Model->sets($temp)->setPrimary($data)->save();
		redirect(site_url('admin/agate_comment'));
	}

	private function unlocked(){
		$data = $this->input->get('id');
		$temp['active'] = 1;
		$this->Model->sets($temp)->setPrimary($data)->save();
		redirect(site_url('admin/agate_comment'));
	}

	private function delete(){
		$data = $this->input->get('id');
		$temp['deleted'] = 1;
		$this->Model->sets($temp)->setPrimary($data)->save();
		redirect(site_url('admin/agate_comment'));
	}
}
