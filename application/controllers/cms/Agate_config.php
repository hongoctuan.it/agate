<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Agate_config extends CMS_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->Model = $this->M_myweb->set_table('config');
	}

	public function index()
	{
		switch ($this->act) {
			case "upd":
				$this->update();
				break;
			default:
				$this->config();
				break;
		}
	}

	private function config(){
		$this->data['config'] = $this->Model->gets();
		$this->data['subview'] = 'cms/config/V_index';
		$this->load->view('cms/_main_page',$this->data);
		
	}

	private function update(){
		$data = $this->input->post();
// 		$temp['value'] = $data['email'];
// 		$this->Model->sets($temp)->setPrimary(2)->save();
// 		$temp['value'] = $data['password'];
// 		$this->Model->sets($temp)->setPrimary(3)->save();
		$temp['value'] = $data['email_recive'];
		$this->Model->sets($temp)->setPrimary(5)->save();
		$temp['value'] = $data['description'];
		$this->Model->sets($temp)->setPrimary(1)->save();
		$temp['value'] = $data['description_email_send'];
		$this->Model->sets($temp)->setPrimary(6)->save();
		$temp['value'] = $data['title_email_send'];
		$this->Model->sets($temp)->setPrimary(7)->save();
		$temp['value'] = $data['description_email_send_ph'];
		$this->Model->sets($temp)->setPrimary(8)->save();
		$temp['value'] = $data['description_email_send_hs'];
		$this->Model->sets($temp)->setPrimary(9)->save();
		


		if(isset($_FILES['config_img']) && $_FILES['config_img']['name']!=""){
			$image_01 = do_upload('avatar','config_img');	
			$temp['value'] = $image_01;		
			$this->Model->sets($temp)->setPrimary(4)->save();		
		}
		$this->data['config'] = $this->Model->gets();
		$this->data['subview'] = 'cms/config/V_index';
		$this->load->view('cms/_main_page',$this->data);
		
	}

}
