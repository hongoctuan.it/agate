<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Agate_gift extends CMS_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->Model = $this->M_myweb->set_table('gift');
		$this->load->model('cms/m_gift');
	}

	public function index()
	{
		switch ($this->act) {
			case "upd_addgift":
				$this->gift_addgift();
				break;
			case "upd_removegift":
				$this->gift_removegift();
				break;
			case "upd_getgift":
				$this->gift_getgift();
				break;
			case "upd_updategift":
				$this->gift_updategift();
				break;
			default:
				$this->gift();
				break;
		}
	}

	private function gift(){
		$this->data['gitflist'] = $this->Model->sets(array('deleted' => 0, 'active' => 1))->gets();
		$this->data['subview'] = 'cms/gift/V_index';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function gift_addgift()
	{
		$temp = $this->input->post();
		$data['type'] = $temp['gift_type'];
		$data['title'] = $temp['gift_title'];
		$data['shopee_link'] = $temp['shopee_link'];
		$data['description'] = $temp['gift_description'];
		$data['slug'] = str_replace(array( '\'', '"',',' , ';', '<', '>', '?', '&',' '),"-",stripUnicode($temp['gift_title']));
		if(isset($_FILES['gift_img_01']) && $_FILES['gift_img_01']['name']!=""){
			$image_01 = do_upload('avatar','gift_img_01');	
			$data['img_01'] = $image_01;				
		}
		if(isset($_FILES['gift_img_02']) && $_FILES['gift_img_02']['name']!=""){
			$image_01 = do_upload('avatar','gift_img_02');	
			$data['img_02'] = $image_01;				
		}
		if(isset($_FILES['gift_img_03']) && $_FILES['gift_img_03']['name']!=""){
			$image_01 = do_upload('avatar','gift_img_03');	
			$data['img_03'] = $image_01;				
		}
		if(isset($_FILES['gift_img_04']) && $_FILES['gift_img_04']['name']!=""){
			$image_01 = do_upload('avatar','gift_img_04');	
			$data['img_04'] = $image_01;				
		}
		if(empty($temp['id'])){
			$this->Model->sets($data)->save();
		}else{
			$this->M_myweb->set_table('gift')->sets($data)->setPrimary($temp['id'])->save();
		} 
		redirect(site_url("admin/agate_gift"));
	}

	private function gift_removegift()
	{
		$data = $this->input->post();
		$temp['deleted'] = 1;
		$this->M_myweb->set_table('gift')->sets($temp)->setPrimary($data['id'])->save();
		print_r('Cập nhật thành công');
	}

	private function gift_getgift(){
		$id = $this->input->get('id');
		if(!empty($id)){
			$this->data['gift'] = $this->Model->sets(array('deleted' => 0, 'active' => 1, 'id' => $id))->get();
		}
		$this->data['subview'] = 'cms/gift/creategift';
		$this->load->view('cms/_main_page',$this->data);
	}

	
	private function gift_updategift()
	{
		$temp = $this->input->post();
		$data['type'] = $temp['gift_type'];
		$data['title'] = $temp['gift_title'];
		$data['description'] = $temp['gift_description'];
		if(isset($_FILES['gift_img_01']) && $_FILES['gift_img_01']['name']!=""){
			$image_01 = do_upload('avatar','gift_img_01');	
			$data['img_01'] = $image_01;				
		}
		if(isset($_FILES['gift_img_02']) && $_FILES['gift_img_02']['name']!=""){
			$image_01 = do_upload('avatar','gift_img_02');	
			$data['img_02'] = $image_01;				
		}
		if(isset($_FILES['gift_img_03']) && $_FILES['gift_img_03']['name']!=""){
			$image_01 = do_upload('avatar','gift_img_03');	
			$data['img_03'] = $image_01;				
		}
		if(isset($_FILES['gift_img_04']) && $_FILES['gift_img_04']['name']!=""){
			$image_01 = do_upload('avatar','gift_img_04');	
			$data['img_04'] = $image_01;				
		}
		$this->Model->sets($data)->setPrimary($temp['id'])->save();
		$result = $this->Model->sets(array('deleted' => 0, 'active' => 1, 'id'=>$temp['id']))->get();
		print_r($result->img_01);
	}

}
