<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH."/third_party/PHPExcel.php";
require_once APPPATH."/third_party/PHPExcel/IOFactory.php";


class Agate_regis extends CMS_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->Model = $this->M_myweb->set_table('reg_info');
	}

	public function index()
	{
		switch ($this->act) {
			case "upd_addgift":
				$this->gift_addgift();
				break;
			case "upd_removegift":
				$this->gift_removegift();
				break;
			case "upd_getgift":
				$this->gift_getgift();
				break;
			case "upd_updategift":
				$this->gift_updategift();
				break;
			case "del":
				$this->delete_regis();
				break;
			case "detail":
				$this->regisDetail();
				break;
			case "update":
				$this->regisUpdate();
				break;
			case "export":
				$this->regisExport();
				break;
			default:
				$this->regisInfo();
				break;
		}
	}

	private function regisInfo(){
		$this->data['regisinfo'] = $this->Model->sets(array('deleted' => 0, 'active' => 1))->set_orderby('id','desc')->gets();
		$this->data['subview'] = 'cms/regisinfo/V_index';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function regisUpdate(){
		$data = $this->input->post();
		$this->Model->sets($data)->setPrimary($data["id"])->save();
		return redirect(site_url('admin/agate_regis'));
	}

	private function regisDetail(){
		$id = $this->input->get('id');
		$this->data['reg'] = $this->Model->sets(array('deleted' => 0, 'active' => 1, 'id' => $id))->get();
		$this->data['subview'] = 'cms/regisinfo/regdetail';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function regisExport(){
		ob_start();
		$objPHPExcel = new PHPExcel();
		$exlHeading = array(
			'font' => array(
				'bold' => true,
				'size' => 12,
				'name' => 'Verdana')
			);
		$objPHPExcel->getProperties()->setCreator("BigDream India")
				->setLastModifiedBy("TEST")
				->setTitle("REPORT")
				->setSubject("ATTENDANCE REPORT")
				->setDescription("Attendance Monthly Report")
				->setKeywords("ATT_REPORT")
				->setCategory("Excel Sheet");

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'Họ tên');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', 'Điện thoại');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', 'Email');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', 'Phân loại');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', 'Ngày đăng ký');
		$regisinfo = $this->Model->sets(array('deleted' => 0, 'active' => 1))->set_orderby('id','desc')->gets();
		for($i=1; $i<count($regisinfo); $i++){
			$course_type ="";
			if($regisinfo[$i]->course_type==1){
				$course_type = "Học sinh";
			}else if($regisinfo[$i]->course_type==2){
				$course_type = "Phụ huynh";
			}
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, $regisinfo[$i]->name);
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, $regisinfo[$i]->phone); 
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, $regisinfo[$i]->email); 
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, $course_type); 
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, date('d/m/Y',$regisinfo[$i]->time)); 

		}
		
		$objPHPExcel->getActiveSheet()->setTitle('Attendance Monthly Report');
		$objPHPExcel->setActiveSheetIndex(0);
		ob_end_clean();
		
		$filename = 'MyOfficeGuardian-Monthly_Report.xls'; 
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="' . $filename . '"'); 
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');





// 		
// 		$data = [
// 			["firstname" => "Mary", "lastname" => "Johnson", "age" => 25],
// 			["firstname" => "Amanda", "lastname" => "Miller", "age" => 18],
// 			["firstname" => "James", "lastname" => "Brown", "age" => 31],
// 			["firstname" => "Patricia", "lastname" => "Williams", "age" => 7],
// 			["firstname" => "Michael", "lastname" => "Davis", "age" => 43],
// 			["firstname" => "Sarah", "lastname" => "Miller", "age" => 24],
// 			["firstname" => "Patrick", "lastname" => "Miller", "age" => 27]
// 		  ];
// 		$data = $this->Model->sets(array('deleted' => 0, 'active' => 1))->gets();
// 		$arr = array();
// 		// foreach($data as $item){
// 		// 	$type = $item->course_type==1?"Học Sinh":"Phụ Huynh";
// 		// 	$arr = ["name" => $item->name, "email" => $item->email, "phone" => $item->phone, "phone" => $item->phone,'loại' => $type, 'thời gian đăng kí' => $item->time];
// 		// }

// 		$arr[] = ["name" => $data[0]->name, "email" => $data[0]->email, "phone" => $data[0]->phone, "phone" => $data[0]->phone,'loại' => 1, 'thời gian đăng kí' => $data[0]->time];
// 		  header("Content-Type: text/plain");

// 		  $filename = "phpzag_data_export_".date('Ymd') . ".xls";			

// 		  header("Content-Type: application/vnd.ms-excel; charset=UTF-8"); 
// header("Pragma: public"); 
// header("Expires: 0"); 
// header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
// header("Content-Type: application/force-download"); 
// header("Content-Type: application/octet-stream"); 
// header("Content-Type: application/download"); 
// header("Content-Disposition: attachment;filename=11.xls "); 
// header("Content-Transfer-Encoding: binary "); 
// 		//   header('Content-Type: application/vnd.ms-excel; charset=utf-8');
// 		  header("Content-Disposition: attachment; filename=\"$filename\"");	
// 		  $show_coloumn = false;
// 		  if(!empty($arr)) {
// 			foreach($arr as $record) {
// 			  if(!$show_coloumn) {
// 				// display field/column names in first row
// 				echo implode("\t", array_keys($record)) . "\n";
// 				$show_coloumn = true;
// 			  }
// 			  echo implode("\t", $record) . "\n";
// 			}
// 		  }
// 		  exit;  
	}

	private function delete_regis(){
		$id = $this->input->get('id');
		$temp['deleted'] = 1;
		$this->M_myweb->set_table('reg_info')->sets($temp)->setPrimary($id)->save();
		return redirect(site_url('admin/agate_regis'));
	}

	private function gift_addgift()
	{
		$temp = $this->input->post();
		$data['title'] = $temp['gift_title'];
		$data['description'] = $temp['gift_description'];
		if(isset($_FILES['gift_img_01']) && $_FILES['gift_img_01']['name']!=""){
			$image_01 = do_upload('avatar','gift_img_01');	
			$data['img_01'] = $image_01;				
		}
		if(isset($_FILES['gift_img_02']) && $_FILES['gift_img_02']['name']!=""){
			$image_01 = do_upload('avatar','gift_img_02');	
			$data['img_02'] = $image_01;				
		}
		if(isset($_FILES['gift_img_03']) && $_FILES['gift_img_03']['name']!=""){
			$image_01 = do_upload('avatar','gift_img_03');	
			$data['img_03'] = $image_01;				
		}
		if(isset($_FILES['gift_img_04']) && $_FILES['gift_img_04']['name']!=""){
			$image_01 = do_upload('avatar','gift_img_04');	
			$data['img_04'] = $image_01;				
		}
		$this->Model->sets($data)->save();
		$maxId = $this->m_gift->getMaxId();
		print_r($maxId.'-'.$data['title'].'-'.$data['img_01']);
	}

	private function gift_removegift()
	{
		$data = $this->input->post();
		$temp['deleted'] = 1;
		$this->M_myweb->set_table('gift')->sets($temp)->setPrimary($data['id'])->save();
		print_r('Cập nhật thành công');
	}

	private function gift_getgift(){
		$data = $this->input->post();
		$this->data['gift'] = $this->Model->sets(array('deleted' => 0, 'active' => 1, 'id' => $data['id']))->get();
		print_r($this->data['gift']->title.'~'.$this->data['gift']->description.'~'.$this->data['gift']->img_01.'~'.$this->data['gift']->img_02.'~'.$this->data['gift']->img_03.'~'.$this->data['gift']->img_04);
	}

	
	private function gift_updategift()
	{
		$temp = $this->input->post();
		$data['title'] = $temp['gift_title'];
		$data['description'] = $temp['gift_description'];
		if(isset($_FILES['gift_img_01']) && $_FILES['gift_img_01']['name']!=""){
			$image_01 = do_upload('avatar','gift_img_01');	
			$data['img_01'] = $image_01;				
		}
		if(isset($_FILES['gift_img_02']) && $_FILES['gift_img_02']['name']!=""){
			$image_01 = do_upload('avatar','gift_img_02');	
			$data['img_02'] = $image_01;				
		}
		if(isset($_FILES['gift_img_03']) && $_FILES['gift_img_03']['name']!=""){
			$image_01 = do_upload('avatar','gift_img_03');	
			$data['img_03'] = $image_01;				
		}
		if(isset($_FILES['gift_img_04']) && $_FILES['gift_img_04']['name']!=""){
			$image_01 = do_upload('avatar','gift_img_04');	
			$data['img_04'] = $image_01;				
		}
		$this->Model->sets($data)->setPrimary($temp['id'])->save();
		$result = $this->Model->sets(array('deleted' => 0, 'active' => 1, 'id'=>$temp['id']))->get();
		print_r($result->img_01);
	}

}
