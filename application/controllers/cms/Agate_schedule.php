<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Agate_schedule extends CMS_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->Model = $this->M_myweb->set_table('schedule');
	}

	public function index()
	{
		switch ($this->act) {
			case "update":
				$this->update();
				break;
			case "lock":
				$this->locked();
				break;
			case "del":
				$this->delete();
				break;
			case "upd_founder":
				$this->staff_founder();
				break;
			default:
				$this->schedule();
				break;
		}
	}

	private function update(){
		$data = $this->input->post();
		if(empty($data['id'])){
			$temp['session'] = $data['session'];
			$temp['time'] = $data['time'];
			$temp['description'] = $data['description'];
			$this->Model->sets($temp)->save();
			return redirect(site_url('admin/agate_schedule'));
		}else{
			$temp['session'] = $data['session'];
			$temp['time'] = $data['time'];
			$temp['description'] = $data['description'];
			$this->Model->sets($temp)->setPrimary($data['id'])->save();
			return redirect(site_url('admin/agate_schedule'));
		}
		
	}



	private function schedule(){
		$this->data['schedules'] = $this->Model->sets(array('deleted' => 0))->gets();
		$this->data['subview'] = 'cms/schedule/schedulelist';
		$this->load->view('cms/_main_page',$this->data);
	}
	private function locked(){
		$data = $this->input->post();
		$temp['active'] = $data['status'];
		$this->Model->sets($temp)->setPrimary($data["id"])->save();
		print_r("Cập nhật thành công");
	}

	private function delete(){
		$data = $this->input->post();
		$temp['deleted'] = 1;
		$this->Model->sets($temp)->setPrimary($data["id"])->save();
		print_r("Xoá thành công");
	}
	
	

}
