<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Agate_staff extends CMS_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->Model = $this->M_myweb->set_table('staff_config');
		$this->load->model('cms/m_class');
	}

	public function index()
	{
		switch ($this->act) {
			case "upd_partner":
				$this->staff_partner();
				break;
			case "upd_staff":
				$this->staff_staff();
				break;
			case "upd_teacher":
				$this->staff_teacher();
				break;
			case "upd_founder":
				$this->staff_founder();
				break;
			default:
				$this->class();
				break;
		}
	}

	private function class(){
		$arr_fouders= array();
		$arr_staffs= array();


		$this->data['staff'] = $this->Model->gets();
		$this->data['staffs'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1))->gets();
		$this->data['staff_01'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['staff'][22]->value))->get();
		$this->data['staff_02'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['staff'][23]->value))->get();
		$this->data['staff_03'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['staff'][24]->value))->get();
		
		$founders = $this->M_myweb->set_table('staff_config')->sets(array('position' => 'founder', 'active' => 1))->gets();
		$this->data['partner'] = $this->M_myweb->set_table('partner')->sets(array('deleted' => 0, 'active' => 1))->gets();
		$this->data['partner_01'] = $this->M_myweb->set_table('partner')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['staff'][0]->value))->get();
		$this->data['partner_02'] = $this->M_myweb->set_table('partner')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['staff'][1]->value))->get();

		$this->data['user'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1))->gets();
		foreach($founders as $item){
			$arr_fouders[] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$item->value))->get();
		}
		$this->data['founders'] = $arr_fouders;
		$this->data['subview'] = 'cms/staff/V_index';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function staff_partner()
	{
		$data = $this->input->post();
		$temp['value'] = $data['partner'][0];
		$this->Model->sets($temp)->setPrimary(1)->save();
		$temp['value'] = $data['partner'][1];
		$this->Model->sets($temp)->setPrimary(2)->save();
		print_r('Cập nhật thành công');
	}

	private function staff_staff()
	{
		$data = $this->input->post();
		if(isset($_FILES['staff1_pc']) && $_FILES['staff1_pc']['name']!=""){
			$image = do_upload('avatar','staff1_pc');	
			$temp['value'] = $image;		
			$this->Model->sets($temp)->setPrimary(4)->save();
		}
		if(isset($_FILES['staff2_pc']) && $_FILES['staff2_pc']['name']!=""){
			$image = do_upload('avatar','staff2_pc');	
			$temp['value'] = $image;		
			$this->Model->sets($temp)->setPrimary(5)->save();
		}
		if(isset($_FILES['staff3_pc']) && $_FILES['staff3_pc']['name']!=""){
			$image = do_upload('avatar','staff3_pc');	
			$temp['value'] = $image;		
			$this->Model->sets($temp)->setPrimary(6)->save();
		}
		if(isset($_FILES['staff1_mb']) && $_FILES['staff1_mb']['name']!=""){
			$image = do_upload('avatar','staff1_mb');	
			$temp['value'] = $image;		
			$this->Model->sets($temp)->setPrimary(7)->save();
		}
		if(isset($_FILES['staff2_mb']) && $_FILES['staff2_mb']['name']!=""){
			$image = do_upload('avatar','staff2_mb');	
			$temp['value'] = $image;		
			$this->Model->sets($temp)->setPrimary(8)->save();
		}
		if(isset($_FILES['staff3_mb']) && $_FILES['staff3_mb']['name']!=""){
			$image = do_upload('avatar','staff3_mb');	
			$temp['value'] = $image;		
			$this->Model->sets($temp)->setPrimary(9)->save();
		}
		$temp['value'] = $data['staff1'];
		$this->Model->sets($temp)->setPrimary(38)->save();
		$temp['value'] = $data['staff2'];
		$this->Model->sets($temp)->setPrimary(39)->save();
		$temp['value'] = $data['staff3'];
		$this->Model->sets($temp)->setPrimary(40)->save();
		print_r('Cập nhật thành công');
	}

	private function staff_teacher()
	{
		$data = $this->input->post();
		$temp['value'] = $data['teacher'][0];
		$this->Model->sets($temp)->setPrimary(9)->save();
		$temp['value'] = $data['teacher'][1];
		$this->Model->sets($temp)->setPrimary(10)->save();
		$temp['value'] = $data['teacher'][2];
		$this->Model->sets($temp)->setPrimary(11)->save();
		print_r('Cập nhật thành công');
	}

	private function staff_founder()
	{
		$data = $this->input->post();
		$temp['time_log'] = time();
		$temp['active'] = 0;
		$this->Model->sets($temp)->setUpdate("position","founder")->save();
		foreach($data['founder'] as $item){
			$temp['value'] = $item;
			$temp['time_log'] = time();
			$temp['position'] = "founder";
			$temp['active'] = 1;
			$this->Model->sets($temp)->save();
		}
		print_r('Cập nhật thành công');
	}

	private function class_basic()
	{
		$data = $this->input->post();
		$temp['value'] = $data['basic_name_01'];
		$this->Model->sets($temp)->setPrimary(13)->save();
		$temp['value'] = $data['basic_name_02'];
		$this->Model->sets($temp)->setPrimary(14)->save();
		$temp['value'] = $data['basic_name_03'];
		$this->Model->sets($temp)->setPrimary(15)->save();
		$temp['value'] = $data['basic_description_01'];
		$this->Model->sets($temp)->setPrimary(16)->save();
		$temp['value'] = $data['basic_description_02'];
		$this->Model->sets($temp)->setPrimary(17)->save();
		$temp['value'] = $data['basic_description_03'];
		$this->Model->sets($temp)->setPrimary(18)->save();
		if($_FILES['basic_img_01']['name']!=""){
			$image_01 = do_upload('avatar','basic_img_01');	
			$data['basic_img_01'] = $image_01;				
		}
		if($_FILES['basic_img_02']['name']!=""){
			$image_02 = do_upload('avatar','basic_img_02');	
			$data['basic_img_02'] = $image_02;				
		}
		if($_FILES['basic_img_03']['name']!=""){
			$image_03 = do_upload('avatar','basic_img_03');	
			$data['basic_img_03'] = $image_03;				
		}
		$temp['value'] = $data['basic_img_01'];
		$this->Model->sets($temp)->setPrimary(19)->save();
		$temp['value'] = $data['basic_img_02'];
		$this->Model->sets($temp)->setPrimary(20)->save();
		$temp['value'] = $data['basic_img_03'];
		$this->Model->sets($temp)->setPrimary(21)->save();
		print_r('Cập nhật thành công');
	}

	private function class_classinfo()
	{
		$data = $this->input->post();
		$temp['value'] = $data['classinfo_title'];
		$this->Model->sets($temp)->setPrimary(22)->save();
		$temp['value'] = $data['classinfo_price'];
		$this->Model->sets($temp)->setPrimary(23)->save();
		$temp['value'] = $data['classinfo_description'];
		$this->Model->sets($temp)->setPrimary(24)->save();
		if($_FILES['classinfo_img']['name']!=""){
			$image = do_upload('avatar','classinfo_img');	
			$data['classinfo_img'] = $image;				
		}
		$temp['value'] = $data['classinfo_img'];
		$this->Model->sets($temp)->setPrimary(25)->save();
		print_r('Cập nhật thành công');
	}

	private function class_removeimg()
	{
		$data = $this->input->post();
		$temp['deleted'] = 1;
		$this->M_myweb->set_table('class_img')->sets($temp)->setPrimary($data['id'])->save();
		print_r('Cập nhật thành công');
	}

	private function class_addimg()
	{
		$data = $this->input->post();
		if($_FILES['classimglist_img']['name']!=""){
			$image = do_upload('avatar','classimglist_img');	
			$data['img'] = $image;				
		}
		$data['class_id']=1;
		$this->M_myweb->set_table('class_img')->sets($data)->save();
		$maxId = $this->m_class->getMaxId();
		print_r($maxId.'-'.$data['img']);
	}

	

}
