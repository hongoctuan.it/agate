<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class hashtag extends CMS_Controller {

	public function __construct(){
		parent::__construct();
		$this->M_hashtag = $this->M_myweb->set_table('trend_hashtag');
	}
	
	public function index(){
		switch($this->act){
			case "upd":
				if($this->input->post())
					$this->save();
				$this->edit();
				break;
			case "add":
				$this->add();
				break;
			case 'del':
				$this->delete();
				break;
			case 'trend':
				$this->updateTrend();
				break;
			case 'profile':
				$this->profile();
				break;
			default:
				$this->home();
				break;
		}
	}
	
	private function home()
	{
		$this->data['hashtags'] = $this->M_hashtag->sets(array('active'=>1,'deleted' => 0))->gets();
		$this->data['subview'] = 'cms/hashtag/hashtaglist';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function delete(){
		$data = $this->input->post();
		$temp['deleted'] = 1;
		$this->M_hashtag->sets($temp)->setPrimary($data["id"])->save();
	}

	private function updateTrend(){
		$data = $this->input->post();
		$temp['trend'] = $data["trend"];
		$this->M_hashtag->sets($temp)->setPrimary($data["id"])->save();
	}


	private function profile(){
		$id=$this->input->get('id');
		$this->data['hashtag'] = $this->M_hashtag->sets(array('id'=>$id))->get();
		$this->data['subview'] = 'cms/hashtag/hashtagdetail';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function add(){
		$data = $this->input->post();
		if(!empty($data)){
			if(empty($data['id'])){
				$temp['title']= $data['title'];
				$this->M_hashtag->sets($temp)->save();
			}else{
				$temp['id'] = $data['id'];
				$temp['title']= $data['title'];
				$this->M_hashtag->sets($temp)->setPrimary($data["id"])->save();
			}
			return redirect(site_url('admin/hashtag?token='.$this->data['infoLog']->token));
		}
		else{
			$this->data['hashtags'] = $this->M_myweb->set_table('trend_hashtag')->gets();
			$this->data['subview'] = 'cms/hashtag/hashtagdetail';
			$this->load->view('cms/_main_page',$this->data);
		}
			
		
	}

}