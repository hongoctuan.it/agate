<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CMS_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->Model = $this->M_myweb->set_table('home');
		$this->load->model('cms/m_class');
	}

	public function index()
	{
		switch ($this->act) {
			case "upd_banner":
				$this->home_banner();
				break;
			case "upd_highlight":
				$this->home_highlight();
				break;
			case "upd_staff":
				$this->home_staff();
				break;
			case "upd_class":
				$this->home_class();
				break;
			case "upd_agatemag":
				$this->home_agatemag();
				break;
			case "upd_agategift":
				$this->home_agategift();
				break;
			case "upd_slider":
				$this->home_slider();
				break;
			case "upd_partner":
				$this->home_partner();
				break;
			case "upd_comment":
				$this->home_comment();
				break;
			default:
				$this->home();
				break;
		}
	}

	private function home(){
		$this->data['home'] = $this->Model->sets(array('deleted' => 0, 'active' => 1))->gets();
		$this->data['staff'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1))->gets();
		$this->data['staff_01'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['home'][19]->value))->get();
		$this->data['staff_02'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['home'][20]->value))->get();
		$this->data['staff_03'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['home'][21]->value))->get();
		$this->data['mag'] = $this->M_myweb->set_table('mag')->sets(array('deleted' => 0, 'active' => 1))->gets();
		$this->data['mag_01'] = $this->M_myweb->set_table('mag')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['home'][22]->value))->get();
		$this->data['mag_02'] = $this->M_myweb->set_table('mag')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['home'][23]->value))->get();
		$this->data['class'] = $this->M_myweb->set_table('class')->sets(array('deleted' => 0, 'active' => 1))->gets();
		$this->data['class_select'] = $this->M_myweb->set_table('class')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['home'][27]->value))->get();
		$this->data['partner'] = $this->M_myweb->set_table('partner')->sets(array('deleted' => 0, 'active' => 1))->gets();
		$this->data['partner_01'] = $this->M_myweb->set_table('partner')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['home'][25]->value))->get();
		$this->data['partner_02'] = $this->M_myweb->set_table('partner')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['home'][26]->value))->get();
		$this->data['gifts'] = $this->M_myweb->set_table('gift')->sets(array('deleted' => 0, 'active' => 1))->gets();
		$this->data['gift'] = $this->M_myweb->set_table('gift')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['home'][24]->value))->get();
		$this->data['classimg'] = $this->M_myweb->set_table('class_img')->sets(array('deleted' => 0))->gets();
// 		$this->data['classimg'] = $this->M_myweb->set_table('class_img')->sets(array('deleted' => 0, 'home'=> 1))->gets();

		$this->data['comment'] = $this->M_myweb->set_table('comment')->sets(array('deleted' => 0, 'active' => 1))->gets();
		$this->data['comment_01'] = $this->M_myweb->set_table('comment')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['home'][35]->value))->get();
		$this->data['comment_02'] = $this->M_myweb->set_table('comment')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['home'][36]->value))->get();
		$this->data['comment_03'] = $this->M_myweb->set_table('comment')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['home'][49]->value))->get();
		$this->data['partner'] = $this->M_myweb->set_table('partner')->sets(array('deleted' => 0, 'active' => 1))->gets();
		$this->data['partner'] = $this->M_myweb->set_table('partner')->sets(array('deleted' => 0, 'active' => 1))->gets();
		$this->data['partner'] = $this->M_myweb->set_table('partner')->sets(array('deleted' => 0, 'active' => 1))->gets();
		$this->data['partner'] = $this->M_myweb->set_table('partner')->sets(array('deleted' => 0, 'active' => 1))->gets();
		$this->data['partner'] = $this->M_myweb->set_table('partner')->sets(array('deleted' => 0, 'active' => 1))->gets();

		$this->data['subview'] = 'cms/home/V_index';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function home_banner()
	{
		$data = $this->input->post();
		$banner_title['value'] = $data['banner_title'];
		$this->Model->sets($banner_title)->setPrimary(1)->save();
		$temp['value'] = $data['banner_description'];
		$this->Model->sets($temp)->setPrimary(2)->save();
		$temp['value'] = $data['banner_item_name_01'];
		$this->Model->sets($temp)->setPrimary(3)->save();
		$temp['value'] = $data['banner_item_name_02'];
		$this->Model->sets($temp)->setPrimary(4)->save();
		$temp['value'] = $data['banner_item_name_03'];
		$this->Model->sets($temp)->setPrimary(5)->save();
		$temp['value'] = $data['banner_item_name_04'];
		$this->Model->sets($temp)->setPrimary(6)->save();
		if(isset($_FILES['banner_img']) && $_FILES['banner_img']['name']!=""){
			$image = do_upload('avatar','banner_img');	
			$temp['value'] = $image;		
			$this->Model->sets($temp)->setPrimary(8)->save();
		}
		print_r('Cập nhật thành công');
	}

	private function home_highlight()
	{
		$data = $this->input->post();
		if(isset($_FILES['highlight_img']) && $_FILES['highlight_img']['name']!=""){
			$image = do_upload('avatar','highlight_img');	
			$temp['value'] = $image;		
			$this->Model->sets($temp)->setPrimary(11)->save();
		}
		$temp['value'] = $data['highlight_title'];
		$this->Model->sets($temp)->setPrimary(16)->save();
		$temp['value'] = $data['highlight_description'];
		$this->Model->sets($temp)->setPrimary(17)->save();
		$temp['value'] = $data['highlight_item_name_01'];
		$this->Model->sets($temp)->setPrimary(12)->save();
		$temp['value'] = $data['highlight_item_name_02'];
		$this->Model->sets($temp)->setPrimary(13)->save();
		$temp['value'] = $data['highlight_item_name_03'];
		$this->Model->sets($temp)->setPrimary(14)->save();
		$temp['value'] = $data['highlight_item_name_04'];
		$this->Model->sets($temp)->setPrimary(15)->save();
		$temp['value'] = $data['highlight_item_url_title_01'];
		$this->Model->sets($temp)->setPrimary(38)->save();
		$temp['value'] = $data['highlight_item_url_title_02'];
		$this->Model->sets($temp)->setPrimary(39)->save();
		$temp['value'] = $data['highlight_item_url_title_03'];
		$this->Model->sets($temp)->setPrimary(40)->save();
		$temp['value'] = $data['highlight_item_url_title_04'];
		$this->Model->sets($temp)->setPrimary(41)->save();
		$temp['value'] = $data['highlight_item_url_01'];
		$this->Model->sets($temp)->setPrimary(42)->save();
		$temp['value'] = $data['highlight_item_url_02'];
		$this->Model->sets($temp)->setPrimary(43)->save();
		$temp['value'] = $data['highlight_item_url_03'];
		$this->Model->sets($temp)->setPrimary(44)->save();
		$temp['value'] = $data['highlight_item_url_04'];
		$this->Model->sets($temp)->setPrimary(45)->save();
		$temp['value'] = $data['highlight_upcoming_01'];
		$this->Model->sets($temp)->setPrimary(46)->save();
		$temp['value'] = $data['highlight_upcoming_02'];
		$this->Model->sets($temp)->setPrimary(47)->save();
		$temp['value'] = $data['highlight_upcoming_03'];
		$this->Model->sets($temp)->setPrimary(48)->save();
		$temp['value'] = $data['highlight_upcoming_04'];
		$this->Model->sets($temp)->setPrimary(49)->save();
		$temp['value'] = $data['highlight_des_01'];
		$this->Model->sets($temp)->setPrimary(29)->save();
		$temp['value'] = $data['highlight_des_02'];
		$this->Model->sets($temp)->setPrimary(30)->save();
		$temp['value'] = $data['highlight_des_03'];
		$this->Model->sets($temp)->setPrimary(31)->save();
		$temp['value'] = $data['highlight_des_04'];
		$this->Model->sets($temp)->setPrimary(32)->save();
		print_r('Cập nhật thành công');
	}

	private function home_comment()
	{
		$data = $this->input->post();
		$banner_title['value'] = $data['comment_title'];
		$this->Model->sets($banner_title)->setPrimary(51)->save();
		$temp['value'] = $data['comment_description'];
		$this->Model->sets($temp)->setPrimary(52)->save();
		$temp['value'] = $data['comment_list'][0];
		$this->Model->sets($temp)->setPrimary(36)->save();
		$temp['value'] = $data['comment_list'][1];
		$this->Model->sets($temp)->setPrimary(37)->save();
		$temp['value'] = $data['comment_list'][2];
		$this->Model->sets($temp)->setPrimary(50)->save();
		print_r('Cập nhật thành công');
	}


	private function home_staff()
	{
		$data = $this->input->post();
		if(isset($_FILES['staff1_pc']) && $_FILES['staff1_pc']['name']!=""){
			$image = do_upload('avatar','staff1_pc');	
			$temp['value'] = $image;		
			$this->Model->sets($temp)->setPrimary(53)->save();
		}
		if(isset($_FILES['staff2_pc']) && $_FILES['staff2_pc']['name']!=""){
			$image = do_upload('avatar','staff2_pc');	
			$temp['value'] = $image;		
			$this->Model->sets($temp)->setPrimary(55)->save();
		}
		if(isset($_FILES['staff3_pc']) && $_FILES['staff3_pc']['name']!=""){
			$image = do_upload('avatar','staff3_pc');	
			$temp['value'] = $image;		
			$this->Model->sets($temp)->setPrimary(54)->save();
		}
		if(isset($_FILES['staff1_mb']) && $_FILES['staff1_mb']['name']!=""){
			$image = do_upload('avatar','staff1_mb');	
			$temp['value'] = $image;		
			$this->Model->sets($temp)->setPrimary(56)->save();
		}
		if(isset($_FILES['staff2_mb']) && $_FILES['staff2_mb']['name']!=""){
			$image = do_upload('avatar','staff2_mb');	
			$temp['value'] = $image;		
			$this->Model->sets($temp)->setPrimary(58)->save();
		}
		if(isset($_FILES['staff3_mb']) && $_FILES['staff3_mb']['name']!=""){
			$image = do_upload('avatar','staff3_mb');	
			$temp['value'] = $image;		
			$this->Model->sets($temp)->setPrimary(57)->save();
		}
		$temp['value'] = $data['staff1'];
		$this->Model->sets($temp)->setPrimary(20)->save();
		$temp['value'] = $data['staff2'];
		$this->Model->sets($temp)->setPrimary(21)->save();
		$temp['value'] = $data['staff3'];
		$this->Model->sets($temp)->setPrimary(22)->save();
		print_r('Cập nhật thành công');
	}

	private function home_class()
	{
		$data = $this->input->post();
		$temp['title'] = $data['title'];
		$temp['description'] = $data['description'];
		if(isset($_FILES['img_01']) && $_FILES['img_01']['name']!=""){
			$image = do_upload('avatar','img_01');	
			$temp['img_01'] = $image;		
		}
		if(isset($_FILES['img_02']) && $_FILES['img_02']['name']!=""){
			$image = do_upload('avatar','img_02');	
			$temp['img_02'] = $image;		
		}
		if(isset($_FILES['img_03']) && $_FILES['img_03']['name']!=""){
			$image = do_upload('avatar','img_03');	
			$temp['img_03'] = $image;		
		}
		$this->M_myweb->set_table('class')->sets($temp)->setPrimary(1)->save();
		print_r('Cập nhật thành công');
	}

	private function home_agatemag()
	{
		$data = $this->input->post();
		$temp['value'] = $data['agatemag'][0];
		$this->Model->sets($temp)->setPrimary(23)->save();
		$temp['value'] = $data['agatemag'][1];
		$this->Model->sets($temp)->setPrimary(24)->save();
		print_r('Cập nhật thành công');
	}

	private function home_agategift()
	{
		$data = $this->input->post();
		if(isset($_FILES['gift_img']) && $_FILES['gift_img']['name']!=""){
			$image = do_upload('avatar','gift_img');	
			$temp['value'] = $image;		
			$this->Model->sets($temp)->setPrimary(7)->save();
		}
		$temp['value'] = $data['agategift'][0];
		$this->Model->sets($temp)->setPrimary(25)->save();
		print_r('Cập nhật thành công');
	}

	private function home_slider()
	{
		$data = $this->input->post();
		$temp['home'] = 0;
		$this->m_class->updateAll('home');
		foreach($data['slider'] as $item){
			$temp['home'] = 1;
			$this->M_myweb->set_table('class_img')->sets($temp)->setPrimary($item)->save();
		}
		print_r("Cập nhật thành công");
	}

	private function home_partner()
	{
		$data = $this->input->post();
		$temp['value'] = $data['agatepartner'][0];
		$this->Model->sets($temp)->setPrimary(26)->save();
		$temp['value'] = $data['agatepartner'][1];
		$this->Model->sets($temp)->setPrimary(27)->save();
		print_r('Cập nhật thành công');
	}
}
