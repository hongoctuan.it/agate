<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mag extends CMS_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('cms/m_mag');
		$this->M_mag = $this->M_myweb->set_table('mag');
	}
	
	public function index(){
		switch($this->act){
			case "upd":
				if($this->input->post())
					$this->save();
				$this->edit();
				break;
			case "uinfo":
				$this->ajax_userInfo();
				break;
			case "setpass":
				$this->setpass();
				break;
			case "lock":
				$this->locked();
				break;
			case 'profile':
				$this->profile();
				break;
			case "add":
				$this->add();
				break;
			case 'del':
				$this->delete();
				break;
			default:
				$this->home();
				break;
		}
	}
	
	private function home()
	{
		$this->data['mags'] = $this->M_mag->sets(array('deleted' => 0))->gets();
		$this->data['hashtags'] = $this->M_myweb->set_table('trend_hashtag')->sets(array('active'=>1,'deleted' => 0))->gets();
		$this->data['subview'] = 'cms/mag/maglist';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function delete(){
		$data = $this->input->post();
		$temp['deleted'] = 1;
		$this->M_mag->sets($temp)->setPrimary($data["id"])->save();
		$this->M_myweb->set_table('hashtag_mag')->sets($temp)->setPrimary($data["mag_id"])->save();

		print_r("Cập nhật thành công");
	}


	private function locked(){
		$data = $this->input->post();
		$temp['active'] = $data['status'];
		$temp['time_active'] = time();
		$this->M_mag->sets($temp)->setPrimary($data["id"])->save();
		print_r("Cập nhật thành công");
	}

	private function profile(){
		$id=$this->input->get('id');
		$this->data['mag'] = $this->M_mag->set('id',$id)->get();
		$this->data['hashtag'] = $this->M_myweb->set_table('hashtag_mag')->sets(array('deleted' => 0,'mag_id'=>$id))->gets();
		$this->data['hashtags'] = $this->M_myweb->set_table('trend_hashtag')->set('deleted',0)->gets();
		$this->data['subview'] = 'cms/mag/magdetail';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function add(){
		$data = $this->input->post();
		if(!empty($data)){
			if(empty($data['id'])){
				$temp['title']= $data['title'];
				$temp['description']= $data['description'];
				$temp['content']= $data['content'];
				$temp['author']= $data['author'];
				$temp['type']= $data['type'];
				$temp['time']= time();
				$temp['slug'] = str_replace(array( '\'','"',',' , ';', '<', '>', '?', '&', ' ','”','“'),"-",stripUnicode($data['title'])).time();
				if(isset($_FILES['mag_img_01']) && $_FILES['mag_img_01']['name']!=""){
					$image = do_upload('avatar','mag_img_01');	
					$temp['thumbnail'] = $image;				
				}
				if(isset($_FILES['mag_img_02']) && $_FILES['mag_img_02']['name']!=""){
					$image = do_upload('avatar','mag_img_02');	
					$temp['banner'] = $image;				
				}

				$this->M_mag->sets($temp)->save();
				$mag_id = $this->m_mag->getMaxId();
				if(!empty($data['hashtag'])){
					foreach($data['hashtag'] as $item){
						$hashtag['hashtag_id'] = $item;
						$hashtag['mag_id'] = $mag_id;
						$this->M_myweb->set_table('hashtag_mag')->sets($hashtag)->save();
					}
				}
			}else{
				$temp['id'] = $data['id'];
				$temp['title']= $data['title'];
				$temp['description']= $data['description'];
				$temp['content']= $data['content'];
				$temp['author']= $data['author'];
				$temp['type']= $data['type'];
				$temp['time']= time();
				// $temp['slug'] = str_replace(array( '\'','"',',' , ';', '<', '>', '?', '&', ' ','”','“'),"-",stripUnicode($data['title'])).time();
				if(isset($_FILES['mag_img_01']) && $_FILES['mag_img_01']['name']!=""){
					$image = do_upload('avatar','mag_img_01');	
					$temp['thumbnail'] = $image;				
				}
				if(isset($_FILES['mag_img_02']) && $_FILES['mag_img_02']['name']!=""){
					$image = do_upload('avatar','mag_img_02');	
					$temp['banner'] = $image;				
				}
				$this->M_mag->sets($temp)->setPrimary($data["id"])->save();
				$mag_deleted['deleted'] = 1;
				$this->M_myweb->set_table('hashtag_mag')->sets($mag_deleted)->setUpdate('mag_id',$temp['id'])->save();
				if(!empty($data['hashtag'])){
					foreach($data['hashtag'] as $item){
						$hashtag['hashtag_id'] = $item;
						$hashtag['mag_id'] = $temp['id'];
						$flag = $this->M_myweb->set_table('hashtag_mag')->sets(array('mag_id' => $temp['id'], 'hashtag_id' => $item))->get();
						if(empty($flag)){
							$this->M_myweb->set_table('hashtag_mag')->sets($hashtag)->save();
						}else{
							$mag_deleted['deleted'] = 0;
							$this->M_myweb->set_table('hashtag_mag')->sets($mag_deleted)->setPrimary($flag->id)->save();
						}
					}
				}
			}
			return redirect(site_url('admin/mag?token='.$this->data['infoLog']->token));
		}
		else{
			$this->data['hashtags'] = $this->M_myweb->set_table('trend_hashtag')->gets();
			$this->data['subview'] = 'cms/mag/magdetail';
			$this->load->view('cms/_main_page',$this->data);
		}
			
		
	}

}