<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CMS_Controller {

	public function __construct(){
		parent::__construct();
		$this->M_user = $this->M_myweb->set_table('user');
	}
	
	public function index(){
		switch($this->act){
			case "upd":
				if($this->input->post())
					$this->save();
				$this->edit();
				break;
			case "uinfo":
				$this->ajax_userInfo();
				break;
			case "setpass":
				$this->setpass();
				break;
			case "lock":
				$this->locked();
				break;
			case 'profile':
				$this->profile();
				break;
			case "add":
				$this->add();
				break;
			case 'del':
				$this->delete();
				break;
			default:
				$this->member();
				break;
		}
	}

	private function member()
	{
		$this->data['users'] = $this->M_user->sets(array('deleted' => 0, 'role' => 1))->gets();
		$this->data['subview'] = 'cms/member/memberlist';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function delete(){
		$data = $this->input->post();
		$temp['deleted'] = 1;
		$this->M_user->sets($temp)->setPrimary($data["id"])->save();
		print_r("Cập nhật thành công");
	}

	private function locked(){
		$data = $this->input->post();
		$temp['active'] = $data['status'];
		$this->M_user->sets($temp)->setPrimary($data["id"])->save();
		print_r("Cập nhật thành công");
	}

	private function profile(){
		$data = $this->input->post();
		$user = $this->M_user->set('id',$data['id'])->get();
		print_r($user->id.'~'.$user->full_name.'~'.$user->email.'~'.$user->phone.'~'.$user->password.'~'.$user->position.'~'.$user->position_description.'~'.$user->description.'~'.$user->img);
	}

	private function add(){
		$data = $this->input->post();
		if(!isset($data['id'])){
			if(isset($_FILES['user_img']) && $_FILES['user_img']['name']!=""){
				$image = do_upload('avatar','user_img');	
				$data['img'] = $image;				
			}
			$this->M_user->sets($data)->save();
		}else{
			if(isset($_FILES['user_img']) && $_FILES['user_img']['name']!=""){
				$image = do_upload('avatar','user_img');	
				$data['img'] = $image;				
			}
			$this->M_user->sets($data)->setPrimary($data["id"])->save();
		}

		
		return redirect(site_url('admin/member?token='.$this->data['infoLog']->token));
	}

}