<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partner extends CMS_Controller {

	public function __construct(){
		parent::__construct();
		$this->M_partner = $this->M_myweb->set_table('partner');
	}
	
	public function index(){
		switch($this->act){
			case "upd":
				if($this->input->post())
					$this->save();
				$this->edit();
				break;
			case "lock":
				$this->locked();
				break;
			case 'profile':
				$this->profile();
				break;
			case "add":
				$this->add();
				break;
			case 'del':
				$this->delete();
				break;
			default:
				$this->partner();
				break;
		}
	}
	
	private function partner()
	{
		$this->data['partners'] = $this->M_partner->sets(array('deleted' => 0,'active' => 1))->gets();
		$this->data['subview'] = 'cms/partner/partnerlist';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function delete(){
		$data = $this->input->post();
		$temp['deleted'] = 1;
		$this->M_partner->sets($temp)->setPrimary($data["id"])->save();
		print_r("Cập nhật thành công");
	}

	private function locked(){
		$data = $this->input->post();
		$temp['active'] = $data['status'];
		$this->M_partner->sets($temp)->setPrimary($data["id"])->save();
		print_r("Cập nhật thành công");
	}

	private function profile(){
		$data = $this->input->post();
		$partner = $this->M_partner->set('id',$data['id'])->get();
		print_r($partner->id.'~'.$partner->name.'~'.$partner->phone.'~'.$partner->address.'~'.$partner->description.'~'.$partner->img);
	}

	private function add(){
		$data = $this->input->post();
		if(!isset($data['id'])){
			if(isset($_FILES['partner_img']) && $_FILES['partner_img']['name']!=""){
				$image = do_upload('avatar','partner_img');	
				$data['img'] = $image;				
			}
			$this->M_partner->sets($data)->save();
		}else{
			if(isset($_FILES['partner_img']) && $_FILES['partner_img']['name']!=""){
				$image = do_upload('avatar','partner_img');	
				$data['img'] = $image;				
			}
			$this->M_partner->sets($data)->setPrimary($data["id"])->save();
		}
		return redirect(site_url('admin/partner?token='.$this->data['infoLog']->token));
	}
}