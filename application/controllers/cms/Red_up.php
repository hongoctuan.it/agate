<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Red_up extends CMS_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->Model = $this->M_myweb->set_table('red_up');
	}

	public function index()
	{
		switch ($this->act) {
			case "add":
				$this->save();
				break;
			case "update":
				$this->update();
				break;
			default:
				$this->home();
				break;
		}
	}

	public function home()
	{
		$this->data['list'] = $this->M_myweb->set_table('red_up')->gets();
		$this->data['subview'] 	= 'cms/red_up/V_index';
		$this->load->view('cms/_main_page', $this->data);		
	}



	public function save()
	{
		$data = $this->input->post();
		if(!empty($data)){
			if (isset($_FILES['uploadfile']['name']) && $_FILES['uploadfile']['name']!="") {
				$config['upload_path'] = './file';
				$config['allowed_types'] = '*';
				$config['max_size'] = 10240000;
				$this->load->library('upload', $config);
				$this->upload->do_upload('uploadfile');
				$data['file_name'] = $_FILES['uploadfile']['name'];
			}
			$this->Model->sets($data)->save();	
			redirect("admin/red_up");
		}else{
			$this->data['subview'] 	= 'cms/red_up/Create';
			$this->load->view('cms/_main_page', $this->data);		
		}
		
	}

	public function update()
	{
		$data = $this->input->post();
		if(!empty($data)){
			if (isset($_FILES['uploadfile']['name']) && $_FILES['uploadfile']['name']!="") {
				$config['upload_path'] = './';
				$config['allowed_types'] = '*';
				$config['max_size'] = 10240000;
				$this->load->library('upload', $config);
				$this->upload->do_upload('uploadfile');
				$data['file_name'] = $_FILES['uploadfile']['name'];
			}
			$this->Model->sets($data)->setPrimary($data['id'])->save();
			redirect("admin/red_up");
		}else{
			$id = $this->input->get('id');
			$this->data['red_up'] = $this->Model->set('id', $id)->get();
			$this->data['subview'] 	= 'cms/red_up/Create';
			$this->load->view('cms/_main_page', $this->data);	
		}
		
	}
}
