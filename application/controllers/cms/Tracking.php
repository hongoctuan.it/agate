<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tracking extends CMS_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('cms/m_tracking');
	}

	public function index()
	{
		$this->data['pie'] = $this->m_tracking->getPieChart();
		$this->data['speed'] = $this->m_tracking->getSpeedChart();
		$this->data['month'] = $this->m_tracking->getCurrentMonth();
		$this->data['day'] = $this->m_tracking->getCurrentDay();
		$this->data['year'] = $this->m_tracking->getCurrentYear();
		$this->data['total'] = $this->m_tracking->getTotal();
		$this->data['subview'] = 'cms/tracking/home';
		$this->load->view('cms/_main_page',$this->data);
	}
}
