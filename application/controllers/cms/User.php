<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CMS_Controller {

	public function __construct(){
		parent::__construct();
		$this->M_user = $this->M_myweb->set_table('user');
	}
	
	public function index(){
		switch($this->act){
			case "upd":
				if($this->input->post())
					$this->save();
				$this->edit();
				break;
			case "uinfo":
				$this->ajax_userInfo();
				break;
			case "setpass":
				$this->setpass();
				break;
			case "lock":
				$this->locked();
				break;
			case 'profile':
				$this->profile();
				break;
			case "add":
				$this->add();
				break;
			case 'del':
				$this->delete();
				break;
			default:
				$this->staff();
				break;
		}
	}
	
	private function staff()
	{
		$this->data['users'] = $this->M_user->sets(array('deleted' => 0, 'active' => 1))->gets();
		$this->data['subview'] = 'cms/user/userlist';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function edit(){
		if($this->id){
			$this->data['obj'] = $this->M_user->set('id',$this->id)->get();
		}
		$this->data['subview'] = 'cms/user/edit';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function save(){
		$data = $this->input->post();
		if($this->id){
			$this->M_user->sets($data)->setPrimary($this->id)->save();
			$_SESSION['system_msg'] = messageDialog("div","success","Cập nhật tài khoản thành công");
		}else{
			$data['password'] = hashpass($data['cfpassword']);
			unset($data['cfpassword']);
			$this->M_user->sets($data)->save();
			$_SESSION['system_msg'] = messageDialog("div","success","Thêm tài khoản thành công");
		}
		return redirect(site_url('admin/user'));
	}

	private function delete(){
		$data = $this->input->post();
		$temp['deleted'] = 1;
		$this->M_user->sets($temp)->setPrimary($data["id"])->save();
		print_r("Cập nhật thành công");
	}

	private function ajax_userInfo(){
		//$user = $this->M_myweb->set('id',$this->id)->set_table('user')->get();
		$user = $this->M_user->set('id',$this->id)->get();
		echo json_encode(array('user'=>$user));
		return;
	}

	private function setpass(){
		if($this->id){
			//$getUser = $this->M_myweb->set('id',$this->id)->set_table('user')->get();
			$getUser = $this->M_user->set('id',$this->id)->get();
			if($getUser){
				$_SESSION['system_msg'] = messageDialog("div","success","Cập nhật mật khẩu thành công");
				$password = hashpass($this->input->post('password'));
				//$this->M_myweb->sets(array('password'=>$password,'updated_info'=>json_encode($updated_info)),$this->id)->setPrimary($this->id)->set_table('user')->save();
				$this->M_user->sets(array('password'=>$password),$this->id)->setPrimary($this->id)->save();
			}else{
				$_SESSION['system_msg'] = messageDialog("div","error","Không thể cập nhật mật khẩu");
			}
		}
		return redirect(site_url('admin/user?token='.$this->data['infoLog']->token));
	}

	private function locked(){
		$data = $this->input->post();
		$temp['active'] = $data['status'];
		$this->M_user->sets($temp)->setPrimary($data["id"])->save();
		print_r("Cập nhật thành công");
	}

	private function profile(){
		$data = $this->input->post();
		$user = $this->M_user->set('id',$data['id'])->get();
		print_r($user->id.'~'.$user->full_name.'~'.$user->email.'~'.$user->phone.'~'.$user->password.'~'.$user->position.'~'.$user->position_description.'~'.$user->description.'~'.$user->thumbnail.'~'.$user->banner);
	}

	private function add(){
		$data = $this->input->post();
		if(!isset($data['id'])){
			if(isset($_FILES['user_img_01']) && $_FILES['user_img_01']['name']!=""){
				$image = do_upload('avatar','user_img_01');	
				$data['thumbnail'] = $image;				
			}
			if(isset($_FILES['user_img_02']) && $_FILES['user_img_02']['name']!=""){
				$image = do_upload('avatar','user_img_02');	
				$data['banner'] = $image;				
			}
			$this->M_user->sets($data)->save();
		}else{
			if(isset($_FILES['user_img_01']) && $_FILES['user_img_01']['name']!=""){
				$image = do_upload('avatar','user_img_01');	
				$data['thumbnail'] = $image;	
				$_SESSION['system']->img = $data['thumbnail'];
			}
			if(isset($_FILES['user_img_02']) && $_FILES['user_img_02']['name']!=""){
				$image = do_upload('avatar','user_img_02');	
				$data['banner'] = $image;				
			}
			$data['password'] = hashpass($data['password']);
			$this->M_user->sets($data)->setPrimary($data["id"])->save();
		}
		return redirect(site_url('admin/user?token='.$this->data['infoLog']->token));
	}

	private function InfoUpd(){
		$data = $this->input->post();
		unset($data['typeUpd']);
		$this->M_user->sets($data)->setPrimary($this->data['infoLog']->logid)->save();
		$_SESSION['system_msg'] = messageDialog("div","success","Update infomation success");
		return redirect(site_url('admin/user?act=profile&id='.$this->id.'&token='.$this->data['infoLog']->token));
	}

	private function PassUpd(){
		$oldpass = $this->input->post('oldpass');
		$newpass = $this->input->post('newpass');
		$getpass = trim($this->M_user->getUserInfo($this->data['infoLog']->id)->password);
		if($getpass == hashpass($oldpass)){
			$password = hashpass($newpass);
			$this->M_user->sets(array('password'=>$password))->setPrimary($this->data['infoLog']->logid)->save();
			echo json_encode(array("msg"=>messageDialog("div","success","Thay đổi mật khẩu thành công"),"rs"=>1));
		}else{
			echo json_encode(array("msg"=>"<span class='text-danger'>Mật khẩu không chính xác</span>","rs"=>0));
		}
		return;
	}
}