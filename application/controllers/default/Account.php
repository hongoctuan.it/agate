<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('obisys');
		$this->Model = $this->M_myweb->set_table('user');
		$this->load->model('default/m_mag');
		$this->load->helper('cookie');
	}
	
	public function index()
	{
		if($this->input->post()){
			$this->login();
		}else{
		    redirect(site_url());
		}
	}

	public function logout(){
		$_SESSION['system_msg'] = messageDialog('div', 'success', 'Đã đăng xuất khỏi hệ thống');
		unset($_SESSION['system']);
		$this->data['infoLog']->img="";
		$this->data['infoLog']->token="";
		$this->data['infoLog']->id="";
		return redirect(site_url());
	}

	public function inforeview(){
		$arr = array();
		$temp = $this->input->post();
		$arr['id'] = 0;
		$arr['title'] = $temp['title'];
		$arr['description'] = $temp['description'];
		$arr['content'] = $temp['content'];
		$arr['type'] = $temp['type'];
		$arr['slug'] = "xem-truoc";
		$arr['hashtag'] = $temp['hashtag'];
		$arr['author'] = $temp['author'];
		$flag = false;
		if(isset($_FILES['thumbnail']) && $_FILES['thumbnail']['name']!=""){
			$image = do_upload('avatar','thumbnail');	
			$arr['thumbnail'] = $image;		
		}else{
			if($temp['file_data_1_src']!=""){
				$arr['thumbnail'] = $temp['file_data_1_src'];
			}else{
				$flag= true;
			}
		}		

		if(isset($_FILES['banner']) && $_FILES['banner']['name']!=""){
			$image = do_upload('avatar','banner');	
			$arr['banner'] = $image;	
		}else{
			if($temp['file_data_2_src']!=""){
				$arr['banner'] = $temp['file_data_2_src'];
			}else{
				$flag= true;
			}	
		}
		if($flag==true){
			print_r("Chưa chọn hình ảnh");
		}
		$_SESSION['viewinfo'] = (object)$arr;
	}

	public function processview(){
		$this->data['mag'] = $_SESSION['viewinfo'];
		if(!isset($this->data['mag']->hashtag[0]->title)){
			$arr_hashtag = explode(",",$this->data['mag']->hashtag);
			$arr_temp = array();
			for($i = 0; $i < count($arr_hashtag); $i++){
				$arr_temp[] = $this->M_myweb->set('id',$arr_hashtag[$i])->set_table('trend_hashtag')->get();
			}
			$this->data['mag']->hashtag =  $arr_temp;			
		}
		$this->data['trend_hashtag'] = $this->M_myweb->set_table('trend_hashtag')->gets();
		$this->data['recommand'] = $this->m_mag->getNewMags(5,$this->data['mag']->type);
		$this->data['hashtag'] = $this->M_myweb->set_table('trend_hashtag')->gets();
		$this->data['subview'] 	= 'default/mag_detail/V_index';
		$this->load->view('default/_main_page',$this->data);
	}

	function login(){
		$data = $this->input->post();
		$get = $this->M_myweb->set('email',$data['email'])->set('password',hashpass($data['password']))->set_table('user')->get();
		if($get){
			$session = array(
				'id'			=> $get->id,
				'logged_in'		=>	true,
				'img'			=>	$get->thumbnail,
				'img2'			=>	$get->banner,
				'phone'			=>	$get->phone,
				'name'			=>	$get->full_name,
				'position'		=>	$get->position,
				'email'			=>	$get->email,
				'role'			=>	$get->role,
				'logid'			=>	$get->id,
				'token'			=>	randomString(30)
			);
			$_SESSION['system'] = (object)$session;
			if(isset($data['savepass']) && $data['savepass']==true){
				$pass= array(
					'name'   => 'remember_password',
					'value'  => $data['password'],                            
					'expire' => '300',                                                                                   
					'secure' => TRUE
				);
				$this->input->set_cookie($pass);
				$email= array(
					'name'   => 'remember_email',
					'value'  => $data['email'],                            
					'expire' => '300',                                                                                   
					'secure' => TRUE
				);
				$this->input->set_cookie($email);
			}else{
				delete_cookie("remember_password");
				delete_cookie("remember_email");
			}
			return redirect(site_url());
		}else{
			$_SESSION['system_msg'] = messageDialog('div', 'error', 'Tên đăng nhập hoặc tài khoản không chính xác');
			return redirect(site_url("dang-nhap"));
		}

	}

	public function info($id){
		$this->data['info'] = $this->Model->set('id',$id)->get('user');
		$this->data['mags'] = $this->m_mag->getUserMags($id);
		$this->data['total_mags'] = count($this->data['mags']);
		$this->data['hashtags'] = $this->M_myweb->set_table('trend_hashtag')->gets();
		$this->data['mags_save'] = $this->m_mag->getSaveMag(100);
		$this->data['subview'] 	= 'default/account/info';
		$this->load->view('default/_main_page',$this->data);

	}

	public function update(){
		$data = $this->input->post();
		$temp['full_name'] = $data['full_name'];
		$temp['phone'] = $data['phone'];
		$temp['address'] = $data['address'];
		$temp['sex'] = $data['sex'];
		$temp['role'] = $data['role'];
		$temp['description'] = $data['description'];
		$this->Model->sets($temp)->setPrimary($data['id'])->save();
		return redirect(site_url("account-info/".$data['id']));
	}

	public function updatepassword(){
		$data = $this->input->post();
		if($data['new_password'] != $data['renew_password']){
			$_SESSION['system_msg'] = messageDialog('div', 'error', 'Mật khẩu nhập lại không giống mật khẩu mới');
			return redirect(site_url("account-info/".$data['id']."?tab=config"));
		}else{
			$info = $this->Model->set('id',$data['id'])->get('user');
			if(hashpass($data['password']) != $info->password){
				$_SESSION['system_msg'] = messageDialog('div', 'error', 'Mật khẩu cũ không chính xác');
				return redirect(site_url("account-info/".$data['id']."?tab=config"));
			}else{
				$_SESSION['system_msg'] = messageDialog('div', 'success', 'Đổi mật khẩu thành công');
				$temp['id'] = $data['id'];
				$temp['password'] = hashpass($data['new_password']);
				$this->Model->sets($temp)->setPrimary($data['id'])->save();
				return redirect(site_url("account-info/".$data['id']."?tab=config"));
			}
		}
		
	}

	public function addpost(){
		$data = $this->input->post();
		$temp['title']= $data['title'];
		$temp['description']= $data['description'];
		$temp['content']= $data['content'];
		$temp['type']= $data['type'];
		$temp['user_id']= $data['id'];
		$temp['active']= 0;
		$temp['time']=time();
		$temp['slug'] = str_replace(array( '\'', '"',',' , ';', '<', '>', '?', '&'),"-",stripUnicode($data['title']));
		if(isset($_FILES['mag_img_01']) && $_FILES['mag_img_01']['name']!=""){
			$image = do_upload('avatar','mag_img_01');	
			$temp['thumbnail'] = $image;				
		}
		if(isset($_FILES['mag_img_02']) && $_FILES['mag_img_02']['name']!=""){
			$image = do_upload('avatar','mag_img_02');	
			$temp['banner'] = $image;				
		}

		$this->M_myweb->set_table('mag')->sets($temp)->save();
		$mag_id = $this->m_mag->getMaxId();
		if(!empty($data['hashtag'])){
			foreach($data['hashtag'] as $item){
				$hashtag['hashtag_id'] = $item;
				$hashtag['mag_id'] = $mag_id;
				$this->M_myweb->set_table('hashtag_mag')->sets($hashtag)->save();
			}
		}
		return redirect(site_url("account-info/".$data['id']));
	}

	public function forgot(){
		if($this->input->post()){
			$data = $this->input->post();
			// $config = Array(
			// 	'protocol' => 'smtp',
			// 	'smtp_host' => 'ssl://smtp.googlemail.com',
			// 	'smtp_port' => 465,
			// 	'smtp_user' => 'admin@agates.live', // change it to yours
			// 	'smtp_pass' => 'tuanthoaQ921801', // change it to yours
			// 	'mailtype' => 'html',
			// 	'charset' => 'utf-8',
			// 	'wordwrap' => TRUE
			// );
			// $this->load->library('email', $config);
			// $password = time();
			// $mail->Username = 'admin@agates.live';
        	// $mail->Password = '12345678a@';
			// $from = "hongoctuan.qc@gmail.com";
			// $to =  $data['email'];
			// $subject = "Agate cấp lại mật khẩu";
			// $message = "Mật khẩu cấp mới của bạn là:".$password;
			// $this->email->set_newline("\r\n");
			// $this->email->from($from);
			// $this->email->to($to);
			// $this->email->subject($subject);
			// $this->email->message($message);


			// $configinfo = $this->M_myweb->set_table('config')->gets(); 
			// // Load PHPMailer library
			// $this->load->library('phpmailer_lib');

			// // PHPMailer object
			// $mail = $this->phpmailer_lib->load();

			// $mail->SMTPAuth = "true";
			// $mail->Username = 'admin@agates.live';
			// $mail->Password = '12345678a@';
			// $mail->SMTPSecure = 'ssl';
			// $mail->CharSet = 'UTF-8';

			// $mail->setFrom('admin@agates.live', 'agate.vn');
			// $mail->addReplyTo('admin@agates.live', 'agate.vn');

			// // Add a recipient
			// $mail->addAddress($data['email']);

			// // Email subject
			// $mail->Subject = $configinfo[6]->value; 

			// // Set email format to HTML
			// $mail->isHTML(true);

			// // Email body content
			// if(isset($data['course_type']) && $data['course_type'] == 1)
			// 	$mailContent = $configinfo[8]->value; 
			// else if(isset($data['course_type']) && $data['course_type'] == 2) 
			// 	$mailContent = $configinfo[7]->value; 
			// else
			// 	$mailContent = $configinfo[5]->value; 
			// $mail->Body = $mailContent;
			// $mail->send();
			// $_SESSION['system_msg'] = messageDialog('div', 'success', 'Đã gửi email mật khẩu mới');
			// $temp['password'] = hashpass($password);
			// $this->Model->sets($temp)->setUpdate("email",$data['email'])->save();

			return redirect(site_url());
		}else{
			return redirect(site_url());
		}
		
	}

	public function register(){
		if($this->input->post()){
			$data = $this->input->post();
			if($data['password'] != $data['re_password']){
				$_SESSION['system_msg_login'] = messageDialog('div', 'error', 'Mật khẩu và mật khẩu nhập lại không chính xác!');
				return redirect(site_url());
			}else{
				$temp['full_name'] = $data['register_name'];
				$temp['email'] = $data['register_email'];
				$temp['phone'] = $data['register_phone'];
				$temp['password'] = hashpass($data['password']);
				$temp['type'] = hashpass($data['register_type']);
				$info = $this->Model->set('email',$data['register_email'])->get();
				if(empty($info->email))
				{
					$this->Model->sets($temp)->save();
					$_SESSION['system_msg_login'] = messageDialog('div', 'success', 'Đăng ký thành công');
					return redirect(site_url());
				}else{
					$_SESSION['system_msg_login'] = messageDialog('div', 'error', 'Email đã tồn tại');
					return redirect(site_url());
				}
				
				
			}
		}else{
			$this->load->view('default/account/register');
		}
	}

	public function update_info_banner(){
		if(isset($_FILES['upload_info_banner']) && $_FILES['upload_info_banner']['name']!=""){
			$image = do_upload('avatar','upload_info_banner');	
			$temp['img_01'] = $image;
			$_SESSION['system']->img2 = $image;
			$this->Model->sets($temp)->setPrimary($this->input->post()['id'])->save();	
		}
	}

	public function update_info_avatar(){
		if(isset($_FILES['upload_info_avatar']) && $_FILES['upload_info_avatar']['name']!=""){
			$image = do_upload('avatar','upload_info_avatar');	
			$temp['img'] = $image;
			$_SESSION['system']->img = $image;
			$this->Model->sets($temp)->setPrimary($this->input->post()['id'])->save();	
			print_r($image);
		}
		
	}
}