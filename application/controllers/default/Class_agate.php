<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class class_agate extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->Model = $this->M_myweb->set_table('class_config');
	} 
	
	public function index()
	{
		$this->data['class'] = $this->Model->gets();
		$this->data['comment_01'] = $this->M_myweb->set_table('comment')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['class'][10]->value))->get();
		$this->data['comment_02'] = $this->M_myweb->set_table('comment')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['class'][11]->value))->get();
		$this->data['comment_03'] = $this->M_myweb->set_table('comment')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['class'][28]->value))->get();
		$this->data['comment_user_01'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['comment_01']->user_id))->get();
		$this->data['comment_user_02'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['comment_02']->user_id))->get();
		$this->data['comment_user_03'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['comment_03']->user_id))->get();
		$this->data['classimg'] = $this->M_myweb->set_table('class_img')->sets(array('deleted' => 0, 'class_id'=> 1))->set_orderby('id','desc')->gets();
		$this->data['schedules'] = $this->M_myweb->set_table('schedule')->sets(array('deleted' => 0, 'active'=> 1))->gets();
		$this->data['home'] = $this->M_myweb->set_table('home')->sets(array('deleted' => 0, 'active' => 1))->gets();
		$this->data['staff_01'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['home'][19]->value))->get();
		$this->data['staff_02'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['home'][20]->value))->get();
		$this->data['staff_03'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['home'][21]->value))->get();
		$this->data['subview'] 	= 'default/class/V_class';
		$this->load->view('default/_main_page',$this->data);
	}
}