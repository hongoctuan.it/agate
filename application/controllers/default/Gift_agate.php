<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class gift_agate extends MY_Controller {

	public function __construct(){
		parent::__construct();
	}
	
	public function index()
	{
		$this->data['giftConfig'] = $this->M_myweb->set_table('gift_config')->gets();
		$this->data['gifts'] = $this->M_myweb->set_table('gift')->sets(array('active'=>1,'deleted'=>0))->gets();
		$this->data['subview'] 	= 'default/gift/V_index';
		$this->load->view('default/_main_page',$this->data);
	}
	public function detail($slug)
	{
		$this->data['gift'] = $this->M_myweb->set_table('gift')->set('slug',$slug)->get();
		$this->data['subview'] 	= 'default/gift/V_giftdetail';
		$this->load->view('default/_main_page',$this->data);
	}
}