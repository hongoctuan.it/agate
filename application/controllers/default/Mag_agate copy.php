<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mag_agate extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->Model = $this->M_myweb->set_table('mag');
		$this->load->model('default/m_mag');
		$this->load->model('default/m_search');
	}
	
	public function index()
	{
		$this->data['mags'] = $this->Model->gets();
		$this->data['hashtag'] = $this->Model->gets($this->data['mags'][0]->id);
		$this->data['trend_hashtag'] = $this->M_myweb->set_table('trend_hashtag')->gets();
		$this->data['comments'] = $this->m_mag->getComment($this->data['mags'][0]->id);
		$this->data['subview'] 	= 'default/mag/V_index';
		$this->data['more'] = 1;
		$this->load->view('default/_main_page',$this->data);
	}
	public function category($slug)
	{
		$page = $this->input->get('page');
		if(!empty($page)){
			$this->data['page'] = $this->input->get('page');
		}else{
			$this->data['page'] = 0;
		}
		$this->data['more'] = 0;
		if($slug=="teen"){
			$this->data['savemags'] = $this->m_mag->getSaveMag(10);
			$this->data['mags'] = $this->m_mag->getMags(1,$this->data['page']);
			if(!empty($this->data['mags'][0]->id))
				$this->data['hashtag'] = $this->Model->gets($this->data['mags'][0]->id);
			else
				$this->data['hashtag'] = $this->Model->gets();
			$this->data['type']='teen';
			$total =$this->m_mag->getTotal(1);
			if(count($this->data['mags']) <= $total){
				$this->data['more'] = 1;
			}
		}
		else if($slug=="mom"){
			$this->data['savemags'] = $this->m_mag->getSaveMag(10);
			$this->data['mags'] = $this->m_mag->getMags(2,$this->data['page']);
			if(!empty($this->data['mags'][0]->id))
				$this->data['hashtag'] = $this->Model->gets($this->data['mags'][0]->id);
			else
				$this->data['hashtag'] = $this->Model->gets();
			$this->data['type']='mon';
			$total =$this->m_mag->getTotal(2);
			if(count($this->data['mags']) <= $total){
				$this->data['more'] = 1;
			}
		}
		// else{
		// 	$this->data['mag'] = $this->m_mag->getMag($slug);
		// 	$this->data['recommand'] = $this->m_mag->getNewMags(5,$this->data['mag']->type);
		// 	$this->data['hashtag'] = $this->Model->gets($this->data['mag']->id);
		// }
		if(isset($this->data['mags'])){
			$this->data['trend_hashtag'] = $this->M_myweb->set_table('trend_hashtag')->gets();
			$this->data['subview'] 	= 'default/mag/V_index';
		}
		// else{
		// 	$this->data['trend_hashtag'] = $this->M_myweb->set_table('trend_hashtag')->gets();
		// 	$this->data['subview'] 	= 'default/mag_detail/V_index';
		// }
		$this->data['page']=$this->data['page']+1;
		$this->load->view('default/_main_page',$this->data);
	}

	

	public function search()
	{
		$data = $this->input->post();
		$this->data['mags'] = $this->m_search->getSearch($data['search']);
		$this->data['trend_hashtag'] = $this->M_myweb->set_table('trend_hashtag')->gets();
		$this->data['subview'] 	= 'default/mag/V_index';
		$this->data['more'] = 0;
		$this->load->view('default/_main_page',$this->data);
		
	}

	public function hashtag($hashtag)
	{	
		$this->data['mags'] = $this->m_search->getHashtag($hashtag);
		$this->data['savemags'] = $this->m_mag->getSaveMag($this->data['infoLog']->id);
		$this->data['trend_hashtag'] = $this->M_myweb->set_table('trend_hashtag')->gets();
		$this->data['subview'] 	= 'default/mag/V_index';
		$this->data['more'] = 0;
		$this->load->view('default/_main_page',$this->data);
		
	}

	public function rating()
	{
		$data = $this->input->post();
		$this->M_myweb->set_table('rating')->sets($data)->save();
		echo "Cảm ơn bạn đã đánh giá!"	;
	}
}