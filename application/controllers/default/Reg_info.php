<?php
defined('BASEPATH') or exit('No direct script access allowed');

class reg_info extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->Model = $this->M_myweb->set_table('reg_info');
	}

	public function index()
	{
		$data = $this->input->post();
		if(isset($data['course_type'])){
			if($data['course_type']=="Học Sinh"){
				$data['course_type']=1;
			}else{
				$data['course_type']=2;
			}
		}
		$data['time'] = time();
		$this->Model->sets($data)->save();
		 $configinfo = $this->M_myweb->set_table('config')->gets(); 
		// Load PHPMailer library
        $this->load->library('phpmailer_lib');

        // PHPMailer object
        $mail = $this->phpmailer_lib->load();

        $mail->SMTPAuth = "true";
        $mail->Username = 'admin@agates.live';
        $mail->Password = '12345678a@';
        $mail->SMTPSecure = 'ssl';
        $mail->CharSet = 'UTF-8';

        $mail->setFrom('admin@agates.live', 'agate.vn');
        $mail->addReplyTo('admin@agates.live', 'agate.vn');

        // Add a recipient
        $mail->addAddress($data['email']);

        // Email subject
        $mail->Subject = $configinfo[6]->value; 

        // Set email format to HTML
        $mail->isHTML(true);

        // Email body content
        if(isset($data['course_type']) && $data['course_type'] == 1)
            $mailContent = $configinfo[8]->value; 
        else if(isset($data['course_type']) && $data['course_type'] == 2) 
            $mailContent = $configinfo[7]->value; 
        else
            $mailContent = $configinfo[5]->value; 
        $mail->Body = $mailContent;
        $mail->send();
        
        ///////////////////////-----------------------
        $mail->clearAllRecipients( );
        $mail->setFrom('admin@agates.live', 'agate.vn');
        $mail->addReplyTo('admin@agates.live', 'agate.vn');

        // Add a recipient
        $mail->addAddress($configinfo[4]->value);

        // Email subject
        $mail->Subject = "Thông báo user đăng kí nhận thông tư vấn"; 

        // Set email format to HTML
        $mail->isHTML(true);

        // Email body content
        $message = "<p>Email đăng kí: ".$data['email']."</p>"; 
        if(isset($data['name'])){ 
          $message = $message."<p>Họ tên đăng kí:".$data['name']."</p>"; 
          $message = $message."<p>Điện thoại đăng kí:".$data['phone']."</p>"; 
          if($data['course_type'] == 1) 
            $message = $message."<p>Phụ huynh/Học sinh đăng kí: Học Sinh</p>"; 
          else 
            $message = $message."<p>Phụ huynh/Học sinh đăng kí: Phụ Huynh</p>"; 
        } 
        $mail->Body = $message;
        $mail->send();
        
	}
}
