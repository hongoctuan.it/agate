<?php
defined('BASEPATH') or exit('No direct script access allowed');

class staff_agate extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->Model= $this->M_myweb->set_table('staff_config'); 
	}

	public function index()
	{
		$arr_founder= array();
		$arr_member= array();
		$this->data['staff'] = $this->Model->gets();
		$founders = $this->Model->sets(array('position' => 'founder', 'active' => 1))->gets();
		$staffs = $this->Model->sets(array('position' => 'staff', 'active' => 1))->gets();

		$this->data['partner_01'] = $this->M_myweb->set_table('partner')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['staff'][0]->value))->get();
		$this->data['partner_02'] = $this->M_myweb->set_table('partner')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['staff'][1]->value))->get();
		
		$this->data['member_01'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['staff'][5]->value))->get();
		$this->data['member_02'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['staff'][6]->value))->get();
		$this->data['member_03'] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$this->data['staff'][7]->value))->get();
		foreach($staffs as $item){
			$arr_staff[] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$item->value))->get();
		}
		foreach($founders as $item){
			$arr_founder[] = $this->M_myweb->set_table('user')->sets(array('deleted' => 0, 'active' => 1, 'id'=>$item->value))->get();
		}
		$this->data['classimg'] = $this->M_myweb->set_table('class_img')->sets(array('deleted' => 0, 'home'=> 1))->gets();
		$this->data['founders'] = $arr_founder;
		$this->data['staffs'] = $arr_staff;
		$this->data['subview'] = 'default/staff/V_index';
		$this->load->view('default/_main_page', $this->data);
	}

}