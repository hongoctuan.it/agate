<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tracking extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('obisys');
		$this->Model = $this->M_myweb->set_table('tracking');
	}
	
	public function index()
	{
		if(!empty($this->act)){
			$data['code'] = $this->act;
			$data['time'] = time();
			$this->Model->sets($data)->save();
		}else{
			$data['code'] = "home";
			$data['time'] = time();
			$this->Model->sets($data)->save();
		}
	}
}