<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class MY_Controller extends CI_Controller {

    public $data = array();
	public function __construct() {
		parent::__construct();
		$this->load->helper('obisys');
        $this->load->model('M_myweb');
		$sys = new stdClass();
		$sys->token = '';
		$sys->img='';
		$sys->id='';
		$_SESSION['system'] = isset($_SESSION['system'])?$_SESSION['system']:$sys;
		$this->data['infoLog'] = $_SESSION['system'];
		$this->data['logo'] = $this->M_myweb->set_table('config')->set('title',"logo")->get()->value;
		$this->data['img'] = $this->data['infoLog']->img==''?base_url('assets/public/avatar/no-avatar.png'):$this->data['infoLog']->img;
		$this->act = isset($_GET['act'])?$_GET['act']:'';
		$this->data['footer_description'] = $this->M_myweb->set_table('config')->set('title',"footer_description")->get()->value;;
	}
}