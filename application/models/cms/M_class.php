<?php 
class M_class extends CI_model
{
	public function getMaxId()
	{
		$maxid = 0;
        $row = $this->db->query('SELECT MAX(id) AS `maxid` FROM `class_img`')->row();
        if ($row) {
            $maxid = $row->maxid; 
        }
        return $maxid;
	}

    public function updateAll($page)
	{
        $this->db->set($page, 0, FALSE);
        $this->db->set('time', time());
        $this->db->update('class_img');
	}

}
?>