<?php 
class M_comment extends CI_model
{
	public function getComment(){
		$arr = array();
		$this->db->where("deleted",0);
		$query = $this->db->get("comment");
		$this->db->order_by("id", "asc");
        foreach($query->result() as $row)
		{  
            $row->user = $this->getUser($row->user_id);
			$row->mag = $this->getMag($row->mag_id);
            $arr[] = $row;
		}
		return $arr;
	}

	public function getCommentId($id){
		$arr = array();
		$this->db->where("deleted",0);
		$this->db->where("id",$id);
		$query = $this->db->get("comment");
		$this->db->order_by("id", "asc");
        foreach($query->result() as $row)
		{  
            $row->user = $this->getUser($row->user_id);
			$row->mag = $this->getMag($row->mag_id);
            $arr[] = $row;
		}
		return $arr;
	}

	public function getUser($key){
		
		$this->db->where("id", $key);
		$this->db->order_by("id", "asc");
		$query = $this->db->get("user");
		return $query->row();
	}

	public function getMag($key){
		$arr = array();
		$this->db->where("id", $key);
		$this->db->order_by("id", "asc");
		$query = $this->db->get("mag");
		return $query->row();
	}

}
?>