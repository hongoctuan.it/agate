<?php 
class M_gift extends CI_model
{
	public function getMaxId()
	{
		$maxid = 0;
        $row = $this->db->query('SELECT MAX(id) AS `maxid` FROM `gift`')->row();
        if ($row) {
            $maxid = $row->maxid; 
        }
        return $maxid;
	}
}
?>