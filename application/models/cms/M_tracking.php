<?php 
class M_tracking extends CI_model
{
	public function getTotal(){
		$arr = array();
		$query = $this->db->get("tracking");
		return $query->num_rows();;
	}

	public function getCurrentDay(){
		$start = date("d-m-Y").' '."00:00:00";
		$end = date("d-m-Y").' '."23:59:59";
		$start = strtotime($start);
		$end = strtotime($end);
		$this->db->where("time >=",$start);
		$this->db->where("time <=",$end);
		$query = $this->db->get("tracking");
		return $query->num_rows();
	}


	public function getCurrentYear(){
		$year = date('Y');
		$start = "01-01-".date("Y").' '."00:00:00";
		$end = "31-12-".date("Y").' '."23:59:59";
		$start = strtotime($start);
		$end = strtotime($end);
		$this->db->where("time >=",$start);
		$this->db->where("time <=",$end);
		$query = $this->db->get("tracking");
		return $query->num_rows();
	}


	public function getCurrentMonth(){
		$arr = array();
		$month = date('m');
		$start = 0;
		$end = 0;
		switch($month){
			case 1:
				$date = "01-01-".date("Y");
				$start = strtotime($date);
				$date = "31-01-".date("Y");
				$end = strtotime($date);
				break;
			case 2:
				$date = "01-02-".date("Y");
				$start = strtotime($date);
				$date = "29-02-".date("Y");
				$end = strtotime($date);
				break;
			case 3:
				$date = "01-03-".date("Y");
				$start = strtotime($date);
				$date = "31-03-".date("Y");
				$end = strtotime($date);
				break;
			case 4:
				$date = "01-04-".date("Y");
				$start = strtotime($date);
				$date = "30-04-".date("Y");
				$end = strtotime($date);
				break;
			case 5:
				$date = "01-05-".date("Y");
				$start = strtotime($date);
				$date = "31-05-".date("Y");
				$end = strtotime($date);
				break;
			case 6:
				$date = "01-06-".date("Y");
				$start = strtotime($date);
				$date = "30-06-".date("Y");
				$end = strtotime($date);
				break;
			case 7:
				$date = "01-07-".date("Y");
				$start = strtotime($date);
				$date = "31-07-".date("Y");
				$end = strtotime($date);
				break;
			case 8:
				$date = "01-08-".date("Y");
				$start = strtotime($date);
				$date = "31-08-".date("Y");
				$end = strtotime($date);
				break;
			case 9:
				$date = "01-09-".date("Y");
				$start = strtotime($date);
				$date = "30-09-".date("Y");
				$end = strtotime($date);
				break;
			case 10:
				$date = "01-10-".date("Y");
				$start = strtotime($date);
				$date = "31-10-".date("Y");
				$end = strtotime($date);
				break;
			case 11:
				$date = "01-11-".date("Y");
				$start = strtotime($date);
				$date = "30-11-".date("Y");
				$end = strtotime($date);
				break;
			case 12:
				$date = "01-12-".date("Y");
				$start = strtotime($date);
				$date = "31-12-".date("Y");
				$end = strtotime($date);
				break;
		}
		$this->db->where("time >=",$start);
		$this->db->where("time <=",$end);
		$query = $this->db->get("tracking");
		return $query->num_rows();;
	}
	public function getSpeedChart(){
		$arr = array();
		$this->db->group_by('code');
		$query = $this->db->get("tracking");
        foreach($query->result() as $row)
		{  
			$row->month_01 = $this->countMonth($row->code,1);
			$row->month_02 = $this->countMonth($row->code,2);
			$row->month_03 = $this->countMonth($row->code,3);
			$row->month_04 = $this->countMonth($row->code,4);
			$row->month_05 = $this->countMonth($row->code,5);
			$row->month_06 = $this->countMonth($row->code,6);
			$row->month_07 = $this->countMonth($row->code,7);
			$row->month_08 = $this->countMonth($row->code,8);
			$row->month_09 = $this->countMonth($row->code,9);
			$row->month_10 = $this->countMonth($row->code,10);
			$row->month_11 = $this->countMonth($row->code,11);
			$row->month_12 = $this->countMonth($row->code,12);
			$row->color = $this->randomColor();
            $arr[] = $row;
		}
		return $arr;
	}

	public function countMonth($code = false,$month){
		$start = 0;
		$end = 0;
		switch($month){
			case 1:
				$date = "01-01-".date("Y");
				$start = strtotime($date);
				$date = "31-01-".date("Y");
				$end = strtotime($date);
				break;
			case 2:
				$date = "01-02-".date("Y");
				$start = strtotime($date);
				$date = "29-02-".date("Y");
				$end = strtotime($date);
				break;
			case 3:
				$date = "01-03-".date("Y");
				$start = strtotime($date);
				$date = "31-03-".date("Y");
				$end = strtotime($date);
				break;
			case 4:
				$date = "01-04-".date("Y");
				$start = strtotime($date);
				$date = "30-04-".date("Y");
				$end = strtotime($date);
				break;
			case 5:
				$date = "01-05-".date("Y");
				$start = strtotime($date);
				$date = "31-05-".date("Y");
				$end = strtotime($date);
				break;
			case 6:
				$date = "01-06-".date("Y");
				$start = strtotime($date);
				$date = "30-06-".date("Y");
				$end = strtotime($date);
				break;
			case 7:
				$date = "01-07-".date("Y");
				$start = strtotime($date);
				$date = "31-07-".date("Y");
				$end = strtotime($date);
				break;
			case 8:
				$date = "01-08-".date("Y");
				$start = strtotime($date);
				$date = "31-08-".date("Y");
				$end = strtotime($date);
				break;
			case 9:
				$date = "01-09-".date("Y");
				$start = strtotime($date);
				$date = "30-09-".date("Y");
				$end = strtotime($date);
				break;
			case 10:
				$date = "01-10-".date("Y");
				$start = strtotime($date);
				$date = "31-10-".date("Y");
				$end = strtotime($date);
				break;
			case 11:
				$date = "01-11-".date("Y");
				$start = strtotime($date);
				$date = "30-11-".date("Y");
				$end = strtotime($date);
				break;
			case 12:
				$date = "01-12-".date("Y");
				$start = strtotime($date);
				$date = "31-12-".date("Y");
				$end = strtotime($date);
				break;
		}
		$arr = array();
		$this->db->where("time >=",$start);
		$this->db->where("time <=",$end);
		if($code)
			$this->db->where('code', $code);
		$query = $this->db->get("tracking");
		return $query->num_rows();
	}


	public function getPieChart(){
	
		$arr = array();
		$this->db->group_by('code');
		$query = $this->db->get("tracking");
        foreach($query->result() as $row)
		{  
			$row->number = $this->countCode($row->code);
			$row->color = $this->randomColor();
            $arr[] = $row;
		}
		return $arr;
	}

	public function countCode($code){
		$arr = array();
		$this->db->where('code', $code);
		$query = $this->db->get("tracking");
		return $query->num_rows();
	}

	function randomColor(){
		$rcolor = '#';
		for($i=0;$i<6;$i++){
		$rNumber = rand(0,15);
		switch ($rNumber) {
		case 10:$rNumber='A';
		break;
		case 11:$rNumber='B';
		break;
		case 12:$rNumber='C';
		break;
		case 13:$rNumber='D';
		break;
		case 14:$rNumber='E';
		break;
		case 15:$rNumber='F';
		break;
	}
		$rcolor .= $rNumber;
	}
		// $rcolor = '#FF0000';
		return $rcolor;
	}
}
?>