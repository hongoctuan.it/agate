<?php 
class M_mag extends CI_model
{

	public function getSaveMag($limit)
    {
		$arr= array();
		if(!empty($this->data['infoLog']->id)){
			$this->db->where('user_id',$this->data['infoLog']->id);
			$this->db->where('status',1);
			$query = $this->db->get('mag_save');
			foreach($query->result() as $row)
			{   
				$arr[]=$this->getMagId($row->mag_id);
			}
		}else{
			//lay danh sach 5 bai viet moi nhat
			$this->db->where("deleted",0);
			$this->db->where("active",1);
			$this->db->limit($limit,0);
			$query = $this->db->get("mag");
			foreach($query->result() as $row)
			{   
				$arr[]=$this->getMagId($row->id);
			}
		}
		return $arr;
    }
	
	public function getTotal($type){
		$query = $this->db->query('SELECT * FROM mag Where `type` = '.$type.' and active = 1 and deleted = 0');
		return $query->num_rows();
	}
	

    public function getMags($key,$page){
		$arr = array();
		$limit = 10*$page;
		$this->db->where("type", $key);
		$this->db->where("deleted",0);
        $this->db->where("active",1);
		$this->db->limit($limit,0);
		$query = $this->db->get("mag");
		$this->db->order_by("id", "asc");
        foreach($query->result() as $row)
		{  
            $row->hashtag = $this->getHashtagId($row->id);
            $arr[]=$row;
		}
		return $arr;
	}

	public function getUserMags($user_id){
		$arr = array();
		// $limit = 10*$page;
		$this->db->where("user_id", $user_id);
		$this->db->where("deleted",0);
        // $this->db->where("active",1);
		// $this->db->limit($limit,0);
		$this->db->order_by("id", "desc");
		$query = $this->db->get("mag");
        foreach($query->result() as $row)
		{  
            $row->hashtag = $this->getHashtagId($row->id);
            $arr[]=$row;
		}
		return $arr;
	}

	public function updatesagepost($mag_id, $user_id,$status){
		$arr = array();
		$this->db->where("user_id", $user_id);
		$this->db->where("mag_id",$mag_id);
		$this->db->set('status',$status);
		$this->db->update('mag_save');

	}

	
	public function getNewMags($limit, $type){
		$arr = array();
		$this->db->where("type", $type);
		$this->db->limit($limit,0);
		$this->db->where("deleted",0);
        $this->db->where("active",1);
		$query = $this->db->get("mag");
		$this->db->order_by("id", "asc");
        foreach($query->result() as $row)
		{  
            $row->hashtag = $this->getHashtagId($row->id);
            $arr[]=$row;
		}
		return $arr;
	}

	public function getMaxId()
	{
		$maxid = 0;
        $row = $this->db->query('SELECT MAX(id) AS `maxid` FROM `mag`')->row();
        if ($row) {
            $maxid = $row->maxid; 
        }
        return $maxid;
	}

    public function getMag($key){
		$this->db->where("slug", $key);
		$this->db->where("deleted",0);
        $this->db->where("active",1);
		$query = $this->db->get("mag");
		$this->db->order_by("id", "asc");
        foreach($query->result() as $row)
		{  
            $row->hashtag = $this->getHashtagId($row->id);
            return $row;
		}
		return false;
	}

	public function getMagId($key){
		$this->db->where("id", $key);
		$this->db->where("deleted",0);
        $this->db->where("active",1);
		$query = $this->db->get("mag");
		$this->db->order_by("id", "asc");
        foreach($query->result() as $row)
		{  
            $row->hashtag = $this->getHashtagId($row->id);
            return $row;
		}
		return false;
	}


	public function getHashtagId($key){
		$arr = array();
		$this->db->where("mag_id", $key);
		$this->db->where("deleted", 0);
		$query = $this->db->get("hashtag_mag");
		$this->db->order_by("id", "asc");
        foreach($query->result() as $row)
		{  
             $arr[] = $this->getHashtagDetail($row->hashtag_id);
		}
		return $arr;
	}

	public function getHashtagDetail($key){
		$this->db->where("id", $key);
		$this->db->where("deleted",0);
        $this->db->where("active",1);
		$query = $this->db->get("trend_hashtag");
		$this->db->order_by("id", "asc");
        foreach($query->result() as $row)
		{  
            return $row;
		}
		return false;
	}
}
?>
