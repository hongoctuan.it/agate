<?php 
class M_search extends CI_model
{
    public function getSearch($key){
		$arr = array();
		$this->db->like("title", $key);
		$this->db->where("deleted",0);
        $this->db->where("active",1);
		$query = $this->db->get("mag");
		$this->db->order_by("id", "asc");
        foreach($query->result() as $row)
		{  
            $arr[]=$row;
		}
		return $arr;
	}

	public function getHashtag($key){
		$arr = array();
		$this->db->where("hashtag_id", $key);
		$query = $this->db->get("hashtag_mag");
		$this->db->order_by("id", "asc");
        foreach($query->result() as $row)
		{  
            $arr[]=$this->getMag($row->mag_id);
		}
		return $arr;
	}

	public function getMag($key){
		$this->db->where("id", $key);
		$this->db->where("deleted",0);
        $this->db->where("active",1);
		$query = $this->db->get("mag");
		$this->db->order_by("id", "asc");
        foreach($query->result() as $row)
		{  
			$row->hashtag = $this->getHashtagId($row->id);
            return $row;
		}
	}
	public function getHashtagId($key){
		$arr = array();
		$this->db->where("mag_id", $key);
		$query = $this->db->get("hashtag_mag");
		$this->db->order_by("id", "asc");
        foreach($query->result() as $row)
		{  
             $arr[] = $this->getHashtagDetail($row->hashtag_id);
		}
		return $arr;
	}

	public function getHashtagDetail($key){
		$this->db->where("id", $key);
		$this->db->where("deleted",0);
        $this->db->where("active",1);
		$query = $this->db->get("trend_hashtag");
		$this->db->order_by("id", "asc");
        foreach($query->result() as $row)
		{  
            return $row;
		}
		return false;
	}
}
?>