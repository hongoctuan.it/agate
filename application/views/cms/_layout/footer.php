<footer class="footer footer-black  footer-white ">
        <div class="container-fluid">
          <div class="row">
            <div class="credits ml-auto">
              <span class="copyright">
                © <script>
                  document.write(new Date().getFullYear())
                </script>, made with <i class="fa fa-heart heart"></i> by Agate
              </span>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  
  <script src="<?php echo site_url('statics/cms/js/main.js')?>"></script>

  <?php if($this->uri->segment(2)!="mag"):?>
    <script src="<?php echo site_url('statics/cms/assets/js/core/jquery.min.js')?>"></script>
  <?php endif;?>


  <script src="<?php echo site_url('statics/cms/assets/js/core/popper.min.js')?>"></script>
  <script src="<?php echo site_url('statics/cms/assets/js/core/bootstrap.min.js')?>"></script>
  <script src="<?php echo site_url('statics/cms/assets/js/plugins/perfect-scrollbar.jquery.min.js')?>"></script>
  
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chart JS -->
  <script src="<?php echo site_url('statics/cms/assets/js/plugins/chartjs.min.js')?>"></script>

  <!--  Notifications Plugin    -->
  <script src="<?php echo site_url('statics/cms/assets/js/plugins/bootstrap-notify.js')?>"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="<?php echo site_url('statics/cms/assets/js/paper-dashboard.min.js?v=2.0.1')?>"></script>
  <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
  <script src="<?php echo site_url('statics/cms/assets/demo/demo.js')?>"></script>
  <script>
    $(document).ready(function() {
      // Javascript method's body can be found in assets/assets-for-demo/js/demo.js
      demo.initChartsPages();
    });
  </script>
  <script src="<?php echo site_url('statics/default/assets/js/simple-datatables.js')?>"></script>
<script>
    let table1 = document.querySelector('#table1');
    let dataTable = new simpleDatatables.DataTable(table1);
</script>
</body>

</html>