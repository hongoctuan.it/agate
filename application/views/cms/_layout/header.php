<!--
=========================================================
* Paper Dashboard 2 - v2.0.1
=========================================================

* Product Page: https://www.creative-tim.com/product/paper-dashboard-2
* Copyright 2020 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Paper Dashboard 2 by Creative Tim
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  
  <link href="<?php echo site_url('statics/cms/assets/css/bootstrap.min.css')?>" rel="stylesheet" />
  <link href="<?php echo site_url('statics/cms/assets/css/paper-dashboard.css?v=2.0.1')?>" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="<?php echo site_url('statics/cms/assets/demo/demo.css')?>" rel="stylesheet" />
  <link href="<?php echo site_url('statics/cms/css/main.css')?>" rel="stylesheet" />
  <?php if($this->uri->segment(2)!="mag"):?>
    <script type="text/javascript" src="<?php echo site_url('filemanager/ckeditor/ckeditor.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo site_url('filemanager/ckfinder/ckfinder.js'); ?>"></script>
  <?php endif;?>
  <link rel="stylesheet" href="<?php echo site_url('statics/default/assets/css/bootstrap.css')?>">
<link rel="stylesheet" href="<?php echo site_url('statics/default/assets/css/style.css')?>">
<link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Lexend+Deca:wght@100&family=Lexend:wght@400;700&display=swap" rel="stylesheet">




</head>


<body class="" style="font-family: 'Lexend', sans-serif;">
  <div class="wrapper ">
    <div class="sidebar" data-color="white" data-active-color="danger">
      <div class="logo">
        <a href="https://www.creative-tim.com" class="simple-text logo-mini">
          <div class="logo-image-small" style="width:32px; height:32px">
            <img src="<?php echo base_url('assets/public/avatar/'.$infoLog->img)?>">
          </div>
        </a>
        <a href="#" class="simple-text logo-normal">
          <?php echo $infoLog->name?>
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <?php echo ($this->uri->segment(2) ==  'home') ? '<li class="active ">' : '<li>'?>
            <a href="<?php echo site_url('admin/home')?>">
              <i class="nc-icon nc-globe"></i>
              <p>Trang Chủ</p>
            </a>
          </li>
          <?php echo ($this->uri->segment(2) ==  'agate_class') ? '<li class="active ">' : '<li>'?>
            <a href="<?php echo site_url('admin/agate_class')?>">
              <i class="nc-icon nc-globe"></i>
              <p>Chương Trình</p>
            </a>
          </li>
          <?php echo ($this->uri->segment(2) ==  'agate_gift') ? '<li class="active ">' : '<li>'?>
            <a href="<?php echo site_url('admin/agate_gift')?>">
              <i class="nc-icon nc-globe"></i>
              <p>Gift</p>
            </a>
          </li>
          <?php echo ($this->uri->segment(2) ==  'agate_staff') ? '<li class="active ">' : '<li>'?>
            <a href="<?php echo site_url('admin/agate_staff')?>">
              <i class="nc-icon nc-globe"></i>
              <p>Đội Ngũ</p>
            </a>
          </li>
          <?php echo ($this->uri->segment(2) ==  'mag') ? '<li class="active ">' : '<li>'?>
            <a href="<?php echo site_url('admin/mag')?>">
              <i class="nc-icon nc-tile-56"></i>
              <p>Magazine</p>
            </a>
          </li>
          <?php echo ($this->uri->segment(2) ==  'hashtag') ? '<li class="active ">' : '<li>'?>
            <a href="<?php echo site_url('admin/hashtag')?>">
              <i class="nc-icon nc-tile-56"></i>
              <p>HashTag</p>
            </a>
          </li>
          <?php echo ($this->uri->segment(2) ==  'member') ? '<li class="active ">' : '<li>'?>
            <a href="<?php echo site_url('admin/member')?>">
              <i class="nc-icon nc-badge"></i>
              <p>Học viên</p>
            </a>
          </li>
          <?php echo ($this->uri->segment(2) ==  'member') ? '<li class="active ">' : '<li>'?>
            <a href="<?php echo site_url('admin/partner')?>">
              <i class="nc-icon nc-badge"></i>
              <p>Đối tác</p>
            </a>
          </li>
          <?php echo ($this->uri->segment(2) ==  'agate_schedule') ? '<li class="active ">' : '<li>'?>
            <a href="<?php echo site_url('admin/agate_schedule')?>">
              <i class="nc-icon nc-single-copy-04"></i>
              <p>Thời Khoá Biểu</p>
            </a>
          </li>
          <?php echo ($this->uri->segment(2) ==  'agate_comment') ? '<li class="active ">' : '<li>'?>
            <a href="<?php echo site_url('admin/agate_comment')?>">
              <i class="nc-icon nc-single-copy-04"></i>
              <p>Testimonials</p>
            </a>
          </li>
          <?php echo ($this->uri->segment(2) ==  'agate_regis') ? '<li class="active ">' : '<li>'?>
            <a href="<?php echo site_url('admin/agate_regis')?>">
              <i class="nc-icon nc-globe"></i>
              <p>Subscriber</p>
            </a>
          </li>
          <?php echo ($this->uri->segment(2) ==  'user') ? '<li class="active ">' : '<li>'?>
            <a href="<?php echo site_url('admin/user')?>">
              <i class="nc-icon nc-single-02"></i>
              <p>Tài khoản</p>
            </a>
          </li>
          <?php echo ($this->uri->segment(2) ==  'tracking') ? '<li class="active ">' : '<li>'?>
            <a href="<?php echo site_url('admin/tracking')?>">
              <i class="nc-icon nc-chart-bar-32"></i>
              <p>Thống Kê</p>
            </a>
          </li>
          <?php echo ($this->uri->segment(2) ==  'agate_comment') ? '<li class="active ">' : '<li>'?>
            <a href="<?php echo site_url('admin/red_up')?>">
              <i class="nc-icon nc-single-copy-04"></i>
              <p>Redirect Upload</p>
            </a>
          </li>
          <?php echo ($this->uri->segment(2) ==  'agate_config') ? '<li class="active ">' : '<li>'?>
            <a href="<?php echo site_url('admin/agate_config')?>">
              <i class="nc-icon nc-settings-gear-65"></i>
              <p>Cấu hình chung</p>
            </a>
          </li>
          <li>
            <a href="<?php echo site_url('logout')?>">
              <i class="nc-icon nc-button-power"></i>
              <p>Đăng xuất</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">

