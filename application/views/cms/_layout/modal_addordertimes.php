<div class="modal fade" id="addordertimesModal" tabindex="-1" role="dialog" aria-labelledby="addordertimesModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Thêm lần</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php echo form_open_multipart("",array('autocomplete'=>"off",'id'=>"userformAddOrderTimes"));?>
                <div class="modal-body">
                    <div class="col-md-12 mb-3">
                        <input type="hidden" name="user" value="<?php echo $infoLog->id ?>" >
                        <input type="hidden" name="order_id" value="<?php echo $obj->id ?>" >
                        <label for="validationCustom01">Ngày làm</label>
                        <input type="text" class="form-control" name="date" id="datepicker2" placeholder="Ngày làm" value="" required>
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="validationCustom01">Ghi chú</label>
                        <textarea name="note" id="exampleText" class="form-control"></textarea>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary">Xác nhận</button>
                </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
