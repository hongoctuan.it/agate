<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link href="<?php echo site_url("statics/cms/style.css")?>" rel="stylesheet" id="bootstrap-css">

<!------ Include the above in your HEAD tag ---------->

<div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->
    <!-- Icon -->
    <div class="fadeIn first">
      <img src="<?php echo site_url("assets/public/avatar/AGATE_footer.png")?>" id="icon" alt="User Icon" />
    </div>

    <!-- Login Form -->
	<?php if(isset($_SESSION['system_msg'])){
				echo $_SESSION['system_msg'];unset($_SESSION['system_msg']);
	}?>
	<form class="login100-form validate-form p-b-33 p-t-5" action="<?php echo site_url('login')?>" method="post">
      <input type="email" id="email" class="fadeIn second" name="email" placeholder="Email">
      <input type="password" id="password" class="fadeIn third" name="password" placeholder="Mật khẩu">
      <input type="submit" class="fadeIn fourth" value="Log In">
    </form>
    <!-- Remind Passowrd -->
    <div id="formFooter">
    </div>
  </div>
</div>