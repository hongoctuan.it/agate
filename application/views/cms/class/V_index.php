<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="<?php echo site_url('statics/cms/js/common.js')?>"></script>
<script>
  $(document).ready(function(){
  $("#benefit_submit").click(function(){
    var form_data = new FormData();
      form_data.append('benefit_title', $( "#benefit_title" ).val());
      form_data.append('benefit_item_name_01', $( "#benefit_item_name_01" ).val());
      form_data.append('benefit_item_name_02', $( "#benefit_item_name_02" ).val());
      form_data.append('benefit_item_name_03', $( "#benefit_item_name_03" ).val());
      form_data.append('benefit_item_name_04', $( "#benefit_item_name_04" ).val());
      form_data.append('benefit_des_01', $( "#benefit_des_01" ).val());
      form_data.append('benefit_des_02', $( "#benefit_des_02" ).val());
      form_data.append('benefit_des_03', $( "#benefit_des_03" ).val());
      form_data.append('benefit_des_04', $( "#benefit_des_04" ).val());
      $.ajax({
          url: '<?php echo site_url('admin/agate_class?act=upd_benefit&token='.$infoLog->token)?>', // gửi đến file upload.php 
          dataType: 'text',
          cache: false,
          contentType: false,
          processData: false,
          data: form_data,
          type: 'post',
          success: function (res) {
            alert(res);
          }
      });
  });

  $("#comment_submit").click(function(){

    var _comment_list = {"comment_title":$( "#comment_title" ).val(),"comment_description":$( "#comment_description" ).val(),"comment_list":$( "#comment_list" ).val()}
    $.ajax({
        url: '<?php echo site_url('admin/agate_class?act=upd_comment&token='.$infoLog->token)?>', // gửi đến file upload.php 
        dataType: 'text',
        data: _comment_list,
        type: 'post',
        success: function (res) {
          alert(res);
        }
    });
  });

  $("#basic_submit").click(function(){
    var form_data = new FormData();
    //thêm files vào trong form data
    form_data.append('basic_name_01', $( "#basic_name_01" ).val());
    form_data.append('basic_name_02', $( "#basic_name_02" ).val());
    form_data.append('basic_name_03', $( "#basic_name_03" ).val());
    form_data.append('basic_name_04', $( "#basic_name_04" ).val());
    form_data.append('basic_description_01', $( "#basic_description_01" ).val());
    form_data.append('basic_description_02', $( "#basic_description_02" ).val());
    form_data.append('basic_description_03', $( "#basic_description_03" ).val());
    form_data.append('basic_description_04', $( "#basic_description_04" ).val());
    form_data.append('basic_title', $( "#basic_title" ).val());
    form_data.append('basic_des', $( "#basic_des" ).val());
    //sử dụng ajax post
    $.ajax({
        url: '<?php echo site_url('admin/agate_class?act=upd_basic&token='.$infoLog->token)?>', // gửi đến file upload.php 
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function (res) {
          alert(res);
        }
    });
  });


  $("#baseline_submit").click(function(){
    var file_data_01 = $('#baseline_img_pc').prop('files')[0];
    var file_data_02 = $('#baseline_img_mb').prop('files')[0];
    var match = ["image/gif", "image/png", "image/jpg",];
    var form_data = new FormData();
    form_data.append('baseline_img_pc', file_data_01);
    form_data.append('baseline_img_mb', file_data_02);
    form_data.append('baseline_item_01', $( "#baseline_item_01" ).val());
    form_data.append('baseline_item_02', $( "#baseline_item_02" ).val());
    form_data.append('baseline_item_03', $( "#baseline_item_03" ).val());
    $.ajax({
        url: '<?php echo site_url('admin/agate_class?act=upd_baseline&token='.$infoLog->token)?>', // gửi đến file upload.php 
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function (res) {
          alert(res);
        }
    });
 
  });


});

</script>
<?php include "inc_banner.php"?>
<?php include "inc_benefit.php"?>
<?php include "inc_comment.php"?>
<?php include "inc_basic.php"?>
<?php include "inc_baseline.php"?>
<?php include "inc_classinfo.php"?>
<?php include "inc_classimglist.php"?>