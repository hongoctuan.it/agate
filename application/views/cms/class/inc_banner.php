
<div class="content">
  <div class="row">
    <div class="col-md-8">
      <div class="card card-user">
        <div class="card-header">
          <h5 class="card-title" id="banner_message">Banner</h5>
        </div>
        <div class="card-body">
        <?php echo form_open_multipart(site_url('admin/agate_class?act=upd_banner&token='.$infoLog->token),array('autocomplete'=>"off",'id'=>"fileUploadForm","method"=>'post'));?>
            <div class="row">
              
              <div class="col-md-5 pr-1">
                <div class="form-group">
                  <label>Tiêu đề</label>
                  <input type="text" class="form-control" name="banner_title" id="banner_title" value="<?php echo $class[0]->value ?>">
                </div>
              </div>
              <div class="col-md-7">
                <div class="form-group">
                  <label>Hình Banner (Hình ảnh hiển thị đẹp nhất với kích thước ngang  2900 × cao 3124)</label>
                  <input type="file" class="form-control" name="banner_img" accept="image/*" onchange="preview_image(event,'output_banner_image')"/>
                  <img id="output_banner_image" height="150px" src="<?php echo site_url('assets/public/avatar/'.$class[32]->value)?>"/>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label>Mô tả</label>
                  <textarea id="banner_description" name="banner_description" class="form-control textarea"><?php echo $class[1]->value ?></textarea>
                </div>
                <script>
									var editor = CKEDITOR.replace('banner_description',{
										language:'vi',
										filebrowserBrowseUrl :'<?php echo base_url()."filemanager/ckfinder/ckfinder.html"?>',

										filebrowserImageBrowseUrl : '<?php echo base_url()."filemanager/ckfinder/ckfinder.html?type=Images"?>',
										
										filebrowserFlashBrowseUrl : '<?php echo base_url()."filemanager/ckfinder/ckfinder.html?type=Flash"?>',
										
										filebrowserUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files"?>',
										
										filebrowserImageUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images"?>',
										
										filebrowserFlashUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash"?>',

									});
              </script>
              </div>
            </div>
            <div class="row">
              <div class="update ml-auto mr-auto">
                <button type="submit" id="banner_submit" class="btn btn-primary btn-round">Lưu</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card card-user">
          <img src="<?php echo site_url('assets/public/avatar/class_banner.png')?>" />
      </div>
    </div>
  </div>
</div>