
<div class="content">
  <div class="row">
    <div class="col-md-8">
      <div class="card card-user">
        <div class="card-header">
          <h5 class="card-title" id="baseline_message">Base line</h5>
        </div>
        <div class="card-body">
        <?php echo form_open_multipart("",array('autocomplete'=>"off",'id'=>"fileUploadForm"));?>
            <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Nội dung block 1</label>
                        <textarea id="baseline_item_01" class="form-control" ><?php echo $class[24]->value?></textarea>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Nội dung block 2</label>
                        <textarea id="baseline_item_02" class="form-control" ><?php echo $class[25]->value?></textarea>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Nội dung block 3</label>
                        <textarea id="baseline_item_03" class="form-control" ><?php echo $class[26]->value?></textarea>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Hình nền cho PC (chiều rộng 1141 x chiều cao 715)</label>
                        <input type="file" class="form-control" id="baseline_img_pc" accept="image/*" onchange="preview_image(event,'output_baseline_image_01_pc')"/>
                        <img id="output_baseline_image_01_pc" width="250px" src="<?php echo site_url('assets/public/avatar/'.$class[27]->value)?>"/>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Hình nền cho Mobile (chiều rộng 1404 x chiều cao 6057)</label>
                        <input type="file" class="form-control" id="baseline_img_mb" accept="image/*" onchange="preview_image(event,'output_baseline_image_02_mb')"/>
                        <img id="output_baseline_image_02_mb" height="250px" src="<?php echo site_url('assets/public/avatar/'.$class[42]->value)?>"/>
                      </div>
                    </div>
              
             
            </div>
            <div class="row">
              <div class="update ml-auto mr-auto">
                <button type="button" id="baseline_submit" class="btn btn-primary btn-round">Lưu</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card card-user">
          <img src="<?php echo site_url('assets/public/avatar/class_noibat.png')?>" />
      </div>
    </div>
  </div>
</div>