
<div class="content">
  <div class="row">
    <div class="col-md-8">
      <div class="card card-user">
        <div class="card-header">
          <h5 class="card-title" id="basic_message">Nền tảng chương trình</h5>
        </div>
        <div class="card-body">
        <?php echo form_open_multipart("",array('autocomplete'=>"off",'id'=>"fileUploadForm"));?>
            <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Tiêu đề</label>
                <input type="text" id="basic_title" class="form-control" value="<?php echo $class[40]->value?>">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Nội dung</label>
                <input type="text" id="basic_des" class="form-control" value="<?php echo $class[41]->value?>">
              </div>
            </div>
              <div class="col-md-6">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Tiêu đề block 01</label>
                    <input type="text" id="basic_name_01" class="form-control" value="<?php echo $class[12]->value?>">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Nội dung block 01</label>
                    <textarea id="basic_description_01" class="form-control"><?php echo $class[15]->value?></textarea>
                  </div>
                </div>
                </div>
              <div class="col-md-6">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Tiêu đề block 02</label>
                    <input type="text" id="basic_name_02" class="form-control" value="<?php echo $class[13]->value?>">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Nội dung block 02</label>
                    <textarea id="basic_description_02" class="form-control"><?php echo $class[16]->value?></textarea>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Tiêu đề block 03</label>
                    <input type="text" id="basic_name_03" class="form-control"value="<?php echo $class[14]->value?>">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Nội dung block 03</label>
                    <textarea id="basic_description_03" class="form-control"><?php echo $class[17]->value?></textarea>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Tiêu đề block 04</label>
                    <input type="text" id="basic_name_04" class="form-control" value="<?php echo $class[29]->value?>">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Nội dung block 04</label>
                    <textarea type="text" id="basic_description_04" class="form-control"><?php echo $class[30]->value?></textarea>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="update ml-auto mr-auto">
                <button type="button" id="basic_submit" class="btn btn-primary btn-round">Lưu</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card card-user">
          <img src="<?php echo site_url('assets/public/avatar/class_basic.png')?>" />
      </div>
    </div>
  </div>
</div>