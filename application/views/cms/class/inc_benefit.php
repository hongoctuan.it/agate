
<div class="content">
  <div class="row">
    <div class="col-md-8">
      <div class="card card-user">
        <div class="card-header">
          <h5 class="card-title" id="benefit_message">YOUniverse</h5>
        </div>
        <div class="card-body">
        <?php echo form_open_multipart("",array('autocomplete'=>"off",'id'=>"fileUploadForm"));?>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Tiêu đề YOUniverse</label>
                  <input type="text" id="benefit_title" class="form-control" value="<?php echo $class[33]->value?>">
                </div>
              </div>
              <div class="col-md-6">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Tiêu đề block 01</label>
                    <input type="text" id="benefit_item_name_01" class="form-control" value="<?php echo $class[2]->value?>">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Nội dung block 01</label>
                    <textarea id="benefit_des_01" class="form-control"><?php echo $class[34]->value?></textarea>
                  </div>
                </div>
                <!-- <div class="col-md-12">
                  <div class="form-group">
                    <label>Image</label>
                    <input type="file" class="form-control" id="benefit_item_img_01" accept="image/*" onchange="preview_image(event,'output_image_01')"/>
                    <img id="output_image_01" height="50px" width="50px" src="<?php echo site_url('assets/public/avatar/'.$class[6]->value)?>"/>
                  </div>
                </div> -->
              </div>
              <div class="col-md-6">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Tiêu đề block 02</label>
                    <input type="text" id="benefit_item_name_02" class="form-control" value="<?php echo $class[3]->value?>">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Nội dung block 02</label>
                    <textarea type="text" id="benefit_des_02" class="form-control"><?php echo $class[35]->value?></textarea>
                  </div>
                </div>
                <!-- <div class="col-md-12">
                  <div class="form-group">
                    <label>Image</label>
                    <input type="file" class="form-control" id="benefit_item_img_02" accept="image/*" onchange="preview_image(event,'output_image_02')"/>
                    <img id="output_image_02" height="50px" width="50px" src="<?php echo site_url('assets/public/avatar/'.$class[7]->value)?>"/>
                  </div>
                </div> -->
              </div>
              <div class="col-md-6">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Tiêu đề block 03</label>
                    <input type="text" id="benefit_item_name_03" class="form-control" value="<?php echo $class[4]->value?>">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                  <label>Nội dung block 03</label>
                  <textarea type="text" id="benefit_des_03" class="form-control"><?php echo $class[36]->value?></textarea>
                  </div>
                </div>
                <!-- <div class="col-md-12">
                  <div class="form-group">
                    <label>Image</label>
                    <input type="file" class="form-control" id="benefit_item_img_03" accept="image/*" onchange="preview_image(event,'output_image_03')"/>
                    <img id="output_image_03" height="50px" width="50px" src="<?php echo site_url('assets/public/avatar/'.$class[8]->value)?>"/>
                  </div>
                </div> -->
              </div>
              <div class="col-md-6">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Tiêu đề block 04</label>
                    <input type="text" id="benefit_item_name_04" class="form-control" placeholder="Company" value="<?php echo $class[5]->value?>">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Nội dung block 04</label>
                    <textarea type="text" id="benefit_des_04" class="form-control"><?php echo $class[37]->value?></textarea>
                  </div>
                </div>
                <!-- <div class="col-md-12">
                  <div class="form-group">
                    <label>Image</label>
                    <input type="file" class="form-control" id="benefit_item_img_04" accept="image/*" onchange="preview_image(event,'output_image_04')"/>
                    <img id="output_image_04" height="50px" width="50px" src="<?php echo site_url('assets/public/avatar/'.$class[9]->value)?>"/>
                  </div>
                </div> -->
              </div>
            </div>
            <div class="row">
              <div class="update ml-auto mr-auto">
                <button type="button" id="benefit_submit" class="btn btn-primary btn-round">Lưu</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card card-user">
          <img src="<?php echo site_url('assets/public/avatar/class_tieubieu.png')?>" />
      </div>
    </div>
  </div>
</div>