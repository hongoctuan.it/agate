<script>
    function deleteRow(rowId){
        $("table tbody").find('#'+rowId).each(function(){
            _data = {'id':rowId}
            $(this).parents("tr").remove();
            $.ajax({
                url: '<?php echo site_url('admin/agate_class?act=upd_removeimg&token='.$infoLog->token)?>', // gửi đến file upload.php 
                dataType: 'text',
                data: _data,
                type: 'post',
                success: function (res) {
                    alert(res);
                }
            });

        });
    }
    $(document).ready(function(){
        $("#classimglist_add_img").click(function(){
            //Lấy ra files
            var file_data = $('#classimglist_img').prop('files')[0];
                var match = ["image/gif", "image/png", "image/jpg",];

                    var form_data = new FormData();
                    form_data.append('classimglist_img', file_data);
                    form_data.append('description', $('#classimglist_url').val());
                    $.ajax({
                        url: '<?php echo site_url('admin/agate_class?act=upd_addimg&token='.$infoLog->token)?>', // gửi đến file upload.php 
                        dataType: 'text',
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (res) {
                            alert(res);
                            location.reload();
                        }
                    });
               

                return false;
        });
        
        // Find and remove selected table rows
        $(".delete-row").click(function(){
            $("table tbody").find('input[name="record"]').each(function(){
                if($(this).is(":checked")){
                    var temp = $(this).find(".customerIDCell").html();
                    alert(temp);
                    $(this).parents("tr").remove();
                }
            });
        });
    });    
</script>

<div class="content">
    <div class="row">
        <div class="col-md-8">
        <div class="card">
            <div class="card-header">
            <h4 class="card-title" id="classimglist_message">Hình ảnh của lớp học</h4>
            </div>
            <div class="card-body">
            <div class="table-responsive">
            <div class="col-md-12 form-group row">
                <div class="col-md-4">
                <label>Hình ảnh</label>
                <input type="file" class="form-control" id="classimglist_img" accept="image/*" onchange="preview_image(event,'output_classimglist_img')"/>
                <img id="output_classimglist_img" height="50px" width="50px" src="<?php echo site_url('assets/public/avatar/default.png')?>"/>
                </div>
                <div class="col-md-8">
                <label>Đường link</label>
                <input type="text" class="form-control" id="classimglist_url"/>
                </div>
            </div>     
            <button type="button" id="classimglist_add_img" class="btn btn-primary btn-round" >Thêm hình ảnh</button>           
            <table class="table">
                <thead class=" text-primary">
                    <th>
                        #
                    </th>
                    <th>
                        Hình Ảnh
                    </th>
                    <th>
                        Đường link
                    </th>
                    <th>
                        Thao tác
                    </th>
                </thead>
                <tbody>
                    <?php if(!empty($classimg)):?>
                        <?php foreach($classimg as $key=>$item):?>
                            <tr>
                                <td>
                                   <?php echo $key+1?>
                                </td>
                                <td>
                                    <img src="<?php echo site_url('assets/public/avatar/'.$item->img)?>" width="200px"/>
                                </td>
                                <td>
                                    <?php echo $item->description ?>
                                </td>
                                <td>
                                    <button type="button" id="<?php echo $item->id ?>" class="btn btn-primary btn-round" onclick="deleteRow(<?php echo $item->id ?>)">Xoá Hình</button>
                                </td>
                            </tr>
                        <?php endforeach;?>
                    <?php endif;?>
                </tbody>
                </table>
            </div>
            </div>
        </div>
        </div>
        <div class="col-md-4">
      <div class="card card-user">
          <img src="<?php echo site_url('assets/public/avatar/class_img.png')?>" />
      </div>
    </div>
    </div>
</div>