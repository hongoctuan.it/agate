<div class="content" id="classinfo">
  <div class="row">
    <div class="col-md-8">
      <div class="card card-user">
        <div class="card-header">
          <h5 class="card-title" id="classinfo_message">Nhận tư vấn về khoá học</h5>
        </div>
        <div class="card-body">
        <form action="<?php echo site_url('admin/agate_class?act=upd_classinfo&token='.$infoLog->token)?>" method='post'>

            <div class="row">
              <div class="col-md-12">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Tiêu đề</label>
                        <input type="text" id="classinfo_title" name="classinfo_title" class="form-control" value="<?php echo $class[21]->value?>">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Nội dung chi tiết (Cho PC)</label>
                        <textarea id="classinfo_description" name="classinfo_description" class="form-control textarea"><?php echo $class[23]->value?></textarea>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Nội dung chi tiết (Cho Mobile)</label>
                        <textarea id="classinfo_description_mb" name="classinfo_description_mb" class="form-control textarea"><?php echo $class[22]->value?></textarea>
                      </div>
                    </div>
              </div>
              <script>
									var editor = CKEDITOR.replace('classinfo_description',{
										language:'en',
										filebrowserBrowseUrl :'<?php echo base_url()."filemanager/ckfinder/ckfinder.html"?>',

										filebrowserImageBrowseUrl : '<?php echo base_url()."filemanager/ckfinder/ckfinder.html?type=Images"?>',
										
										filebrowserFlashBrowseUrl : '<?php echo base_url()."filemanager/ckfinder/ckfinder.html?type=Flash"?>',
										
										filebrowserUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files"?>',
										
										filebrowserImageUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images"?>',
										
										filebrowserFlashUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash"?>',

									});
              </script>
              <script>
									var editor = CKEDITOR.replace('classinfo_description_mb',{
										language:'en',
										filebrowserBrowseUrl :'<?php echo base_url()."filemanager/ckfinder/ckfinder.html"?>',

										filebrowserImageBrowseUrl : '<?php echo base_url()."filemanager/ckfinder/ckfinder.html?type=Images"?>',
										
										filebrowserFlashBrowseUrl : '<?php echo base_url()."filemanager/ckfinder/ckfinder.html?type=Flash"?>',
										
										filebrowserUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files"?>',
										
										filebrowserImageUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images"?>',
										
										filebrowserFlashUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash"?>',

									});
              </script>

             
            </div>
            <div class="row">
              <div class="update ml-auto mr-auto">
                <button type="submit" id="classinfo_submit" class="btn btn-primary btn-round">Lưu</button>
              </div>
            </div>
            </form>

        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card card-user">
          <img src="<?php echo site_url('assets/public/avatar/class_regis.png')?>" />
      </div>
    </div>
  </div>
</div>