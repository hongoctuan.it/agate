<div class="content">
  <div class="row">
    <div class="col-md-8">
      <div class="card card-user">
        <div class="card-header">
          <h5 class="card-title" id="comment_message">Bình luận</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-5 pr-1">
                  <div class="form-group">
                    <label>Tiêu đề</label>
                    <input type="text" class="form-control" id="comment_title" placeholder="Company" value="<?php echo $class[38]->value?>">
                  </div>
                </div>
                <div class="col-md-7">
                  <div class="form-group">
                    <label>Nội dung</label>
                    <textarea id="comment_description" class="form-control textarea"><?php echo $class[39]->value?></textarea>
                  </div>
                </div>
              <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">
              <script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>
              <script>
              $(document).ready(function(){

              var multipleCancelButton = new Choices('#comment_list', {
              removeItemButton: true,
              maxItemCount:3,
              searchResultLimit:500,
              renderChoiceLimit:500
              });


              });
              </script>
              <style>
                .mt-100 {
                  margin-top: 100px
              }

              body {
                  background: #00B4DB;
                  background: -webkit-linear-gradient(to right, #0083B0, #00B4DB);
                  background: linear-gradient(to right, #0083B0, #00B4DB);
                  color: #514B64;
                  min-height: 100vh
              }
              </style>
                  <div class="col-md-12"> 
                    <select id="comment_list" placeholder="Chọn tối đa 3 bình luận" multiple>
                      <?php foreach($comments as $item):?>
                        <?php if($item->id == $comment_01->id || $item->id == $comment_02->id || $item->id == $comment_03->id):?>
                          <option value="<?php echo $item->id ?>" selected><?php echo $item->description?></option>
                        <?php else:?>
                          <option value="<?php echo $item->id ?>"><?php echo $item->description?></option>
                        <?php endif;?>                   
                      <?php endforeach; ?>
                    </select> 
                  </div>
            </div>
            <div class="row">
              <div class="update ml-auto mr-auto">
                <button type="button" id="comment_submit" class="btn btn-primary btn-round">Lưu</button>
              </div>
            </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card card-user">
          <img src="<?php echo site_url('assets/public/avatar/class_comment.png')?>" />
      </div>
    </div>
  </div>
</div>