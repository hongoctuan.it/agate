<script src="<?php echo site_url('statics/cms/js/common.js')?>"></script>

<div class="content">
    <div class="row">
        <div class="col-md-12">
        <div class="card">
            <div class="card-header">
            <h4 class="card-title" id="classimglist_message">Danh sách bình luận</h4>
            </div>
            <div class="card-body">
            <div class="table-responsive">   
              <div class="col-md-12">
                <div class="form-group">
                  <a href="<?php echo site_url('admin/agate_comment?act=add&token='.$infoLog->token)?>"><button id="gift_add" class="btn btn-primary btn-round" data-toggle="modal" data-target="#exampleModal" >Thêm bình luận</button> </a>         
                </div>
              </div>   
            <table class="table">
                <thead class=" text-primary">
                    <th>
                        #
                    </th>
                    <th>
                        Nội dung
                    </th>
                    <th>
                        Thời gian
                    </th>
                    <th>
                        Người BL
                    </th>
                </thead>
                <tbody>
                    <?php if(!empty($comments)):?>
                      <?php foreach($comments as $key=>$item):?>
                          <tr>
                              <td>
                                <?php echo $key+1 ?>
                              </td>
                              <td width="50%">
                                <?php echo $item->description ?>
                              </td>
                              <td>
                                <?php echo date("d-m-Y",$item->time) ?>
                              </td>
                              <td>
                                <?php echo $item->user_name ?>
                              </td>
                              <td>
                                  <?php if($item->active == 1):?>
                                    <a href="<?php echo site_url('admin/agate_comment?act=lock&id='.$item->id.'&token='.$infoLog->token)?>"><i class="fa fa-unlock" style="font-size:24px"></i></a>
                                  <?php else:?>
                                    <a href="<?php echo site_url('admin/agate_comment?act=unlock&id='.$item->id.'&token='.$infoLog->token)?>"><i class="fa fa-unlock-alt" style="font-size:24px"></i></a>
                                  <?php endif; ?>
                                    <a href="<?php echo site_url('admin/agate_comment?act=del&id='.$item->id.'&token='.$infoLog->token)?>"><i class="fa fa-trash-o" style="font-size:24px"></i></a>
                                  <a href="<?php echo site_url('admin/agate_comment?act=edit&id='.$item->id.'&token='.$infoLog->token)?>"><i class="nc-icon nc-settings" style="font-size:24px"></i></a>
                              </td>
                          </tr>
                      <?php endforeach;?>
                    <?php endif;?>
                </tbody>
                </table>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>
