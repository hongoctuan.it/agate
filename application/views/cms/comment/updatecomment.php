
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="<?php echo site_url('statics/cms/js/common.js')?>"></script>
<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-user">
        <div class="card-header">
          <h5 class="card-title" id="banner_message">Cập nhật bình luận</h5>
        </div>
        <div class="card-body">
        <?php echo form_open_multipart(site_url('admin/agate_comment?act=upd&token='.$infoLog->token),array('autocomplete'=>"off",'id'=>"fileUploadForm"));?>
            <input type="hidden" name="id" name="id"  class="form-control" value="<?php echo isset($comment)?$comment->id:""?>">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Nội dung bình luận </label>
                  <textarea  name="description" class="form-control"><?php echo !empty($comment->description)?$comment->description:"" ?></textarea>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Tên người bình luận</label>
                  <textarea  name="user_name" class="form-control"><?php echo !empty($comment->user_name)?$comment->user_name:"" ?></textarea>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Hình đại diện (Hình có tỷ lệ kích thước 321px x 321px)</label>
                  <input type="file" class="form-control" id="comment_img_01" name="comment_img_01" accept="image/*" onchange="preview_image(event,'output_comment_img_01')"/>
                  <img id="output_comment_img_01" height="100px" width="100px" src="<?php echo isset($comment)?site_url('assets/public/avatar/'.$comment->img):site_url('assets/public/avatar/default.png')?>"/>
                </div>
              </div>
            </div>
            <!-- <div class="row">
            <div class="col-md-12"> 
              <div class="form-group">
              <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">
              <script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>
              <script>
              $(document).ready(function(){

              var multipleCancelButton = new Choices('#user_comment', {
              removeItemButton: true,
              maxItemCount:1,
              searchResultLimit:500,
              renderChoiceLimit:500
              });


              });
              </script>
              <style>
                .mt-100 {
                  margin-top: 100px
              }

              body {
                  background: #00B4DB;
                  background: -webkit-linear-gradient(to right, #0083B0, #00B4DB);
                  background: linear-gradient(to right, #0083B0, #00B4DB);
                  color: #514B64;
                  min-height: 100vh
              }
              </style>
                    <select id="user_comment" name="user_id" placeholder="Chọn người bình luận" multiple >
                      <?php foreach($users as $item):?>
                        <?php if($item->id == $comment->user_id):?>
                          <option value="<?php echo $item->id ?>" selected><?php echo $item->full_name?></option>
                        <?php else:?>
                          <option value="<?php echo $item->id ?>"><?php echo $item->full_name?></option>
                        <?php endif;?>                   
                      <?php endforeach; ?>
                    </select> 
                  </div>
                  </div>
            </div>
            </div> -->
            <div class="row">
              <div class="update ml-auto mr-auto">
                <a href="<?php echo site_url('admin/agate_comment')?>"><button type="button" class="btn-primary btn-round">Đóng</button></a>
                <button type="submit" id="gift_submit"  data-id="0" class="btn-primary btn-round">Lưu</button>
              </div>
            </div>
        </form>
        </div>
      </div>
    </div>
   
  </div>
</div>


            
