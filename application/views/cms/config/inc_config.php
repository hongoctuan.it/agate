
<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-user">
        <div class="card-header">
          <h5 class="card-title" id="baseline_message">Cấu hình chung</h5>
        </div>
        <div class="card-body">
        <?php echo form_open_multipart(site_url('admin/agate_config?act=upd&token='.$infoLog->token),array('autocomplete'=>"off",'id'=>"fileUploadForm"));?>
            <div class="row">
              <div class="col-md-12">
                <fieldset class="scheduler-border">                    
                    <!--<div class="col-md-12">-->
                    <!--  <div class="form-group">-->
                    <!--    <label>Email gửi thông báo <a href="https://docs.google.com/document/d/16yUI-1mlQFnUVDaqOimWGkMpht4c_UTL_t19e0XhkPk/edit?usp=sharing" target="_blank">Hướng dẫn cấu hình Gmail gửi thông báo</a></label>-->
                    <!--    <input type="email" id="email" name="email" class="form-control" placeholder="Email gửi thông báo" value="<?php echo $config[1]->value?>">-->
                    <!--  </div>-->
                    <!--</div>-->
                    <!--<div class="col-md-12">-->
                    <!--  <div class="form-group">-->
                    <!--    <label>Password Email gửi thông báo</label>-->
                    <!--    <input type="password" id="password" name="password"  class="form-control" placeholder="Password email gửi thông báo" value="<?php echo $config[2]->value?>">-->
                    <!--  </div>-->
                    <!--</div>-->
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Email nhận thông báo khi có user đăng ký</label>
                        <input type="email" id="email_recive" name="email_recive"  class="form-control" placeholder="Email nhận thông báo" value="<?php echo $config[4]->value?>">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Tiêu đề mail thông báo cho user</label>
                        <input type="text" id="title_email_send" name="title_email_send" class="form-control" value="<?php echo $config[6]->value?>"/>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Nội dung mail thông báo cho user khác</label>
                        <textarea id="description_email_send" name="description_email_send" class="form-control"><?php echo $config[5]->value?></textarea>
                        <script>
                          var editor = CKEDITOR.replace('description_email_send',{
                            language:'vi',
                            filebrowserBrowseUrl :'<?php echo base_url()."filemanager/ckfinder/ckfinder.html"?>',

                            filebrowserImageBrowseUrl : '<?php echo base_url()."filemanager/ckfinder/ckfinder.html?type=Images"?>',
                            
                            filebrowserFlashBrowseUrl : '<?php echo base_url()."filemanager/ckfinder/ckfinder.html?type=Flash"?>',
                            
                            filebrowserUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files"?>',
                            
                            filebrowserImageUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images"?>',
                            
                            filebrowserFlashUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash"?>',

                          });
                        </script>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Nội dung mail thông báo cho user phụ huynh</label>
                        <textarea id="description_email_send" name="description_email_send_ph" class="form-control"><?php echo $config[7]->value?></textarea>
                        <script>
                          var editor = CKEDITOR.replace('description_email_send_ph',{
                            language:'vi',
                            filebrowserBrowseUrl :'<?php echo base_url()."filemanager/ckfinder/ckfinder.html"?>',

                            filebrowserImageBrowseUrl : '<?php echo base_url()."filemanager/ckfinder/ckfinder.html?type=Images"?>',
                            
                            filebrowserFlashBrowseUrl : '<?php echo base_url()."filemanager/ckfinder/ckfinder.html?type=Flash"?>',
                            
                            filebrowserUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files"?>',
                            
                            filebrowserImageUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images"?>',
                            
                            filebrowserFlashUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash"?>',

                          });
                        </script>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Nội dung mail thông báo cho user học sinh</label>
                        <textarea id="description_email_send" name="description_email_send_hs" class="form-control"><?php echo $config[8]->value?></textarea>
                        <script>
                          var editor = CKEDITOR.replace('description_email_send_hs',{
                            language:'vi',
                            filebrowserBrowseUrl :'<?php echo base_url()."filemanager/ckfinder/ckfinder.html"?>',

                            filebrowserImageBrowseUrl : '<?php echo base_url()."filemanager/ckfinder/ckfinder.html?type=Images"?>',
                            
                            filebrowserFlashBrowseUrl : '<?php echo base_url()."filemanager/ckfinder/ckfinder.html?type=Flash"?>',
                            
                            filebrowserUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files"?>',
                            
                            filebrowserImageUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images"?>',
                            
                            filebrowserFlashUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash"?>',

                          });
                        </script>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Logo site</label>
                        <input type="file" class="form-control" id="config_img" name="config_img" accept="image/*" onchange="preview_image(event,'output_config')"/>
                        <img id="output_config" height="50px" width="50px"src="<?php echo site_url('assets/public/avatar/'.$config[3]->value)?>"/>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Mô tả cho phần footer</label>
                        <textarea id="description" name="description" class="form-control"><?php echo $config[0]->value?></textarea>
                      </div>
                    </div>
                </fieldset>
              </div>
              
             
            </div>
            <div class="row">
              <div class="update ml-auto mr-auto">
                <button type="submit" id="baseline_submit" class="btn btn-primary btn-round">Lưu</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>