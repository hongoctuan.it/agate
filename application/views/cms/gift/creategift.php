
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="<?php echo site_url('statics/cms/js/common.js')?>"></script>
<div class="content">
  <div class="row">
    <div class="col-md-8">
      <div class="card card-user">
        <div class="card-header">
          <h5 class="card-title" id="banner_message">Tạo Gift</h5>
        </div>
        <div class="card-body">
        <?php echo form_open_multipart(site_url('admin/agate_gift?act=upd_addgift&token='.$infoLog->token),array('autocomplete'=>"off",'id'=>"fileUploadForm"));?>
          <input type="hidden" name="id" name="id"  class="form-control" value="<?php echo isset($gift)?$gift->id:""?>">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Tên loại gift </label>
                  <input type="text" name="gift_type" class="form-control" placeholder="Tên loại gift" value="<?php echo isset($gift)?$gift->type:""?>">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Tiêu đề gift</label>
                  <input type="text" name="gift_title" class="form-control" placeholder="Tiêu đề gift" value="<?php echo isset($gift)?$gift->title:""?>">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Nội dung gift</label>
                  <textarea class="form-control textarea" name="gift_description" ><?php echo isset($gift)?$gift->description:""?></textarea>
                  <script>
                    var editor = CKEDITOR.replace('gift_description',{
                      language:'en',
                      filebrowserBrowseUrl :'<?php echo base_url()."filemanager/ckfinder/ckfinder.html"?>',

                      filebrowserImageBrowseUrl : '<?php echo base_url()."filemanager/ckfinder/ckfinder.html?type=Images"?>',
                      
                      filebrowserFlashBrowseUrl : '<?php echo base_url()."filemanager/ckfinder/ckfinder.html?type=Flash"?>',
                      
                      filebrowserUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files"?>',
                      
                      filebrowserImageUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images"?>',
                      
                      filebrowserFlashUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash"?>',
									  });
                  </script>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3 form-group">
                <label>Hình ảnh 01 (Hình ảnh hiển thị đẹp nhất với kích thước ngang 666px × cao 638px</label>
                <input type="file" class="form-control" name="gift_img_01" accept="image/*" onchange="preview_image(event,'output_gift_image_01')"/>
                <img id="output_gift_image_01" height="50px" width="50px" src="<?php echo isset($gift->img_01)?base_url('assets/public/avatar/'.$gift->img_01):base_url('assets/public/avatar/default.png')?>"/>
              </div>
              <div class="col-md-3 form-group">
              <label>Hình ảnh 02 (Hình ảnh hiển thị đẹp nhất với kích thước ngang 666px × cao 638px</label>
                <input type="file" class="form-control" name="gift_img_02" accept="image/*" onchange="preview_image(event,'output_gift_image_02')"/>
                <img id="output_gift_image_02" height="50px" width="50px" src="<?php echo isset($gift->img_02)?base_url('assets/public/avatar/'.$gift->img_02):base_url('assets/public/avatar/default.png')?>"/>
              </div>
              <div class="col-md-3 form-group">
              <label>Hình ảnh 03 (Hình ảnh hiển thị đẹp nhất với kích thước ngang 666px × cao 638px</label>
                <input type="file" class="form-control" name="gift_img_03" accept="image/*" onchange="preview_image(event,'output_gift_image_03')"/>
                <img id="output_gift_image_03" height="50px" width="50px" src="<?php echo isset($gift->img_03)?base_url('assets/public/avatar/'.$gift->img_03):base_url('assets/public/avatar/default.png')?>"/>
              </div>
              <div class="col-md-3 form-group">
              <label>Hình ảnh 04 (Hình ảnh hiển thị đẹp nhất với kích thước ngang 666px × cao 638px</label>
                <input type="file" class="form-control" name="gift_img_04" accept="image/*" onchange="preview_image(event,'output_gift_image_04')"/>
                <img id="output_gift_image_04" height="50px" width="50px" src="<?php echo isset($gift->img_04)?base_url('assets/public/avatar/'.$gift->img_04):base_url('assets/public/avatar/default.png')?>"/>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Shopee Link</label>
                  <input type="text" name="shopee_link" class="form-control" placeholder="Shopee link" value="<?php echo isset($gift)?$gift->shopee_link:""?>">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="update ml-auto mr-auto">
                <a href="<?php echo site_url('admin/agate_gift')?>"><button type="button" class="btn-primary btn-round">Đóng</button></a>
                <button type="submit" id="gift_submit"  data-id="0" class="btn-primary btn-round">Lưu</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card card-user">
          <img src="<?php echo site_url('assets/public/avatar/create_gift.png')?>" />
      </div>
    </div>
  </div>
</div>