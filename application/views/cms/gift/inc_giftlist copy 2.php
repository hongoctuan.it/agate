<div class="content">
    <div class="row">
        <div class="col-md-12">
          <div class="card">
              <div class="card-header">
              <h4 class="card-title" id="gift_message"> Danh sách gift</h4>
              </div>
              <div class="card-body">
              <div class="table-responsive">
              <div class="col-md-12">
                <div class="form-group">
                  <a href="<?php echo site_url('admin/agate_gift?act=upd_getgift&token='.$infoLog->token)?>"><button id="gift_add" class="btn btn-primary btn-round" data-toggle="modal" data-target="#exampleModal" >Thêm gift</button> </a>         
                </div>
              </div>     
                <table class="table">
                  <thead class=" text-primary">
                      <th>
                          STT
                      </th>
                      <th>
                          Hình Ảnh
                      </th>
                      <th>
                          Thao tác
                      </th>
                  </thead>
                  <tbody>
                      <?php foreach($gitflist as $item):?>
                        <tr>
                            <td id="title<?php echo $item->id?>">
                              <?php echo $item->title ?>
                            </td>
                            <td>
                              <img id="img<?php echo $item->id?>" src="<?php echo base_url('assets/public/avatar/'.$item->img_01);?>" width="100px"/>
                            </td>
                            <td>
                              <a href="#" id="<?php echo $item->id?>" onclick="deleteRow(<?php echo $item->id?>)"><i class="fa fa-trash-o" style="font-size:24px"></i></a>
                              <a href="<?php echo site_url('admin/agate_gift?act=upd_getgift&id='.$item->id.'&token='.$infoLog->token)?>" id="updateRow" data-id="<?php echo $item->id?>" ><i class="nc-icon nc-settings" style="font-size:24px"></i></a> 
                            </td>
                        </tr>
                      <?php endforeach?>
                  </tbody>
                </table>
                <div class="paging">
                  <label>Tổng số trang: </label>
                </div>
              </div>
              </div>
          </div>
        </div>
    </div>
</div>
