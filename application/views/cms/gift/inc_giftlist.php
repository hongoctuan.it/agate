
    
    <link rel="stylesheet" href="<?php echo site_url('statics/default/assets/css/bootstrap.css')?>">
    <link rel="stylesheet" href="<?php echo site_url('statics/default/assets/css/style.css')?>">


    <section class="section">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title" id="gift_message"> Danh sách gift</h4>
            </div>
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <thead>
                        <tr>
                            <th>
                            STT
                        </th>
                        <th>
                            Hình Ảnh
                        </th>
                        <th>
                            Thao tác
                        </th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($gitflist as $item):?>
                        <tr>
                            <td id="title<?php echo $item->id?>">
                              <?php echo $item->title ?>
                            </td>
                            <td>
                              <img id="img<?php echo $item->id?>" src="<?php echo base_url('assets/public/avatar/'.$item->img_01);?>" width="100px"/>
                            </td>
                            <td>
                              <a href="#" id="<?php echo $item->id?>" onclick="deleteRow(<?php echo $item->id?>)"><i class="fa fa-trash-o" style="font-size:24px"></i></a>
                              <a href="<?php echo site_url('admin/agate_gift?act=upd_getgift&id='.$item->id.'&token='.$infoLog->token)?>" id="updateRow" data-id="<?php echo $item->id?>" ><i class="nc-icon nc-settings" style="font-size:24px"></i></a> 
                            </td>
                        </tr>
                      <?php endforeach?>
                    </tbody>
                </table>
            </div>
        </div>

    </section>  
<script src="<?php echo site_url('statics/default/assets/js/simple-datatables.js')?>"></script>
<script>
    let table1 = document.querySelector('#table1');
    let dataTable = new simpleDatatables.DataTable(table1);
</script>


