
<link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="<?php echo site_url('statics/cms/js/common.js')?>"></script>

<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<style>.note-image-input{
  opacity: 1 !important;
  position: unset !important;
}
.form-group .form-control, .input-group .form-control{
  padding-top: 0px !important;
  padding-bottom: 0px !important;

}</style>
<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-user">
        <div class="card-header">
          <h5 class="card-title" id="banner_message">Tạo Hashtag</h5>
        </div>
        <div class="card-body">
        <?php echo form_open_multipart(site_url('admin/hashtag?act=add&token='.$infoLog->token),array('autocomplete'=>"off",'id'=>"fileUploadForm"));?>
          <input type="hidden" name="id" id="id"  class="form-control" value="<?php echo isset($hashtag)?$hashtag->id:""?>">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Tiêu đề bài Hashtag</label>
                  <input type="text" name="title" id="title"  class="form-control" placeholder="Tiêu đề Hashtag" value="<?php echo isset($hashtag)? $hashtag->title : ""  ?>">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="update ml-auto mr-auto">
              <a href="<?php echo site_url('admin/hashtag')?>"><button type="button" class="btn-primary btn-round">Đóng</button></a>
                <button type="submit" id="save_mag" data-id="0" class="btn-primary btn-round">Lưu</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

