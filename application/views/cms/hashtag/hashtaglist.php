<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link href="https://getbootstrap.com/docs/5.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">
<script src="http://code.jquery.com/jquery-1.7.2.js"></script>
<script src="https://code.jquery.com/jquery-1.4.3.min.js" integrity="sha256-+ACzmeXHpSVPxmu0BxF/44294FKHgOaMn3yH0pn4SGo=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>
<script src="<?php echo site_url('statics/cms/js/common.js')?>"></script>
<script>
    function deleteRow(rowId){
      _data = {'id':rowId}
      $.ajax({
          url: '<?php echo site_url('admin/hashtag?act=del&token='.$infoLog->token)?>',
          dataType: 'text',
          data: _data,
          type: 'post',
          success: function (res) {
            location.reload();
          }
      });
    }
    function updateTrend(rowId,trend){
        if(trend == 1)
            trend = 0
        else
            trend = 1
      _data = {'id':rowId, 'trend':trend}
      $.ajax({
          url: '<?php echo site_url('admin/hashtag?act=trend&token='.$infoLog->token)?>',
          dataType: 'text',
          data: _data,
          type: 'post',
          success: function (res) {
            location.reload();
          }
      });
    }
</script>







<link rel="stylesheet" href="<?php echo site_url('statics/default/assets/css/bootstrap.css')?>">
<link rel="stylesheet" href="<?php echo site_url('statics/default/assets/css/style.css')?>">
<div class="content">
    <div class="row">
        <div class="col-md-12">
        <div class="card">
            <div class="card-header">
            <h4 class="card-title" id="classimglist_message">Danh sách hashtag</h4>
            </div>
            <div class="card-body">
            <div class="table-responsive">   
                    <a href="<?php echo site_url('admin/hashtag?act=add&token='.$infoLog->token)?>" id="classimglist_add_img" class="btn btn-primary btn-round" >Thêm Hashtag</a>           
            <table class="table table-striped" id="table1">
                <thead class=" text-primary">
                    <th>
                        #
                    </th>
                    <th>
                        Tiêu đề
                    </th>
                    <th>
                        Trend Hashtag
                    </th>
                </thead>
                <tbody>
                    <?php if(!empty($hashtags)):?>
                      <?php foreach($hashtags as $key=>$item):?>
                          <tr>
                               <td>
                                <?php echo $key+1 ?>
                              </td>
                              <td>
                                <?php echo $item->title ?>
                              </td>       
                              <td>
                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" onclick="updateTrend(<?php echo $item->id?>,<?php echo $item->trend?>)" <?php echo $item->trend == 1 ? "checked" : ""?>>
                              </td>                 
                              <td>
                                  <a href="#" onclick="deleteRow(<?php echo $item->id?>)"><i class="fa fa-trash-o" style="font-size:24px"></i></a>
                                  <a href="<?php echo site_url('admin/hashtag?act=profile&id='.$item->id.'&token='.$infoLog->token)?>"><i class="nc-icon nc-settings" style="font-size:24px"></i></a>           
                              </td>
                          </tr>
                      <?php endforeach;?>
                    <?php endif;?>
                </tbody>
                </table>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>

<script src="<?php echo site_url('statics/default/assets/js/simple-datatables.js')?>"></script>
<script>
    let table1 = document.querySelector('#table1');
    let dataTable = new simpleDatatables.DataTable(table1);
</script>
