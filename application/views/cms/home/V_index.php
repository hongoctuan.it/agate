<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="<?php echo site_url('statics/cms/js/common.js')?>"></script>
<script>
  $(document).ready(function(){
  $("#banner_submit").click(function(){

    var file_data = $('#banner_img').prop('files')[0];
    var form_data = new FormData();
    form_data.append('banner_title', $( "#banner_title" ).val());
    form_data.append('banner_img', file_data);
    form_data.append('banner_description', $( "#banner_description" ).val());
    form_data.append('banner_item_name_01', $( "#banner_item_name_01" ).val());
    form_data.append('banner_item_name_02', $( "#banner_item_name_02" ).val());
    form_data.append('banner_item_name_03', $( "#banner_item_name_03" ).val());
    form_data.append('banner_item_name_04', $( "#banner_item_name_04" ).val());
    $.ajax({
        url: '<?php echo site_url('admin/home?act=upd_banner&token='.$infoLog->token)?>', // gửi đến file upload.php 
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function (res) {
          alert(res);
        }
    });
    return false;
  });
  $("#highlight_submit").click(function(){
    var file_data = $('#highlight_img').prop('files')[0];
    let myImg = document.querySelector("#output_highlight_image");
    let realWidth = myImg.naturalWidth;
    let realHeight = myImg.naturalHeight;
    var match = ["image/gif", "image/png", "image/jpg",];
    var form_data = new FormData();
      form_data.append('highlight_img', file_data);
      form_data.append('highlight_title', $( "#highlight_title" ).val());
      form_data.append('highlight_description', $( "#highlight_description" ).val());
      form_data.append('highlight_des_01', $( "#highlight_des_01" ).val());
      form_data.append('highlight_des_02', $( "#highlight_des_02" ).val());
      form_data.append('highlight_des_03', $( "#highlight_des_03" ).val());
      form_data.append('highlight_des_04', $( "#highlight_des_04" ).val());
      form_data.append('highlight_item_name_01', $( "#highlight_item_name_01" ).val());
      form_data.append('highlight_item_name_02', $( "#highlight_item_name_02" ).val());
      form_data.append('highlight_item_name_03', $( "#highlight_item_name_03" ).val());
      form_data.append('highlight_item_name_04', $( "#highlight_item_name_04" ).val());
      form_data.append('highlight_item_url_title_01', $( "#highlight_item_url_title_01" ).val());
      form_data.append('highlight_item_url_title_02', $( "#highlight_item_url_title_02" ).val());
      form_data.append('highlight_item_url_title_03', $( "#highlight_item_url_title_03" ).val());
      form_data.append('highlight_item_url_title_04', $( "#highlight_item_url_title_04" ).val());
      form_data.append('highlight_item_url_01', $( "#highlight_item_url_01" ).val());
      form_data.append('highlight_item_url_02', $( "#highlight_item_url_02" ).val());
      form_data.append('highlight_item_url_03', $( "#highlight_item_url_03" ).val());
      form_data.append('highlight_item_url_04', $( "#highlight_item_url_04" ).val());
      form_data.append('highlight_upcoming_01', $( "#highlight_upcoming_01" ).is(':checked'));
      form_data.append('highlight_upcoming_02', $( "#highlight_upcoming_02" ).is(':checked'));
      form_data.append('highlight_upcoming_03', $( "#highlight_upcoming_03" ).is(':checked'));
      form_data.append('highlight_upcoming_04', $( "#highlight_upcoming_04" ).is(':checked'));
      $.ajax({
          url: '<?php echo site_url('admin/home?act=upd_highlight&token='.$infoLog->token)?>',
          dataType: 'text',
          cache: false,
          contentType: false,
          processData: false,
          data: form_data,
          type: 'post',
          success: function (res) {
            alert(res);
          }
      });
    
        
  });

  $("#comment_submit").click(function(){
    var _comment_list = {"comment_title":$( "#comment_title" ).val(),"comment_description":$( "#comment_description" ).val(),"comment_list":$( "#comment_list" ).val()}
    comment_list= $( "#comment_list" ).val()
    if(comment_list.length == 3){
      $.ajax({
          url: '<?php echo site_url('admin/home?act=upd_comment&token='.$infoLog->token)?>', // gửi đến file upload.php 
          dataType: 'text',
          data: _comment_list,
          type: 'post',
          success: function (res) {
            alert(res);
          }
      });
    }else{
      alert("Phải chọn 3 đủ 3 comment")
    }
    
  });

  $("#staff_submit").click(function(){
  

    staff1_list= $( "#staff1" ).val()
    staff2_list= $( "#staff2" ).val()
    staff3_list= $( "#staff3" ).val()

    var file_data1 = $('#staff1_pc').prop('files')[0];
    var file_data2 = $('#staff2_pc').prop('files')[0];
    var file_data3 = $('#staff3_pc').prop('files')[0];
    var file_data4 = $('#staff1_mb').prop('files')[0];
    var file_data5 = $('#staff2_mb').prop('files')[0];
    var file_data6 = $('#staff3_mb').prop('files')[0];
    var match = ["image/gif", "image/png", "image/jpg"];
    var form_data = new FormData();
      form_data.append('staff1_pc', file_data1);
      form_data.append('staff2_pc', file_data2);
      form_data.append('staff3_pc', file_data3);
      form_data.append('staff1_mb', file_data4);
      form_data.append('staff2_mb', file_data5);
      form_data.append('staff3_mb', file_data6);
      form_data.append('staff1', $("#staff1").val());
      form_data.append('staff2', $("#staff2").val());
      form_data.append('staff3', $("#staff3").val());
    if(staff1_list.length == 1 && staff2_list.length == 1 && staff3_list.length == 1){
      $.ajax({
          url: '<?php echo site_url('admin/home?act=upd_staff&token='.$infoLog->token)?>', // gửi đến file upload.php 
          dataType: 'text',
          cache: false,
          contentType: false,
          processData: false,
          data: form_data,
          type: 'post',
          success: function (res) {
            alert(res);
          }
      });
    }else{
      alert("Mỗi block chọn 1 chuyên gia")
    }
    
  });
  $("#agateimage_submit").click(function(){
    //Lấy ra files
    //sử dụng ajax post 
    var file_data1 = $('#agateimage_01').prop('files')[0];
    var file_data2 = $('#agateimage_02').prop('files')[0];
    var file_data3 = $('#agateimage_03').prop('files')[0];
    var match = ["image/gif", "image/png", "image/jpg"];
    var form_data = new FormData();
      form_data.append('img_01', file_data1);
      form_data.append('img_02', file_data2);
      form_data.append('img_03', file_data3);
      form_data.append('title', $("#agateimage_title").val());
      form_data.append('description', $("#agateimage_description").val());
      $.ajax({
        url: '<?php echo site_url('admin/home?act=upd_class&token='.$infoLog->token)?>', // gửi đến file upload.php 
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function (res) {
          alert(res);
        }
      });
  });
  $("#agatemag_submit").click(function(){
    //Lấy ra files
    //sử dụng ajax post 
    var agatemag = {'agatemag':$("#agatemag").val()}
    $.ajax({
        url: '<?php echo site_url('admin/home?act=upd_agatemag&token='.$infoLog->token)?>', // gửi đến file upload.php 
        dataType: 'text',
        data: agatemag,
        type: 'post',
        success: function (res) {
          alert(res);
        }
    });
  });
  $("#agategift_submit").click(function(){
    var file_data = $('#gift_img').prop('files')[0];
    var form_data = new FormData();
      form_data.append('gift_img', file_data);
      form_data.append('agategift', $( "#agategift" ).val());
      agategift_list= $("#agategift").val()
      if(agategift_list.length == 1){
        $.ajax({
            url: '<?php echo site_url('admin/home?act=upd_agategift&token='.$infoLog->token)?>', // gửi đến file upload.php 
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (res) {
              alert(res);
            }
        });
      }else{
        alert("Chỉ chọn 1 gift")
      }
  });
  $("#slider_submit").click(function(){
    //Lấy ra files
    //sử dụng ajax post 
    var _slider = {'slider':$("#slider").val()}
    slider_list = $("#slider").val();
    if(slider_list.length >= 4 && slider_list.length <=10 ){
      $.ajax({
        url: '<?php echo site_url('admin/home?act=upd_slider&token='.$infoLog->token)?>', // gửi đến file upload.php 
        dataType: 'text',
        data: _slider,
        type: 'post',
        success: function (res) {
          alert(res);
        }
      });
    }else{
      alert("Chọn tối tối thiểu 4 và tối đa 10 ảnh")
    }
    
  });
  $("#agatepartner_submit").click(function(){
    //Lấy ra files
    //sử dụng ajax post 
    var agatepartner = {'agatepartner':$("#agatepartner").val()}
    agatepartner_list = $("#agatepartner").val()
    if(agatepartner_list.length == 2){
      $.ajax({
          url: '<?php echo site_url('admin/home?act=upd_partner&token='.$infoLog->token)?>', // gửi đến file upload.php 
          dataType: 'text',
          data: agatepartner,
          type: 'post',
          success: function (res) {
            alert(res)
          }
      });
    }
    else{
      alert("Phải chọn đủ 2 đối tác")
    }
  });
});

</script>
<?php include "inc_banner.php"?>
<?php include "inc_highlights.php"?>
<?php include "inc_agategift.php"?>
<?php include "inc_comment.php"?>
<?php include "inc_agateimage.php"?>
<?php include "inc_staff.php"?>
