<div class="content" style="margin-top:0px">
        <div class="row">
          <div class="col-md-8">
            <div class="card card-user">
              <div class="card-header">
                <h5 class="card-title" id="highlight_message">Agate Gift</h5>
              </div>
              <div class="card-body">
                <form>
                  <div class="row">
                    <div class="col-md-5 pr-1">
                      <div class="form-group">
                        <label>Hình ảnh (Hình ảnh hiển thị đẹp nhất với size ngang 2076 × cao 2616)</label>
                        <input type="file" class="form-control" id="gift_img" accept="image/*" onchange="preview_image(event,'output_gift_image')"/>
                        <img id="output_gift_image" height="100px" src="<?php echo site_url('assets/public/avatar/'.$home[6]->value)?>"/>
                      </div>
                    </div>
                    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">
              <script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>
              <script>
              $(document).ready(function(){
                var multipleCancelButton = new Choices('#agategift', {
                removeItemButton: true,
                maxItemCount:1,
                searchResultLimit:500,
                renderChoiceLimit:500
                });
              });
              </script>
                    <div class="col-md-12"> 
                      <select id="agategift" placeholder="Chọn 1 gift" multiple>
                        <?php foreach($gifts as $item):?>
                          <?php if($item->id == $gift->id):?>
                            <option value="<?php echo $item->id ?>" selected><?php echo $item->title?></option>
                          <?php else:?>
                            <option value="<?php echo $item->id ?>"><?php echo $item->title?></option>
                          <?php endif;?>                   
                      <?php endforeach; ?>
                      </select> 
                    </div>
                  </div>
                  <div class="row">
                    <div class="update ml-auto mr-auto">
                      <button type="button" id="agategift_submit" class="btn btn-primary btn-round">Lưu</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card card-user">
              <img src="<?php echo site_url('assets/public/avatar/home_gift.png')?>" />
            </div>
          </div>
        </div>
      </div>
