<div class="content" style="margin-top:0px">
        <div class="row">
          <div class="col-md-8">
            <div class="card card-user">
              <div class="card-header">
                <h5 class="card-title" id="highlight_message">Agate Image</h5>
              </div>
              <div class="card-body">
                <form>
                  <div class="row">
                    <div class="col-md-4 pr-1">
                      <div class="form-group">
                        <label>Hình ảnh 1(Hình ảnh hiển thị đẹp nhất với size ngang 1606 × cao 1525)</label>
                        <input type="file" class="form-control" id="agateimage_01" accept="image/*" onchange="preview_image(event,'output_img_01')"/>
                        <img id="output_img_01" height="100px" src="<?php echo site_url('assets/public/avatar/'.$class_select->img_01)?>"/>
                      </div>
                    </div>
                    <div class="col-md-4 pr-1">
                      <div class="form-group">
                        <label>Hình ảnh 2(Hình ảnh hiển thị đẹp nhất với size ngang 753 × cao 716)</label>
                        <input type="file" class="form-control" id="agateimage_02" accept="image/*" onchange="preview_image(event,'output_img_02')"/>
                        <img id="output_img_02" height="50px" src="<?php echo site_url('assets/public/avatar/'.$class_select->img_02)?>"/>
                      </div>
                    </div>
                    <div class="col-md-4 pr-1">
                      <div class="form-group">
                        <label>Hình ảnh 3(Hình ảnh hiển thị đẹp nhất với size ngang 753 × cao 716)</label>
                        <input type="file" class="form-control" id="agateimage_03" accept="image/*" onchange="preview_image(event,'output_img_01')"/>
                        <img id="output_img_03" height="50px" src="<?php echo site_url('assets/public/avatar/'.$class_select->img_03)?>"/>
                      </div>
                    </div>
                    
                    <div class="col-md-12"> 
                      <div class="form-group">
                        <label>Tiêu đề</label>
                        <input type="text" class="form-control" id="agateimage_title" value="<?php echo $class_select->title?>">
                      </div>
                    </div>
                    <div class="col-md-12"> 
                      <div class="form-group">
                        <label>Nội dung</label>
                        <textarea class="form-control" id="agateimage_description"><?php echo $class_select->description?></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="update ml-auto mr-auto">
                      <button type="button" id="agateimage_submit" class="btn btn-primary btn-round">Lưu</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card card-user">
              <img src="<?php echo site_url('assets/public/avatar/home_trainghiem.png')?>" />
            </div>
          </div>
        </div>
      </div>
