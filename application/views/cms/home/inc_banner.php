
<div class="content">
  <div class="row">
    <div class="col-md-8">
      <div class="card card-user">
        <div class="card-header">
          <h5 class="card-title" id="banner_message">Banner</h5>
        </div>
        <div class="card-body">
        <?php echo form_open_multipart("",array('autocomplete'=>"off",'id'=>"fileUploadForm"));?>
            <div class="row">
              <div class="col-md-5 pr-1">
                <div class="form-group">
                  <label>Tiêu đề phần banner</label>
                  <input type="text" class="form-control" id="banner_title" value="<?php echo $home[0]->value?>">
                </div>
              </div>
              <div class="col-md-7">
                <div class="form-group">
                  <label>Mô tả chi tiết</label>
                  <textarea id="banner_description" class="form-control textarea"><?php echo $home[1]->value?></textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Nội dung block 01</label>
                        <textarea id="banner_item_name_01" class="form-control textarea"><?php echo $home[2]->value?></textarea>
                      </div>
                    </div>
              </div>
              <div class="col-md-6">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Nội dung block 02</label>
                        <textarea id="banner_item_name_02" class="form-control textarea"><?php echo $home[3]->value?></textarea>
                      </div>
                    </div>
              </div>
              <div class="col-md-6">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Nội dung block 03</label>
                        <textarea id="banner_item_name_03" class="form-control textarea"><?php echo $home[4]->value?></textarea>
                      </div>
                    </div>
              </div>
              <div class="col-md-6">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Nội dung block 04</label>
                        <textarea id="banner_item_name_04" class="form-control textarea"><?php echo $home[5]->value?></textarea>
                      </div>
                    </div>
              </div>
              <div class="col-md-12">
                    <div class="col-md-12">
                      <div class="form-group">
                      <label>Hình ảnh (Hình ảnh hiển thị đẹp nhất với size 2678px x 3331px)</label>
                      <input type="file" class="form-control" id="banner_img" accept="image/*" onchange="preview_image(event,'output_banner_image')"/>
                      <img id="output_banner_image" height="100px" src="<?php echo site_url('assets/public/avatar/'.$home[7]->value)?>"/>

                    </div>
                    </div>
              </div>
            </div>
      
            <div class="row">
              <div class="update ml-auto mr-auto">
                <button type="btnSubmit" id="banner_submit" class="btn btn-primary btn-round">Lưu</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card card-user">
          <img src="<?php echo site_url('assets/public/avatar/home_banner.png')?>" />
      </div>
    </div>
  </div>
</div>