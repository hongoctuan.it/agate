
<div class="content">
  <div class="row">
    <div class="col-md-8">
      <div class="card card-user">
        <div class="card-header">
          <h5 class="card-title" id="comment_message">Bình Luận</h5>
        </div>
        <div class="card-body">
            <div class="row">
              <div class="col-md-5 pr-1">
                <div class="form-group">
                  <label>Tiêu đề phần comment</label>
                  <input type="text" class="form-control" id="comment_title" placeholder="Company" value="<?php echo $home[50]->value?>">
                </div>
              </div>
              <div class="col-md-7">
                <div class="form-group">
                  <label>Mô tả phần comment</label>
                  <textarea id="comment_description" class="form-control textarea"><?php echo $home[51]->value?></textarea>
                </div>
              </div>
            </div>
            <div class="col-md-12">
                <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">
                <script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>
                <script>
                $(document).ready(function(){
                  var multipleCancelButton = new Choices('#comment_list', {
                  removeItemButton: true,
                  maxItemCount:3,
                  searchResultLimit:500,
                  renderChoiceLimit:500
                  });
                });
                </script>
                  <div class="col-md-12"> 
                    <select id="comment_list" placeholder="Chọn đủ 3 comment" multiple>
                      <?php foreach($comment as $item):?>
                        <?php if($item->id == $comment_01->id || $item->id == $comment_02->id || $item->id == $comment_03->id):?>
                          <option value="<?php echo $item->id ?>" selected><?php echo $item->description?></option>
                        <?php else:?>
                          <option value="<?php echo $item->id ?>"><?php echo $item->description?></option>
                        <?php endif;?>                   
                      <?php endforeach; ?>
                    </select> 
                  </div>
              </div>
            <div class="row">
              <div class="update ml-auto mr-auto">
                <button type="btnSubmit" id="comment_submit" class="btn btn-primary btn-round">Lưu</button>
              </div>
            </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card card-user">
          <img src="<?php echo site_url('assets/public/avatar/home_comment.png')?>" />
      </div>
    </div>
  </div>
</div>