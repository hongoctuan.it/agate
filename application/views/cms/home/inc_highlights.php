<div class="content" style="margin-top:0px">
        <div class="row">
          <div class="col-md-8">
            <div class="card card-user">
              <div class="card-header">
                <h5 class="card-title" id="highlight_message">Giới Thiệu</h5>
              </div>
              <div class="card-body">
                <form>
                  <div class="row">
                    <div class="col-md-5 pr-1">
                      <div class="form-group">
                        <label>Hình ảnh (Hình ảnh hiển thị đẹp nhất với size ngang 1808 × cao 1708)</label>
                        <input type="file" class="form-control" id="highlight_img" accept="image/*" onchange="preview_image(event,'output_highlight_image')"/>
                        <img id="output_highlight_image" height="100px" src="<?php echo site_url('assets/public/avatar/'.$home[10]->value)?>"/>
                      </div>
                    </div>
                    <div class="col-md-7 pr-1">
                      <div class="form-group">
                        <label>Tiêu đề</label>
                        <input type="text" class="form-control" id="highlight_title" value="<?php echo $home[15]->value ?>">
                      </div>
                    </div>
                    <div class="col-md-12 pr-1">
                      <div class="form-group">
                        <label>Nội dung giới thiệu</label>
                        <textarea type="text" class="form-control" id="highlight_description"><?php echo $home[16]->value ?></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Tiêu đề block 01</label>
                          <input type="text" class="form-control" id="highlight_item_name_01" value="<?php echo $home[11]->value ?>">
                        </div>
                      </div>
                      <div class="col-md-12 pr-1">
                        <div class="form-group">
                          <label>Nội dung block 01</label>
                          <textarea class="form-control" id="highlight_des_01"><?php echo $home[28]->value ?></textarea>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Sắp tới</label>
                          <input style="margin-left:5px" class="form-check-input" type="checkbox" value="" id="highlight_upcoming_01" <?php echo ($home[45]->value == 'true') ? "checked" : ""?>>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Tên đường dẫn liên kết</label>
                          <input type="text" class="form-control" id="highlight_item_url_title_01" value="<?php echo $home[37]->value ?>">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Đường dẫn liên kết</label>
                          <input type="text" class="form-control" id="highlight_item_url_01" value="<?php echo $home[41]->value ?>">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Tiêu đề block 01</label>
                          <input type="text" class="form-control" id="highlight_item_name_02" value="<?php echo $home[12]->value ?>">
                        </div>
                      </div>
                      <div class="col-md-12 pr-1">
                        <div class="form-group">
                          <label>Nội dung block 02</label>
                          <textarea class="form-control" id="highlight_des_02"><?php echo $home[29]->value ?></textarea>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Sắp tới</label>
                          <input style="margin-left:5px" class="form-check-input" type="checkbox" value="" id="highlight_upcoming_02" <?php echo ($home[46]->value == 'true') ? "checked" : ""?>>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Tên đường dẫn liên kết</label>
                          <input type="text" class="form-control" id="highlight_item_url_title_02" value="<?php echo $home[38]->value ?>">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Đường dẫn liên kết</label>
                          <input type="text" class="form-control" id="highlight_item_url_02" value="<?php echo $home[42]->value ?>">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Tiêu đề block 03</label>
                          <input type="text" class="form-control" id="highlight_item_name_03" placeholder="Tiêu đề" value="<?php echo $home[13]->value ?>">
                        </div>
                      </div>
                      <div class="col-md-12 pr-1">
                        <div class="form-group">
                          <label>Nội dung block 03</label>
                          <textarea class="form-control" id="highlight_des_03"><?php echo $home[30]->value ?></textarea>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Sắp tới</label>
                          <input style="margin-left:5px" class="form-check-input" type="checkbox" value="" id="highlight_upcoming_03" <?php echo ($home[47]->value == 'true') ? "checked" : ""?>>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Tên đường dẫn liên kết</label>
                          <input type="text" class="form-control" id="highlight_item_url_title_03" value="<?php echo $home[39]->value ?>">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Đường dẫn liên kết</label>
                          <input type="text" class="form-control" id="highlight_item_url_03" value="<?php echo $home[43]->value ?>">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Tiêu đề block 04</label>
                          <input type="text" class="form-control" id='highlight_item_name_04' value="<?php echo $home[14]->value ?>">
                        </div>
                      </div>
                      <div class="col-md-12 pr-1">
                        <div class="form-group">
                          <label>Nội dung block 04</label>
                          <textarea class="form-control" id="highlight_des_04"><?php echo $home[31]->value ?></textarea>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Sắp tới</label>
                          <input style="margin-left:5px" class="form-check-input" type="checkbox" value="" id="highlight_upcoming_04" <?php echo ($home[48]->value == 'true') ? "checked" : ""?>>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Tên đường dẫn liên kết</label>
                          <input type="text" class="form-control" id="highlight_item_url_title_04" value="<?php echo $home[40]->value ?>">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Đường dẫn liên kết</label>
                          <input type="text" class="form-control" id="highlight_item_url_04" value="<?php echo $home[44]->value ?>">
                        </div>
                      </div>
                      </div> 
                  </div>
                  <div class="row">
                    <div class="update ml-auto mr-auto">
                      <button type="button" id="highlight_submit" class="btn btn-primary btn-round">Lưu</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card card-user">
                <img src="<?php echo site_url('assets/public/avatar/home_gioithieu.png')?>" />
            </div>
          </div>
        </div>
      </div>

