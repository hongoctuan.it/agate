<div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-user">
              <div class="card-header">
                <h5 class="card-title" id="slider_message">Slider</h5>
              </div>
              <div class="card-body">
                <form>
                  <div class="row">
                    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">
                    <script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>
                    <script>
                    $(document).ready(function(){

                    var multipleCancelButton = new Choices('#slider', {
                    removeItemButton: true,
                    maxItemCount:500,
                    searchResultLimit:500,
                    renderChoiceLimit:500
                    });


                    });
                    </script>
                    <style>
                      .mt-100 {
                        margin-top: 100px
                    }

                    body {
                        background: #00B4DB;
                        background: -webkit-linear-gradient(to right, #0083B0, #00B4DB);
                        background: linear-gradient(to right, #0083B0, #00B4DB);
                        color: #514B64;
                        min-height: 100vh
                    }
                    </style>
                        <div class="col-md-12"> 
                          <select id="slider" placeholder="Select upto 5 tags" multiple>
                            <?php if(!empty($classimg)):?>
                              <?php foreach($classimg as $img):?>
                                <?php foreach($slider as $item):?>
                                  <?php if($item->id == $img->id):?>
                                    <option value="<?php echo $item->id ?>" selected><?php echo $item->id?></option>
                                  <?php else:?>
                                    <option value="<?php echo $item->id ?>"><?php echo $item->id?></option>
                                  <?php endif;?>      
                              <?php endforeach; ?>
                              <?php endforeach; ?>
                            <?php else:?>
                              <?php foreach($slider as $item):?>
                                    <option value="<?php echo $item->id ?>"><?php echo $item->id?></option>
                              <?php endforeach; ?>
                            <?php endif;?>
                          </select> 
                        </div>
                  </div>
                  <div class="row">
                    <div class="update ml-auto mr-auto">
                      <button type="button" id="slider_submit" class="btn btn-primary btn-round">Lưu</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>