<div class="content" style="margin-top:0px">
  <div class="row">
    <!-- staff -->
    <div class="col-md-8">
      <div class="card card-user">
        <div class="card-header">
          <h5 class="card-title" id="staff_message">Đội ngũ chuyên gia</h5>
        </div>
        <div class="card-body">
          <form>
            <div class="row form-group">
              <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">
              <script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>
              <script>
              $(document).ready(function(){

              var multipleCancelButton = new Choices('#staff1', {
              removeItemButton: true,
              maxItemCount:1,
              searchResultLimit:500,
              renderChoiceLimit:500
              });


              });
              </script>
              <style>
                .mt-100 {
                  margin-top: 100px
              }

              body {
                  background: #00B4DB;
                  background: -webkit-linear-gradient(to right, #0083B0, #00B4DB);
                  background: linear-gradient(to right, #0083B0, #00B4DB);
                  color: #514B64;
                  min-height: 100vh
              }
              </style>
                  <div class="col-md-8"> 
                    <label>Ảnh đại diện PC block 1</label>
                    <input type="file" class="form-control" id="staff1_pc" accept="image/*" onchange="preview_image(event,'output_staff1_pc')"/>
                    <img id="output_staff1_pc" width="70px" src="<?php echo site_url('assets/public/avatar/'.$home[52]->value)?>"/>
                    <label>Ảnh đại diện Mobile block 1</label>
                    <input type="file" class="form-control" id="staff1_mb" accept="image/*" onchange="preview_image(event,'output_staff1_mb')"/>
                    <img id="output_staff1_mb" width="70px" src="<?php echo site_url('assets/public/avatar/'.$home[55]->value)?>"/>
                  </div>
                  <div class="col-md-4"> 
                    <select id="staff1" placeholder="Chọn 1 chuyên gia" multiple>
                      <?php foreach($staff as $item):?>
                        <?php if($item->id == $staff_01->id):?>
                          <option value="<?php echo $item->id ?>" selected><?php echo $item->full_name?></option>
                        <?php else:?>
                          <option value="<?php echo $item->id ?>"><?php echo $item->full_name?></option>
                        <?php endif;?>                   
                      <?php endforeach; ?>
                    </select> 
                  </div>
            </div>
            <div class="row form-group">
              <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">
              <script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>
              <script>
              $(document).ready(function(){

              var multipleCancelButton = new Choices('#staff2', {
              removeItemButton: true,
              maxItemCount:1,
              searchResultLimit:500,
              renderChoiceLimit:500
              });


              });
              </script>
              <style>
                .mt-100 {
                  margin-top: 100px
              }

              body {
                  background: #00B4DB;
                  background: -webkit-linear-gradient(to right, #0083B0, #00B4DB);
                  background: linear-gradient(to right, #0083B0, #00B4DB);
                  color: #514B64;
                  min-height: 100vh
              }
              </style>
                  <div class="col-md-8"> 
                    <label>Ảnh đại diện PC block 2</label>
                    <input type="file" class="form-control" id="staff2_pc" accept="image/*" onchange="preview_image(event,'output_staff2_pc')"/>
                    <img id="output_staff2_pc" width="70px" src="<?php echo site_url('assets/public/avatar/'.$home[54]->value)?>"/>
                    <label>Ảnh đại diện Mobile block 2</label>
                    <input type="file" class="form-control" id="staff2_mb" accept="image/*" onchange="preview_image(event,'output_staff2_mb')"/>
                    <img id="output_staff2_mb" width="70px" src="<?php echo site_url('assets/public/avatar/'.$home[57]->value)?>"/>
                  </div>
                  <div class="col-md-4"> 
                    <select id="staff2" placeholder="Chọn 1 chuyên gia" multiple>
                      <?php foreach($staff as $item):?>
                        <?php if($item->id == $staff_02->id):?>
                          <option value="<?php echo $item->id ?>" selected><?php echo $item->full_name?></option>
                        <?php else:?>
                          <option value="<?php echo $item->id ?>"><?php echo $item->full_name?></option>
                        <?php endif;?>                   
                      <?php endforeach; ?>
                    </select> 
                  </div>
            </div>
            <div class="row form-group">
              <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">
              <script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>
              <script>
              $(document).ready(function(){

              var multipleCancelButton = new Choices('#staff3', {
              removeItemButton: true,
              maxItemCount:1,
              searchResultLimit:500,
              renderChoiceLimit:500
              });


              });
              </script>
              <style>
                .mt-100 {
                  margin-top: 100px
              }

              body {
                  background: #00B4DB;
                  background: -webkit-linear-gradient(to right, #0083B0, #00B4DB);
                  background: linear-gradient(to right, #0083B0, #00B4DB);
                  color: #514B64;
                  min-height: 100vh
              }
              </style>
                  <div class="col-md-8"> 
                    <label>Ảnh đại diện PC block 3</label>
                    <input type="file" class="form-control" id="staff3_pc" accept="image/*" onchange="preview_image(event,'output_staff3_pc')"/>
                    <img id="output_staff3_pc" width="70px" src="<?php echo site_url('assets/public/avatar/'.$home[53]->value)?>"/>
                    <label>Ảnh đại diện Mobile block 3</label>
                    <input type="file" class="form-control" id="staff3_mb" accept="image/*" onchange="preview_image(event,'output_staff3_mb')"/>
                    <img id="output_staff3_mb" width="70px" src="<?php echo site_url('assets/public/avatar/'.$home[56]->value)?>"/>
                  </div>
                  <div class="col-md-4"> 
                    <select id="staff3" placeholder="Chọn 1 chuyên gia" multiple>
                      <?php foreach($staff as $item):?>
                        <?php if($item->id == $staff_03->id):?>
                          <option value="<?php echo $item->id ?>" selected><?php echo $item->full_name?></option>
                        <?php else:?>
                          <option value="<?php echo $item->id ?>"><?php echo $item->full_name?></option>
                        <?php endif;?>                   
                      <?php endforeach; ?>
                    </select> 
                  </div>
            </div>
            <div class="row">
              <div class="update ml-auto mr-auto">
                <button type="button" id="staff_submit" class="btn btn-primary btn-round">Lưu</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card card-user">
          <img src="<?php echo site_url('assets/public/avatar/home_staff.png')?>" />
      </div>
    </div>
    <!-- mag -->
    <div class="col-md-3">
      <div class="card card-user">
        <div class="card-header">
          <h5 class="card-title" id="agatemag_message">Trang thông tin</h5>
        </div>
        <div class="card-body">
          <form>
          <div class="row">
        <script>
        $(document).ready(function(){

        var multipleCancelButton = new Choices('#agatemag', {
        removeItemButton: true,
        maxItemCount:2,
        searchResultLimit:500,
        renderChoiceLimit:500
        });


        });
        </script>
            <div class="col-md-12"> 
              <select id="agatemag" placeholder="Chọn tối đa 2 thông tin" multiple>
                <?php foreach($mag as $item):?>
                  <?php if($item->id == $mag_01->id || $item->id == $mag_02->id):?>
                    <option value="<?php echo $item->id ?>" selected><?php echo $item->title?></option>
                  <?php else:?>
                    <option value="<?php echo $item->id ?>"><?php echo $item->title?></option>
                  <?php endif;?>                   
                <?php endforeach; ?>
              </select> 
            </div>
      </div>
      <div class="row">
        <div class="update ml-auto mr-auto">
          <button type="button" id="agatemag_submit" class="btn btn-primary btn-round">Lưu</button>
        </div>
      </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="card card-user">
          <img src="<?php echo site_url('assets/public/avatar/home_info.png')?>" />
      </div>
    </div>    
    <!-- partner -->
    <div class="col-md-3">
      <div class="card card-user">
        <div class="card-header">
          <h5 class="card-title" id="agatepartner_message">Đối Tác</h5>
        </div>
        <div class="card-body">
          <form>
            <div class="row">
                <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">
                <script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>
                <script>
                $(document).ready(function(){
                  var multipleCancelButton = new Choices('#agatepartner', {
                  removeItemButton: true,
                  maxItemCount:2,
                  searchResultLimit:500,
                  renderChoiceLimit:500
                  });
                });
                </script>
                  <div class="col-md-12"> 
                    <select id="agatepartner" placeholder="Chọn đủ 2 đối tác" multiple>
                      <?php foreach($partner as $item):?>
                        <?php if($item->id == $partner_01->id || $item->id == $partner_02->id):?>
                          <option value="<?php echo $item->id ?>" selected><?php echo $item->name?></option>
                        <?php else:?>
                          <option value="<?php echo $item->id ?>"><?php echo $item->name?></option>
                        <?php endif;?>                   
                      <?php endforeach; ?>
                    </select> 
                  </div>
              </div>
              <div class="row">
                <div class="update ml-auto mr-auto">
                  <button type="button" id="agatepartner_submit" class="btn btn-primary btn-round">Lưu</button>
                </div>
              </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="card card-user">
          <img src="<?php echo site_url('assets/public/avatar/home_partner.png')?>" />
      </div>
    </div>
    <!-- slider -->
    <!--<div class="col-md-3">-->
      <!--<div class="card card-user">-->
      <!--  <div class="card-header">-->
      <!--    <h5 class="card-title" id="slider_message">Ảnh Chương Trình</h5>-->
      <!--  </div>-->
      <!--  <div class="card-body">-->
      <!--    <form>-->
            <!--<div class="row">-->
            <!--  <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">-->
            <!--  <script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>-->
            <!--  <script>-->
            <!--  $(document).ready(function(){-->

            <!--  var multipleCancelButton = new Choices('#slider', {-->
            <!--  removeItemButton: true,-->
            <!--  maxItemCount:10,-->
            <!--  searchResultLimit:500,-->
            <!--  renderChoiceLimit:500-->
            <!--  });-->


            <!--  });-->
            <!--  </script>-->
            <!--  <style>-->
            <!--    .mt-100 {-->
            <!--      margin-top: 100px-->
            <!--  }-->

            <!--  body {-->
            <!--      background: #00B4DB;-->
            <!--      background: -webkit-linear-gradient(to right, #0083B0, #00B4DB);-->
            <!--      background: linear-gradient(to right, #0083B0, #00B4DB);-->
            <!--      color: #514B64;-->
            <!--      min-height: 100vh-->
            <!--  }-->
            <!--  </style>-->
            <!--      <div class="col-md-12"> -->
            <!--      <?php ?>-->
            <!--        <select id="slider" placeholder="Chọn min 4 và max 10 ảnh" multiple>-->
                        
                      <?php 
                    //   if(!empty($classimg)){
                    //       $flag = false;
                    //       foreach($slider as $img){
                    //         foreach($classimg as $item){
                    //           if($item->id == $img->id){
                    //             $flag= true;
                    //             break;
                    //           }
                    //         }
                    //         if($flag == true){
                    //           echo '<option value="'.$item->id.'" selected>'.$item->description.'</option>';
                    //         }else{
                    //           echo '<option value="'.$item->id.'">'.$item->description.'</option>';
                    //         }
                    //       }
                    //   }else{
                    //     foreach($slider as $item){
                    //       echo '<option value="'.$item->id.'">'.$item->description.'</option>';
                    //     }
                    //}
                      ?>
            <!--        </select> -->
            <!--      </div>-->
            <!--</div>-->
            <!--<div class="row">-->
            <!--  <div class="update ml-auto mr-auto">-->
            <!--    <button type="button" id="slider_submit" class="btn btn-primary btn-round">Lưu</button>-->
            <!--  </div>-->
            <!--</div>-->
    <!--      </form>-->
    <!--    </div>-->
    <!--  </div>-->
    <!--</div>-->
    <!--<div class="col-md-3">-->
    <!--  <div class="card card-user">-->
    <!--      <img src="<?php echo site_url('assets/public/avatar/home_classlist.png')?>" />-->
    <!--  </div>-->
    <!--</div>-->
  </div>
</div>