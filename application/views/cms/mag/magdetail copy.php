
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="<?php echo site_url('statics/cms/js/common.js')?>"></script>
<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-user">
        <div class="card-header">
          <h5 class="card-title" id="banner_message">Tạo Magazine</h5>
        </div>
        <div class="card-body">
        <?php echo form_open_multipart(site_url('admin/mag?act=add&token='.$infoLog->token),array('autocomplete'=>"off",'id'=>"fileUploadForm"));?>
          <input type="hidden" name="id" id="id"  class="form-control" value="<?php echo isset($mag)?$mag->id:""?>">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Tiêu đề bài Magazine</label>
                  <input type="text" name="title" id="title"  class="form-control" placeholder="Tiêu đề bài Magazine" value="<?php echo isset($mag)? $mag->title : ""  ?>">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Mô tả ngắn bài Magazine</label>
                  <textarea type="text" name="editor" id="editor" class="form-control"> <?php echo isset($mag)? $mag->description : ""  ?></textarea>

                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Thông tin tác giả.</label>
                  <textarea id="author" name="author" class="form-control textarea"><?php echo isset($mag)? $mag->author : ""  ?></textarea>
                </div>
  
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Nội dung bài Magazine</label>
                  <textarea name="content" class="form-control textarea"><?php echo isset($mag)? $mag->content : ""  ?></textarea>
                  <script>
  ClassicEditor
    .create( document.querySelector( '#editor' ), {
        plugins: [ Essentials, Paragraph, Bold, Italic ],
        toolbar: [ 'bold', 'italic' ]
    } )
    .then( editor => {
        console.log( 'Editor was initialized', editor );
    } )
    .catch( error => {
        console.error( error.stack );
    } );
</script>
                </div>
 
              </div>
            </div>
            <div class="row">
              <div class="col-md-2">
                <div class="form-group">
                  <label>Phân loại Magazine</label>
                  <select class="form-control" name="type" id="type" aria-label="Default select example">
                    <option value="1" <?php echo isset($mag->type) && $mag->type == 1 ? "selected": ""  ?>>Teen</option>
                    <option value="2" <?php echo isset($mag->type) && $mag->type == 2 ? "selected": ""  ?>>Mon</option>
                  </select>
                </div>
              </div>
              <div class="col-md-5">
                <div class="form-group">
                  <label>Hình thumbnail (Rộng 273px x cao 168px)</label>
                  <input type="file" class="form-control" id="mag_img_01" name="mag_img_01" accept="image/*" onchange="preview_image(event,'output_mag_image_01')"/>
                  <img id="output_mag_image_01" width="136px" height="84px" src="<?php echo isset($mag)?site_url('assets/public/avatar/'.$mag->thumbnail):site_url('assets/public/avatar/default.png')?>"/>
                </div>
              </div>
              <div class="col-md-5">
                <div class="form-group">
                  <label>Hình banner (Rộng 940px x cao 500px)</label>
                  <input type="file" class="form-control" id="mag_img_02" name="mag_img_02" accept="image/*" onchange="preview_image(event,'output_mag_image_02')"/>
                  <img id="output_mag_image_02" width="188px" height="100px" src="<?php echo isset($mag)?site_url('assets/public/avatar/'.$mag->banner):site_url('assets/public/avatar/default.png')?>"/>
                </div>
              </div>
            </div>
            <div class="row">
                <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">
                <script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>
                <script>
                $(document).ready(function(){
                  var multipleCancelButton = new Choices('#agatehashtag', {
                  removeItemButton: true,
                  maxItemCount:5,
                  searchResultLimit:5,
                  renderChoiceLimit:5
                  });
                });
                </script>
                <div class="col-md-12">
                  <div class="form-group">
                    <select id="agatehashtag" name="hashtag[]" placeholder="Chọn danh sách hashtag của bài magazine" multiple>
                        <?php if(!empty($hashtag)):?>
                          <?php foreach($hashtags as $item):
                              $flag=false;
                              foreach($hashtag as $item2){
                                if($item->id == $item2->hashtag_id){
                                  $flag=true;
                                  break;
                                }
                              }
                            ?>
                            <?php if($flag==true):?>
                              <option value="<?php echo $item->id ?>" selected><?php echo $item->title?></option>
                            <?php else:?>
                              <option value="<?php echo $item->id ?>"><?php echo $item->title?></option>
                            <?php endif?>
                          <?php endforeach; ?>
                        <?php else:?>
                          <?php foreach($hashtags as $item):?>
                            
                            <option value="<?php echo $item->id ?>"><?php echo $item->title?></option>
                          <?php endforeach; ?>
                        <?php endif;?>
                      </select> 
                  </div>
                </div>
   
            </div>
            <div class="row">
              <div class="update ml-auto mr-auto">
              <a href="<?php echo site_url('admin/mag')?>"><button type="button" class="btn-primary btn-round">Đóng</button></a>
                <button type="submit" id="save_mag" data-id="0" class="btn-primary btn-round">Lưu</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

