
<link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="<?php echo site_url('statics/cms/js/common.js')?>"></script>

<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<!-- <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script> -->
<script src="<?php echo base_url('statics/directory/vendor/summernote/summernote.js'); ?>"></script>

<style>.note-image-input{
  opacity: 1 !important;
  position: unset !important;
}
.form-group .form-control, .input-group .form-control{
  padding-top: 0px !important;
  padding-bottom: 0px !important;

}</style>
<script>
  $(document).ready(function(){
    $("#review_post").click(function(){
      var file_data_2 = $('#mag_img_02').prop('files')[0];
      var file_data_1 = $('#mag_img_01').prop('files')[0];
      var file_data_1_src = ""
      var file_data_2_src = ""
      if(file_data_1==null){
        temp_src_1 = $('#output_mag_image_01').attr('src').split("/")
        file_data_1_src = temp_src_1[temp_src_1.length-1];
      }

      if(file_data_2==null){
        temp_src_2 = $('#output_mag_image_02').attr('src').split("/")
        file_data_2_src = temp_src_2[temp_src_2.length-1];
      }
      var form_data = new FormData();
      form_data.append('thumbnail', file_data_1);
      form_data.append('banner', file_data_2);
      form_data.append('title', $( "#title").val());
      form_data.append('description',$( "#description" ).val());
      form_data.append('content', $( "#content" ).val());
      form_data.append('type', $( "#type" ).val());
      form_data.append('hashtag', $( "#agatehashtag" ).val());
      form_data.append('author', $( "#author" ).val());
      form_data.append('file_data_1_src', file_data_1_src);
      form_data.append('file_data_2_src', file_data_2_src);
      $.ajax({
          url: '<?php echo site_url('info-review');?>', // gửi đến file upload.php 
          dataType: 'text',
          cache: false,
          contentType: false,
          processData: false,
          data: form_data,
          type: 'post',
          success: function (res) {
            if(res!=""){
              alert(res)
            }else{
              window.open("<?php echo site_url('info_process')?>","_blank")
            }
          }
      });
    });
    $("#close_post").click(function(){
      if (confirm("Lưu bài viết trước khi đóng")) {
        window.location.href = "<?php echo site_url('admin/mag')?>";
      }
    })
  })

  
</script>
<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-user">
        <div class="card-header">
          <h5 class="card-title" id="banner_message">Tạo Magazine</h5>
        </div>
        <div class="card-body">
        <?php echo form_open_multipart(site_url('admin/mag?act=add&token='.$infoLog->token),array('autocomplete'=>"off",'id'=>"fileUploadForm"));?>
          <input type="hidden" name="id" id="id"  class="form-control" value="<?php echo isset($mag)?$mag->id:""?>">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Tiêu đề bài Magazine</label>
                  <input type="text" name="title" id="title"  class="form-control" placeholder="Tiêu đề bài Magazine" value="<?php echo isset($mag)? $mag->title : ""  ?>">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Mô tả ngắn bài Magazine</label>
                  <textarea type="text" name="description" id="description" class="form-control"> <?php echo isset($mag)? $mag->description : ""  ?></textarea>
                  <script>
                      $(document).ready(function() {
                        $('#description').summernote({
                          height: 250,
                          toolbar: [
                            // [groupName, [list of button]]
                            ['style', ['bold', 'italic', 'underline', 'clear','fontname']],
                            ['font', ['strikethrough', 'superscript', 'subscript']],
                            ['fontsize', ['fontsize']],
                            ['color', ['color','link']],
                            ['para', ['ul', 'ol', 'paragraph']],
                            ['height', ['height']],
                            ['insert',['picture']],
                            ['view', ['codeview']],
                          ]
                        });
                      });
                    </script>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Thông tin tác giả.</label>
                  <textarea id="author" name="author" class="form-control textarea"><?php echo isset($mag)? $mag->author : ""  ?></textarea>
                </div>
                <script>
                      $(document).ready(function() {
                        $('#author').summernote({
                          height: 250,
                          toolbar: [
                            // [groupName, [list of button]]
                            ['style', ['bold', 'italic', 'underline', 'clear','fontname']],
                            ['font', ['strikethrough', 'superscript', 'subscript']],
                            ['fontsize', ['fontsize']],
                            ['color', ['color','link']],
                            ['para', ['ul', 'ol', 'paragraph']],
                            ['height', ['height']],
                            ['insert',['picture']],
                            ['view', ['codeview']]
                          ]
                        });
                      });
                    </script>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Nội dung bài Magazine</label>
                  <textarea name="content" id="content" class="form-control textarea"><?php echo isset($mag)? $mag->content : ""  ?></textarea>
                </div>
                <script>
                      $(document).ready(function() {
                        $('#content').summernote({
                          height: 250,
                          toolbar: [
                            // [groupName, [list of button]]
                            ['style', ['bold', 'italic', 'underline', 'clear','fontname']],
                            ['font', ['strikethrough', 'superscript', 'subscript']],
                            ['fontsize', ['fontsize']],
                            ['color', ['color','link']],
                            ['para', ['ul', 'ol', 'paragraph']],
                            ['height', ['height']],
                            ['insert',['picture']],
                            ['view', ['codeview']]
                          ]
                        });
                      });
                    </script>
              </div>
            </div>
            <div class="row">
              <div class="col-md-2">
                <div class="form-group">
                  <label>Phân loại Magazine</label>
                  <select class="form-control" name="type" id="type" aria-label="Default select example">
                    <option value="1" <?php echo isset($mag->type) && $mag->type == 1 ? "selected": ""  ?>>Teen</option>
                    <option value="2" <?php echo isset($mag->type) && $mag->type == 2 ? "selected": ""  ?>>Mom</option>
                  </select>
                </div>
              </div>
              <div class="col-md-5">
                <div class="form-group">
                  <label>Hình thumbnail (Rộng 273px x cao 168px)</label>
                  <br>
                  <br>
                  <input type="file" class="form-control" id="mag_img_01" name="mag_img_01" accept="image/*" onchange="preview_image(event,'output_mag_image_01')"/>
                  <img id="output_mag_image_01" width="136px" height="84px" style="object-fit: cover" src="<?php echo isset($mag)?site_url('assets/public/avatar/'.$mag->thumbnail):site_url('assets/public/avatar/default.png')?>"/>
                </div>
              </div>
              <div class="col-md-5">
                <div class="form-group">
                  <label>Hình banner (Rộng 940px x cao 500px)</label>
                  <br>
                  <br>
                  <input type="file" class="form-control" id="mag_img_02" name="mag_img_02" accept="image/*" onchange="preview_image(event,'output_mag_image_02')"/>
                  <img id="output_mag_image_02" width="188px" height="100px" style="object-fit: cover" src="<?php echo isset($mag)?site_url('assets/public/avatar/'.$mag->banner):site_url('assets/public/avatar/default.png')?>"/>
                </div>
              </div>
            </div>
            <div class="row">
                <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">
                <script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>
                <script>
                $(document).ready(function(){
                  var multipleCancelButton = new Choices('#agatehashtag', {
                  removeItemButton: true,
                  maxItemCount:5,
                  searchResultLimit:5,
                  renderChoiceLimit:5
                  });
                });
                </script>
                <div class="col-md-12">
                  <div class="form-group">
                    <select id="agatehashtag" name="hashtag[]" placeholder="Chọn danh sách hashtag của bài magazine" multiple>
                        <?php if(!empty($hashtag)):?>
                          <?php foreach($hashtags as $item):
                              $flag=false;
                              foreach($hashtag as $item2){
                                if($item->id == $item2->hashtag_id){
                                  $flag=true;
                                  break;
                                }
                              }
                            ?>
                            <?php if($flag==true):?>
                              <option value="<?php echo $item->id ?>" selected><?php echo $item->title?></option>
                            <?php else:?>
                              <option value="<?php echo $item->id ?>"><?php echo $item->title?></option>
                            <?php endif?>
                          <?php endforeach; ?>
                        <?php else:?>
                          <?php foreach($hashtags as $item):?>
                            <option value="<?php echo $item->id ?>"><?php echo $item->title?></option>
                          <?php endforeach; ?>
                        <?php endif;?>
                      </select> 
                  </div>
                </div>
   
            </div>
            <div class="row">
              <div class="update ml-auto mr-auto">
                <button type="button" id="review_post" class="btn-primary btn-round" style="background:white; color:green">Preview</button>
                <button type="button"  id="close_post" class="btn-primary btn-round" style="background:white; color:black">Close</button>
                <button type="submit" id="save_mag" data-id="0" class="btn-primary btn-round">Save</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

