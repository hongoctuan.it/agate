<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link href="https://getbootstrap.com/docs/5.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">
<script src="http://code.jquery.com/jquery-1.7.2.js"></script>
<script src="https://code.jquery.com/jquery-1.4.3.min.js" integrity="sha256-+ACzmeXHpSVPxmu0BxF/44294FKHgOaMn3yH0pn4SGo=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>
<script src="<?php echo site_url('statics/cms/js/common.js')?>"></script>
<script>
    function deleteRow(rowId){
      _data = {'id':rowId}
      $.ajax({
          url: '<?php echo site_url('admin/mag?act=del&token='.$infoLog->token)?>',
          dataType: 'text',
          data: _data,
          type: 'post',
          success: function (res) {
            location.reload();
          }
      });
    }
    function lock(status,rowId){
            _data = {'id':rowId, 'status': status}
            $.ajax({
                url: '<?php echo site_url('admin/mag?act=lock&token='.$infoLog->token)?>',
                dataType: 'text',
                data: _data,
                type: 'post',
                success: function (res) {
                  location.reload();
                }
            });

    }

    function updateRow(rowId){
            _data = {'id':rowId}
            $.ajax({
                url: '<?php echo site_url('admin/mag?act=profile&token='.$infoLog->token)?>', // gửi đến file upload.php 
                dataType: 'text',
                data: _data,
                type: 'post',
                success: function (res) {
                  const arr = res.split("~");
                  $('#id').val(arr[0]);
                  $('#title').val(arr[1]);
                  $('#description').val(arr[2]);
                  $("#content").val(arr[3]);
                  $("#video").val(arr[4]);
                  $("#output_mag_image").attr("src","<?php echo base_url('assets/public/avatar/')?>"+arr[5]);
                  $('#temp').html($(arr[6]))   

                  
                  var multipleCancelButton = new Choices(`#choices-multiples-1`, {
                  removeItemButton: true,
                  maxItemCount:5,
                  searchResultLimit:5,
                  renderChoiceLimit:5
                  });
               }
            });

    } 
    $(document).ready(function(){
    var multipleCancelButton = new Choices(`#choices-multiples-1`, {
                  removeItemButton: true,
                  maxItemCount:5,
                  searchResultLimit:5,
                  renderChoiceLimit:5
                  });
    });
</script>







<link rel="stylesheet" href="<?php echo site_url('statics/default/assets/css/bootstrap.css')?>">
<link rel="stylesheet" href="<?php echo site_url('statics/default/assets/css/style.css')?>">
<div class="content">
    <div class="row">
        <div class="col-md-12">
        <div class="card">
            <div class="card-header">
            <h4 class="card-title" id="classimglist_message">Danh sách magazine</h4>
            </div>
            <div class="card-body">
            <div class="table-responsive">   
                    <a href="<?php echo site_url('admin/mag?act=add&token='.$infoLog->token)?>" id="classimglist_add_img" class="btn btn-primary btn-round" >Thêm Magazine</a>           
            <table class="table table-striped" id="table1">
                <thead class=" text-primary">
                    <th>
                        #
                    </th>
                    <th>
                        Tiêu đề
                    </th>
                    <th>
                        Thumbnail
                    </th>
                    <th>
                        Loại
                    </th>
                </thead>
                <tbody>
                    <?php if(!empty($mags)):?>
                      <?php foreach($mags as $key=>$item):?>
                          <tr>
                               <td>
                                <?php echo $key+1 ?>
                              </td>
                              <td>
                                <?php echo $item->title ?>
                              </td>
                              <td>
                                <img src="<?php echo site_url('assets/public/avatar/'.$item->thumbnail)?>" width="50px" style="border-radius:5px"/>
                              </td>
                              <td>
                                <?php if($item->type==1):?>
                                  Teen
                                <?php elseif($item->type==2):?>
                                  Mom
                                <?php endif;?>
                              </td>
                              <td>
                                  <?php if($item->active == 1):?>
                                    <a href="#" onclick="lock(0,<?php echo $item->id?>)" style="color:green">đã duyệt</i></a>
                                  <?php else:?>
                                    <a href="#" onclick="lock(1,<?php echo $item->id?>)" style="color:red">chưa duyệt</i></a>
                                  <?php endif; ?>
                                  <a href="#" onclick="deleteRow(<?php echo $item->id?>)"><i class="fa fa-trash-o" style="font-size:24px"></i></a>
                                  <a href="<?php echo site_url('admin/mag?act=profile&id='.$item->id.'&token='.$infoLog->token)?>"><i class="nc-icon nc-settings" style="font-size:24px"></i></a>           
                              </td>
                          </tr>
                      <?php endforeach;?>
                    <?php endif;?>
                </tbody>
                </table>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>

<script src="<?php echo site_url('statics/default/assets/js/simple-datatables.js')?>"></script>
<script>
    let table1 = document.querySelector('#table1');
    let dataTable = new simpleDatatables.DataTable(table1);
</script>
