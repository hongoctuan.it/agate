<script src="<?php echo site_url('statics/cms/js/common.js')?>"></script>
<script>
    function deleteRow(rowId){
      _data = {'id':rowId}
      $.ajax({
          url: '<?php echo site_url('admin/member?act=del&token='.$infoLog->token)?>', // gửi đến file upload.php 
          dataType: 'text',
          data: _data,
          type: 'post',
          success: function (res) {
            location.reload();
          }
      });
    }
    function lock(status,rowId){
            _data = {'id':rowId, 'status': status}
            $.ajax({
                url: '<?php echo site_url('admin/member?act=lock&token='.$infoLog->token)?>', // gửi đến file upload.php 
                dataType: 'text',
                data: _data,
                type: 'post',
                success: function (res) {
                  location.reload();
                }
            });

    }

    function updateRow(rowId){
            _data = {'id':rowId}
            $.ajax({
                url: '<?php echo site_url('admin/member?act=profile&token='.$infoLog->token)?>', // gửi đến file upload.php 
                dataType: 'text',
                data: _data,
                type: 'post',
                success: function (res) {
                  const arr = res.split("~");
                  $('#id').val(arr[0]);
                  $('#full_name').val(arr[1]);
                  $('#email').val(arr[2]);
                  $("#phone").val(arr[3]);
                  $("#password").val(arr[4]);
                }
            });

    } 
</script>

<div class="content">
    <div class="row">
        <div class="col-md-12">
        <div class="card">
            <div class="card-header">
            <h4 class="card-title" id="classimglist_message">Danh sách học viên</h4>
            </div>
            <div class="card-body">
            <div class="table-responsive">   
            <table class="table">
                <thead class=" text-primary">
                    <th>
                        #
                    </th>
                    <th>
                        Họ và tên
                    </th>
                    <th>
                       Email
                    </th>
                    <th>
                       Số điện thoại
                    </th>
                    <th>
                       Thao tác
                    </th>
                </thead>
                <tbody>
                    <?php if(!empty($users)):?>
                      <?php foreach($users as $key=>$item):?>
                          <tr>
                              <td>
                                <?php echo $key+1?>
                              </td>
                              <td>
                                <?php echo $item->full_name ?>
                              </td>
                              <td>
                                <?php echo $item->email ?>
                              </td>
                              <td>
                                <?php echo $item->phone ?>
                              </td>
                              <td>
                                  <?php if($item->active == 1):?>
                                    <a href="#" onclick="lock(0,<?php echo $item->id?>)"><i class="fa fa-lock" style="font-size:24px"></i></a>
                                  <?php else:?>
                                    <a href="#" onclick="lock(1,<?php echo $item->id?>)"><i class="fa fa-unlock-alt" style="font-size:24px"></i></a>
                                  <?php endif; ?>
                                  <a href="#" onclick="deleteRow(<?php echo $item->id?>)"><i class="fa fa-trash-o" style="font-size:24px"></i></a>
                                  <a href="#" onclick="updateRow(<?php echo $item->id?>)" data-id="<?php echo $item->id?>" data-toggle="modal" data-target="#exampleModal" ><i class="nc-icon nc-settings" style="font-size:24px"></i></a>             

                              </td>
                          </tr>
                      <?php endforeach;?>
                    <?php endif;?>
                </tbody>
                </table>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <?php echo form_open_multipart(site_url('admin/member?act=add&token='.$infoLog->token),array('autocomplete'=>"off",'id'=>"fileUploadForm"));?>
          <div class="form-group">
            <input type="hidden" name="id" id="id"  class="form-control">
            <input type="text" name="full_name" id="full_name"  class="form-control" placeholder="Họ và Tên" value="Chet">
            <input type="email" name="email" id="email" class="form-control" placeholder="Email">
            <input type="text" name="phone" id="phone" class="form-control" placeholder="Số điện thoại">
            <input type="password" name="password" id="password" class="form-control" placeholder="Mật khẩu">
          </div>

          
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn-primary btn-round" data-dismiss="modal">Đóng</button>
        <button type="submit" data-id="0" class="btn-primary btn-round">Lưu</button>
      </div>
      </form>
    </div>
  </div>
</div>