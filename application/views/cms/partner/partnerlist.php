<script src="<?php echo site_url('statics/cms/js/common.js')?>"></script>
<script>
    function deleteRow(rowId){
      _data = {'id':rowId}
      $.ajax({
          url: '<?php echo site_url('admin/partner?act=del&token='.$infoLog->token)?>', // gửi đến file upload.php 
          dataType: 'text',
          data: _data,
          type: 'post',
          success: function (res) {
            location.reload();
          }
      });
    }
    function lock(status,rowId){
            _data = {'id':rowId, 'status': status}
            $.ajax({
                url: '<?php echo site_url('admin/partner?act=lock&token='.$infoLog->token)?>', // gửi đến file upload.php 
                dataType: 'text',
                data: _data,
                type: 'post',
                success: function (res) {
                  location.reload();
                }
            });

    }

    function updateRow(rowId){
      _data = {'id':rowId}
      $.ajax({
          url: '<?php echo site_url('admin/partner?act=profile&token='.$infoLog->token)?>', // gửi đến file upload.php 
          dataType: 'text',
          data: _data,
          type: 'post',
          success: function (res) {
            const arr = res.split("~");
            $('#id').val(arr[0]);
            $('#name').val(arr[1]);
            $('#phone').val(arr[2]);
            $("#address").val(arr[3]);
            $("#description").val(arr[4]);
            $("#output_user_image_01").attr("src","<?php echo base_url('assets/public/avatar/')?>"+arr[5]);
          }
      });
    } 

    function clearData(){
            $('#id').val("");
            $('#name').val("");
            $("#phone").val("");
            $("#address").val("");
            $("#description").val("");
            $("#position_description").val("");
            $("#output_user_image_01").attr("src","<?php echo base_url('assets/public/avatar/default.png')?>")
    } 
    function checkImageSize(){
      let myImg_01 = document.querySelector("#output_user_image_01");
      let realWidth_01 = myImg_01.naturalWidth;
      let realHeight_01 = myImg_01.naturalHeight;
      if(realWidth_01 == 150 && realHeight_01==50){
        return true;
      }else{
        alert("Hình ảnh không đúng kích thước (chiều rộng 150px và chiều cao 50px)");
        return false;
      }
    } 
</script>

<div class="content">
    <div class="row">
        <div class="col-md-12">
        <div class="card">
            <div class="card-header">
            <h4 class="card-title" id="classimglist_message"> Danh sách đối tác</h4>
            </div>
            <div class="card-body">
            <div class="table-responsive">   
                    <button type="button" id="classimglist_add_img" onclick="clearData()" class="btn btn-primary btn-round"data-toggle="modal" data-target="#exampleModal">Thêm đối tác</button>           
            <table class="table">
                <thead class=" text-primary">
                    <th>
                        #
                    </th>
                    <th>
                        Tên đối tác
                    </th>
                    <th>
                        Điện thoại
                    </th>
                    <th>
                        Địa chỉ
                    </th>
                    <th>
                        Thao tác
                    </th>
                </thead>
                <tbody>
                    <?php if(!empty($partners)):?>
                      <?php foreach($partners as $key=>$item):?>
                          <tr>
                              <td>
                                <?php echo $key+1 ?>
                              </td>
                              <td>
                                <?php echo $item->name ?>
                              </td>
                              <td>
                                <?php echo $item->phone ?>
                              </td>
                              <td>
                                <?php echo $item->address ?>
                              </td>
                              <td>
                                  <?php if($item->active == 1):?>
                                    <a href="#" onclick="lock(0,<?php echo $item->id?>)"><i class="fa fa-lock" style="font-size:24px"></i></a>
                                  <?php else:?>
                                    <a href="#" onclick="lock(1,<?php echo $item->id?>)"><i class="fa fa-unlock-alt" style="font-size:24px"></i></a>
                                  <?php endif; ?>
                                  <a href="#" onclick="deleteRow(<?php echo $item->id?>)"><i class="fa fa-trash-o" style="font-size:24px"></i></a>
                                  <a href="#" onclick="updateRow(<?php echo $item->id?>)" data-id="<?php echo $item->id?>" data-toggle="modal" data-target="#exampleModal"><i class="nc-icon nc-settings" style="font-size:24px"></i></a>       

                              </td>
                          </tr>
                      <?php endforeach;?>
                    <?php endif;?>
                </tbody>
                </table>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <?php echo form_open_multipart(site_url('admin/partner?act=add&token='.$infoLog->token),array('autocomplete'=>"off",'id'=>"fileUploadForm"));?>
          <input type="hidden" name="id" id="id"  class="form-control">
          <div class="form-group">
            <div class="col-md-12">
              <div class="form-group">
                <input type="text" name="name" id="name"  class="form-control" placeholder="Tên đối tác">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <input type="text" name="phone" id="phone" class="form-control" placeholder="Số điện thoại">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <input type="text" name="address" id="address" class="form-control" placeholder="Địa chỉ đối tác">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <input type="text" name="description" id="description" class="form-control" placeholder="Mô tả đối tác">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label>Hình đại diện (Hình có tỷ lệ kích thước rộng 150px x cao 50px)</label>
            <input type="file" class="form-control" id="partner_img" name="partner_img" accept="image/*" onchange="preview_image(event,'output_user_image_01')"/>
            <img id="output_user_image_01" height="50px" width="150px"/>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn-primary btn-round" data-dismiss="modal">Đóng</button>
        <button type="submit"  data-id="0" class="btn-primary btn-round">Lưu</button>
      </div>
      </form>
    </div>
  </div>
</div>