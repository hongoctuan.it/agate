
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="<?php echo site_url('statics/cms/js/common.js')?>"></script>
<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-user">
        <div class="card-header">
          <h5 class="card-title" id="banner_message">Tạo bình luận</h5>
        </div>
        <div class="card-body">
        <?php echo form_open_multipart(!isset($red_up)?site_url('admin/red_up?act=add&token='.$infoLog->token):site_url('admin/red_up?act=update&token='.$infoLog->token),array('autocomplete'=>"off",'id'=>"fileUploadForm"));?>
            <input type="hidden" name="id" name="id"  class="form-control" value="<?php echo isset($red_up)?$red_up->id:""?>">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Short Link</label>
                  <input type="text"  name="short_url" class="form-control" value="<?php echo !empty($red_up->short_url)?$red_up->short_url:"" ?>"/>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label>Url</label>
                  <input type="text"  name="url" class="form-control" value="<?php echo !empty($red_up->url)?$red_up->url:"" ?>"/>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <select class="form-select" name="type" aria-label="Default select example">
                    <option value="1">Upload</option>
                    <option value="2">Redirect</option>
                  </select>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group" >
                  <label>File: <?php echo !empty($red_up->file_name)?$red_up->file_name:"" ?></label>
                  <td><input type="file" name="uploadfile" style="opacity:unset; position: unset"/></td>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="update ml-auto mr-auto">
                <button type="submit" data-id="0" class="btn-primary btn-round">Lưu</button>
              </div>
            </div>
        </form>
        </div>
      </div>
    </div>
   
  </div>
</div>


            
