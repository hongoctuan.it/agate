<div class="content">
    <div class="row">
        <div class="col-md-12">
        <div class="card">
            <div class="card-header">
            <h4 class="card-title" id="gift_message">Danh sách đăng kí khoá học và nhận tin</h4>
            </div>
            <div class="card-body">
            <div class="table-responsive">   
              <div class="col-md-12">
                <div class="form-group">
                  <a href="<?php echo site_url('admin/red_up?act=add&token='.$infoLog->token)?>"><button class="btn btn-primary btn-round" data-toggle="modal" data-target="#exampleModal" >Thêm redirect/upload</button> </a>         
                </div>
              </div>    
            <table class="table">
                <thead class=" text-primary">
                    <th>
                      STT
                    </th>
                    <th>
                      Sort url
                    </th>
                    <th>
                      Phân loại
                    </th>
                    <th>
                      Url
                    </th>
                    <th>
                      Cập Nhật
                    </th>
                </thead>
                <tbody>
                  <?php if(!empty($list)):?>
                    <?php foreach($list as $key=>$item):?>
                      <tr>
                          <td>
                            <?php echo $key+1 ?>
                          </td>
                          <td id="title<?php echo $item->id?>">
                            <?php echo $item->short_url ?>
                          </td>
                          <td>
                            <?php if($item->type == 1): ?>
                              Upload
                            <?php elseif($item->type == 2): ?>
                              Redirect
                            <?php endif; ?>
                          </td> 
                          <td id="title<?php echo $item->id?>">
                            <?php echo $item->url ?>
                          </td>
                          <td id="title<?php echo $item->id?>">
                            <a href="<?php echo site_url('admin/red_up?act=update&id='.$item->id.'&token='.$infoLog->token)?>"><i class="nc-icon nc-settings" style="font-size:24px"></i></a>       
                          </td>          
                      </tr>
                    <?php endforeach?>
                  <?php endif;?>
                </tbody>
                </table>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>

