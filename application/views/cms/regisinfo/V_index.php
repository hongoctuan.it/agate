<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="<?php echo site_url('statics/cms/js/common.js')?>"></script>
<script>
  function deleteRow(rowId){
      $("table tbody").find('#'+rowId).each(function(){
          _data = {'id':rowId}
          $(this).parents("tr").remove();
          $.ajax({
              url: '<?php echo site_url('admin/agate_gift?act=upd_removegift&token='.$infoLog->token)?>', // gửi đến file upload.php 
              dataType: 'text',
              data: _data,
              type: 'post',
              success: function (res) {
                  alert(res);
              }
          });
      });
  }

  $(document).ready(function(){
    $(document).on('click','#gift_submit',function(){
      rowId = $(this).attr('data-id');
      if(rowId == 0){
        var file_img_01 = $('#gift_img_01').prop('files')[0];
        var file_img_02 = $('#gift_img_02').prop('files')[0];
        var file_img_03 = $('#gift_img_03').prop('files')[0];
        var file_img_04 = $('#gift_img_04').prop('files')[0];
            var type = file_img_01.type;
            var match = ["image/gif", "image/png", "image/jpg",];
            if (type == match[0] || type == match[1] || type == match[2]) {
                var form_data = new FormData();
                form_data.append('gift_img_01', file_img_01);
                form_data.append('gift_img_02', file_img_02);
                form_data.append('gift_img_03', file_img_03);
                form_data.append('gift_img_04', file_img_04);
                form_data.append('gift_title', $( "#gift_title" ).val());
                form_data.append('gift_description', $( "#gift_description" ).val());
                alert(123)
                $.ajax({
                    url: '<?php echo site_url('admin/agate_gift?act=upd_addgift&token='.$infoLog->token)?>', // gửi đến file upload.php 
                    dataType: 'text',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function (res) {
                      const arr = res.split("-");
                      $("#gift_message").html(res);
                      var markup = '<tr><td>'+arr[1]+'</td><td><img src="<?php echo site_url('assets/public/avatar/')?>'+arr[2]+'" width="200px"/></td><td><button type="button" id="'+arr[0]+'" class="btn btn-primary btn-round" onclick="deleteRow('+arr[0]+')">Xoá</button><button type="button" id="updateRow" class="btn btn-primary btn-round" data-id="'+arr[0]+'"data-toggle="modal" data-target="#exampleModal">Cập nhật</button></td></tr>';
                      $("table tbody").append(markup);
                    }
                });
                //sử dụng ajax post
            } else {
              $("#gift_message").html('that bai');
            }
            return false;
      }else{
          
            // //Lấy ra files
          var file_img_01 = $('#gift_img_01').prop('files')[0];
          var file_img_02 = $('#gift_img_02').prop('files')[0];
          var file_img_03 = $('#gift_img_03').prop('files')[0];
          var file_img_04 = $('#gift_img_04').prop('files')[0];
          //Xét kiểu file được upload
          var match = ["image/gif", "image/png", "image/jpg",];
          var form_data = new FormData();
          var title= $( "#gift_title" ).val();
          //thêm files vào trong form data
          form_data.append('gift_img_01', file_img_01);
          form_data.append('gift_img_02', file_img_02);
          form_data.append('gift_img_03', file_img_03);
          form_data.append('gift_img_04', file_img_04);
          form_data.append('gift_title', $( "#gift_title" ).val());
          form_data.append('gift_description', $( "#gift_description" ).val());
          form_data.append('id', rowId);
          $.ajax({
              url: '<?php echo site_url('admin/agate_gift?act=upd_updategift&token='.$infoLog->token)?>', // gửi đến file upload.php 
              dataType: 'text',
              cache: false,
              contentType: false,
              processData: false,
              data: form_data,
              type: 'post',
              success: function (res) {
                $("#title"+rowId).html(title);
                $("#img"+rowId).attr("src","<?php echo base_url('assets/public/avatar/')?>"+res);
              }
          });

      }
  });

    $(document).on('click','#updateRow',function(){
      rowId = $(this).attr('data-id');
      var id = {'id':rowId}
      $.ajax({
          url: '<?php echo site_url('admin/agate_gift?act=upd_getgift&token='.$infoLog->token)?>', // gửi đến file upload.php 
          dataType: 'text',
          data: id,
          type: 'post',
          success: function (res) {
            const arr = res.split("~");
            $('#gift_title').val(arr[0]);
            $('#gift_description').val(arr[1]);
            $("#output_gift_image_01").attr("src","<?php echo base_url('assets/public/avatar/')?>"+arr[2]);
            $("#output_gift_image_02").attr("src","<?php echo base_url('assets/public/avatar/')?>"+arr[3]);
            $("#output_gift_image_03").attr("src","<?php echo base_url('assets/public/avatar/')?>"+arr[4]);
            $("#output_gift_image_04").attr("src","<?php echo base_url('assets/public/avatar/')?>"+arr[5]);
            $("#gift_submit").attr("data-id",rowId);
          }
      });
  });

  $(document).on('click','#gift_add',function(){
    $('#gift_title').val("");
    $('#gift_description').val("");
    $("#output_gift_image_01").attr("src","");
    $("#output_gift_image_02").attr("src","");
    $("#output_gift_image_03").attr("src","");
    $("#output_gift_image_04").attr("src","");
    $("#gift_submit").attr("data-id",0);
  });

});



</script>

<?php include "inc_regisinfo.php"?>
