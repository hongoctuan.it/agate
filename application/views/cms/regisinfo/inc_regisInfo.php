<div class="content">
    <div class="row">
        <div class="col-md-12">
        <div class="card">
            <div class="card-header">
            <h4 class="card-title" id="gift_message">Danh sách đăng kí khoá học và nhận tin</h4>
            </div>
            <div class="card-body">
            <div class="table-responsive">    
            <a href="<?php echo site_url('admin/agate_regis?act=export&token='.$infoLog->token)?>" id="classimglist_add_img" class="btn btn-primary btn-round" >Xuất file</a>           

            <table class="table">
                <thead class=" text-primary">
                    <th>
                      STT
                    </th>
                    <th>
                      Email
                    </th>
                    <th>
                      Loại đăng kí
                    </th>
                    <th>
                      Họ và Tên
                    </th>
                    <th>
                      Số điện thoại
                    </th>
                    <th>
                      Đối tượng
                    </th>
                    <th>
                      Thời gian
                    </th>
                </thead>
                <tbody>
                    <?php foreach($regisinfo as $key=>$item):?>
                      <tr>
                          <td>
                            <?php echo $key+1 ?>
                          </td>
                          <td id="title<?php echo $item->id?>">
                            <?php echo $item->email ?>
                          </td>
                          <td>
                            <?php if($item->type == 1): ?>
                              Nhận thông tin
                            <?php elseif($item->type == 2): ?>
                              Cập nhật thông tin khoá học
                            <?php elseif($item->type == 3): ?>
                              Tư vấn về khoá học
                            <?php endif; ?>
                          </td>
                          <td>
                            <?php echo $item->name?>
                          </td>
                          <td>
                            <?php echo $item->phone?>
                          </td>
                          <td>
                            <?php if($item->course_type==2) echo "Phụ Huynh"; elseif($item->course_type==1) echo "Học Sinh";?>
                          </td>
                          <td>
                            <?php echo empty($item->time)?"":date("d-m-Y",$item->time)?>
                          </td>
                          <td>
                            <a href="<?php echo site_url('admin/agate_regis?act=del&id='.$item->id.'&token='.$infoLog->token)?>" onclick="deleteRow(<?php echo $item->id?>)"><i class="fa fa-trash-o" style="font-size:24px"></i></a>
                            <a href="<?php echo site_url('admin/agate_regis?act=detail&id='.$item->id.'&token='.$infoLog->token)?>"><i class="nc-icon nc-settings" style="font-size:24px"></i></a>           
                          </td>
                      </tr>
                    <?php endforeach?>
                </tbody>
                </table>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <?php echo form_open_multipart("",array('autocomplete'=>"off",'id'=>"fileUploadForm"));?>
          <div class="form-group">
            <label>First Name</label>
            <input type="text" id="gift_title" class="form-control" placeholder="Company" value="Chet">
            <textarea class="form-control textarea" id="gift_description">Oh so, your weak rhyme You doubt I'll bother, reading into it</textarea>
          </div>
          <div class="form-group">
            <label>Image 01</label>
            <input type="file" class="form-control" id="gift_img_01" accept="image/*" onchange="preview_image(event,'output_gift_image_01')"/>
            <img id="output_gift_image_01" height="50px" width="50px"/>
          </div>
          <div class="form-group">
            <label>Image 02</label>
            <input type="file" class="form-control" id="gift_img_02" accept="image/*" onchange="preview_image(event,'output_gift_image_02')"/>
            <img id="output_gift_image_02" height="50px" width="50px"/>
          </div>
          <div class="form-group">
            <label>Image 03</label>
            <input type="file" class="form-control" id="gift_img_03" accept="image/*" onchange="preview_image(event,'output_gift_image_03')"/>
            <img id="output_gift_image_03" height="50px" width="50px"/>
          </div>
          <div class="form-group">
            <label>Image 04</label>
            <input type="file" class="form-control" id="gift_img_04" accept="image/*" onchange="preview_image(event,'output_gift_image_04')"/>
            <img id="output_gift_image_04" height="50px" width="50px"/>
          </div>
      </form>
      </div>
      <div class="modal-footer">
        <button type="button" id="gift_close" class="btn-primary btn-round" data-dismiss="modal">Close</button>
        <button type="button" id="gift_submit"  data-id="0" class="btn-primary btn-round">Save changes</button>
      </div>
    </div>
  </div>
</div>
