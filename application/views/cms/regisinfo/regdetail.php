
<link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="<?php echo site_url('statics/cms/js/common.js')?>"></script>

<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-user">
        <div class="card-header">
          <h5 class="card-title" id="banner_message">Cập nhật</h5>
        </div>
        <div class="card-body">
        <?php echo form_open_multipart(site_url('admin/agate_regis?act=update&token='.$infoLog->token),array('autocomplete'=>"off",'id'=>"fileUploadForm"));?>
          <input type="hidden" name="id" id="id"  class="form-control" value="<?php echo isset($reg)?$reg->id:""?>">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Email</label>
                  <input type="text" name="email" id="email"  class="form-control" placeholder="Tiêu đề bài Magazine" value="<?php echo isset($reg)? $reg->email : ""  ?>">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Tên</label>
                  <input type="text" name="name" id="name"  class="form-control" placeholder="Tên người đăng ký" value="<?php echo isset($reg)? $reg->name : ""  ?>">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Điện thoại</label>
                  <input type="text" name="phone" id="phone"  class="form-control" placeholder="Điện thoại" value="<?php echo isset($reg)? $reg->phone : ""  ?>">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-2">
                <div class="form-group">
                  <label>Đối tượng</label>
                  <select class="form-control" name="course_type" id="course_type" style="padding:0px !important" aria-label="Default select example">
                    <option value="3" <?php echo isset($reg->course_type) && $reg->course_type == 3 ? "selected": ""  ?>></option>
                    <option value="1" <?php echo isset($reg->course_type) && $reg->course_type == 1 ? "selected": ""  ?>>Học Sinh</option>
                    <option value="2" <?php echo isset($reg->course_type) && $reg->course_type == 2 ? "selected": ""  ?>>Phụ Huynh</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="update ml-auto mr-auto">
                <a href="<?php echo site_url('admin/agate_regis')?>"><button type="button" class="btn-primary btn-round">Đóng</button></a>
                <button type="submit" id="save_mag" data-id="0" class="btn-primary btn-round">Lưu</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

