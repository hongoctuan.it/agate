<script src="<?php echo site_url('statics/cms/js/common.js')?>"></script>
<script>
    function deleteRow(rowId){
      _data = {'id':rowId}
      $.ajax({
          url: '<?php echo site_url('admin/agate_comment?act=del&token='.$infoLog->token)?>', // gửi đến file upload.php 
          dataType: 'text',
          data: _data,
          type: 'post',
          success: function (res) {
            alert(res);
            location.reload();
          }
      });
    }
    function lock(status,rowId){
            _data = {'id':rowId, 'status': status}
            $.ajax({
                url: '<?php echo site_url('admin/agate_comment?act=lock&token='.$infoLog->token)?>', // gửi đến file upload.php 
                dataType: 'text',
                data: _data,
                type: 'post',
                success: function (res) {
                  location.reload();
                }
            });

    }
</script>


<div class="content">
    <div class="row">
        <div class="col-md-12">
        <div class="card">
            <div class="card-header">
            <h4 class="card-title" id="classimglist_message">Danh sách bình luận</h4>
            </div>
            <div class="card-body">
            <div class="table-responsive">   
            <table class="table">
                <thead class=" text-primary">
                    <th>
                        #
                    </th>
                    <th>
                        Nội dung
                    </th>
                    <th>
                        Thời gian
                    </th>
                    <th>
                        Người BL
                    </th>
                    <th>
                        Magazine
                    </th>
                </thead>
                <tbody>
                    <?php if(!empty($regisinfo)):?>
                      <?php foreach($regisinfo as $key=>$item):?>
                          <tr>
                              <td>
                                <?php echo $key+1 ?>
                              </td>
                              <td width="50%">
                                <?php echo $item->description ?>
                              </td>
                              <td>
                                <?php echo date("d-m-Y",$item->time) ?>
                              </td>
                              <td>
                                <?php echo $item->user->full_name ?>
                              </td>
                              <td>
                                <?php echo $item->mag->title ?>
                              </td>
                              <td>
                                  <?php if($item->active == 1):?>
                                    <a href="#" onclick="lock(0,<?php echo $item->id?>)"><i class="fa fa-lock" style="font-size:24px"></i></a>
                                  <?php else:?>
                                    <a href="#" onclick="lock(1,<?php echo $item->id?>)"><i class="fa fa-unlock-alt" style="font-size:24px"></i></a>
                                  <?php endif; ?>
                                  <a href="#" onclick="deleteRow(<?php echo $item->id?>)"><i class="fa fa-trash-o" style="font-size:24px"></i></a>
                              </td>
                          </tr>
                      <?php endforeach;?>
                    <?php endif;?>
                </tbody>
                </table>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>


