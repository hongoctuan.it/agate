<script src="<?php echo site_url('statics/cms/js/common.js')?>"></script>
<script>
    function deleteRow(rowId){
      _data = {'id':rowId}
      $.ajax({
          url: '<?php echo site_url('admin/agate_schedule?act=del&token='.$infoLog->token)?>', // gửi đến file upload.php 
          dataType: 'text',
          data: _data,
          type: 'post',
          success: function (res) {
            alert(res);
            location.reload();
          }
      });
    }
    function lock(status,rowId){
      _data = {'id':rowId, 'status': status}
      $.ajax({
          url: '<?php echo site_url('admin/agate_schedule?act=lock&token='.$infoLog->token)?>', // gửi đến file upload.php 
          dataType: 'text',
          data: _data,
          type: 'post',
          success: function (res) {
            location.reload();
          }
      });
    }

    function updateRow(rowId,session,time,description){
      $('#id').val(rowId);
      $('#session').val($(session).text().trim());
      $('#time').val($(time).text().trim());
      $("#description").val($(description).text().trim());
    } 

    function clearData(){
      $('#id').val("");
      $('#session').val("");
      $('#time').val("");
      $("#description").val("");
    } 

</script>

<div class="content">
    <div class="row">
        <div class="col-md-12">
        <div class="card">
            <div class="card-header">
            <h4 class="card-title" id="classimglist_message">Thời Khoá Biểu</h4>
            </div>
            <div class="card-body">
            <button id="schedule_add" class="btn btn-primary btn-round" data-toggle="modal" data-target="#exampleModal" onclick="clearData()"  >Thêm buổi</button>           

            <div class="table-responsive">   
            <table class="table">
                <thead class=" text-primary">
                    <th>
                        #
                    </th>
                    <th>
                        Buổi
                    </th>
                    <th>
                        Thời gian
                    </th>
                    <th>
                        Nội dung
                    </th>
                    <th>
                        Thao tác
                    </th>
                </thead>
                <tbody>
                    <?php if(!empty($schedules)):?>
                      <?php foreach($schedules as $key=>$item):?>
                          <tr>
                              <td>
                                <?php echo $key+1 ?>
                              </td>
                              <td width="50%" id="session<?php echo $item->id?>">
                                <?php echo $item->session ?>
                              </td>
                              <td id="time<?php echo $item->id ?>">
                                <?php echo $item->time ?>
                              </td>
                              <td id="description<?php echo $item->id ?>">
                                <?php echo $item->description ?>
                              </td>
                              <td>
                                  <?php if($item->active == 1):?>
                                    <a href="#" onclick="lock(0,<?php echo $item->id?>)"><i class="fa fa-lock" style="font-size:24px"></i></a>
                                  <?php else:?>
                                    <a href="#" onclick="lock(1,<?php echo $item->id?>)"><i class="fa fa-unlock-alt" style="font-size:24px"></i></a>
                                  <?php endif; ?>
                                  <a href="#" onclick="updateRow(<?php echo $item->id?>,'#session<?php echo $item->id ?>','#time<?php echo $item->id ?>','#description<?php echo $item->id ?>')" data-id="<?php echo $item->id?>" data-toggle="modal" data-target="#exampleModal" ><i class="nc-icon nc-settings" style="font-size:24px"></i></a> 
                                  <a href="#" onclick="deleteRow(<?php echo $item->id?>)"><i class="fa fa-trash-o" style="font-size:24px"></i></a>
                              </td>
                          </tr>
                      <?php endforeach;?>
                    <?php endif;?>
                </tbody>
                </table>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Thời Khoá Biểu</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <?php echo form_open_multipart(site_url('admin/agate_schedule?act=update&token='.$infoLog->token),array('autocomplete'=>"off",'id'=>"fileUploadForm"));?>
        <input type="hidden" name="id" id="id"  class="form-control">
  
        <div class="form-group">
          <input type="text" name="session" id="session"  class="form-control" placeholder="Buổi học">
        </div>
        <div class="form-group">
          <input type="text" name="time" id="time" class="form-control" placeholder="Thời gian">
        </div>
        <div class="form-group">
          <textarea type="text" name="description" id="description" class="form-control"></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn-primary btn-round" data-dismiss="modal">Đóng</button>
        <button type="submit" data-id="0" class="btn-primary btn-round">Lưu</button>
      </div>
      </form>
    </div>
  </div>
</div>