<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="<?php echo site_url('statics/cms/js/common.js')?>"></script>
<script>
  $(document).ready(function(){ 
  $("#partner_submit").click(function(){
    //Lấy ra files
    //sử dụng ajax post 
    var partner = {'partner':$("#partner").val()}
    partner_list = $("#partner").val()
    if(partner_list.length == 2){
        $.ajax({
          url: '<?php echo site_url('admin/agate_staff?act=upd_partner&token='.$infoLog->token)?>', // gửi đến file upload.php 
          dataType: 'text',
          data: partner,
          type: 'post',
          success: function (res) {
              alert(res);
          }
      });
    }else{
      alert("Phải chọn 2 đối tác")
    }
    
  });

  $("#teacher_submit").click(function(){
    var teacher = {'teacher':$("#teacher").val()}
    teacher_list = $("#teacher").val()
    if(teacher_list.length == 3){
      $.ajax({
          url: '<?php echo site_url('admin/agate_staff?act=upd_teacher&token='.$infoLog->token)?>', // gửi đến file upload.php 
          dataType: 'text',
          data: teacher,
          type: 'post',
          success: function (res) {
            alert(res);
          }
      });
    }else{
      alert("Phải chọn 3 chuyên viên")
    }
  });

  $("#founder_submit").click(function(){
    //Lấy ra files
    //sử dụng ajax post 
    var founder = {'founder':$("#founder").val()}
    $.ajax({
        url: '<?php echo site_url('admin/agate_staff?act=upd_founder&token='.$infoLog->token)?>', // gửi đến file upload.php 
        dataType: 'text',
        data: founder,
        type: 'post',
        success: function (res) {
            alert(res);
        }
    });
  });

  $("#staff_submit").click(function(){
    staff1_list= $( "#staff1" ).val()
    staff2_list= $( "#staff2" ).val()
    staff3_list= $( "#staff3" ).val()

    var file_data1 = $('#staff1_pc').prop('files')[0];
    var file_data2 = $('#staff2_pc').prop('files')[0];
    var file_data3 = $('#staff3_pc').prop('files')[0];
    var file_data4 = $('#staff1_mb').prop('files')[0];
    var file_data5 = $('#staff2_mb').prop('files')[0];
    var file_data6 = $('#staff3_mb').prop('files')[0];
    var match = ["image/gif", "image/png", "image/jpg"];
    var form_data = new FormData();
      form_data.append('staff1_pc', file_data1);
      form_data.append('staff2_pc', file_data2);
      form_data.append('staff3_pc', file_data3);
      form_data.append('staff1_mb', file_data4);
      form_data.append('staff2_mb', file_data5);
      form_data.append('staff3_mb', file_data6);
      form_data.append('staff1', $("#staff1").val());
      form_data.append('staff2', $("#staff2").val());
      form_data.append('staff3', $("#staff3").val());
    if(staff1_list.length == 1 && staff2_list.length == 1 && staff3_list.length == 1){
      $.ajax({
          url: '<?php echo site_url('admin/agate_staff?act=upd_staff&token='.$infoLog->token)?>', // gửi đến file upload.php 
          dataType: 'text',
          cache: false,
          contentType: false,
          processData: false,
          data: form_data,
          type: 'post',
          success: function (res) {
            alert(res);
          }
      });
    }else{
      alert("Mỗi block chọn 1 chuyên gia")
    }
  });
});
</script>
<?php include "inc_partner.php"?>
<?php include "inc_staff.php"?>
<?php include "inc_founder.php"?>
