<div class="content">
  <div class="row">
    <div class="col-md-8">
      <div class="card card-user">
        <div class="card-header">
          <h5 class="card-title" id="partner_message">Đối tác</h5>
        </div>
        <div class="card-body">
          <form>
            <div class="row">
              <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">
              <script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>
              <script>
              $(document).ready(function(){

              var multipleCancelButton = new Choices('#partner', {
              removeItemButton: true,
              maxItemCount:2,
              searchResultLimit:500,
              renderChoiceLimit:500
              });


              });
              </script>
              <style>
                .mt-100 {
                  margin-top: 100px
              }

              body {
                  background: #00B4DB;
                  background: -webkit-linear-gradient(to right, #0083B0, #00B4DB);
                  background: linear-gradient(to right, #0083B0, #00B4DB);
                  color: #514B64;
                  min-height: 100vh
              }
              </style>
                  <div class="col-md-12"> 
                    <select id="partner" placeholder="Chọn tối đa 2 đối tác" multiple >
                      <?php foreach($partner as $item):?>
                        <?php if($item->id == $partner_01->id || $item->id == $partner_02->id):?>
                          <option value="<?php echo $item->id ?>" selected><?php echo $item->name?></option>
                        <?php else:?>
                          <option value="<?php echo $item->id ?>"><?php echo $item->name?></option>
                        <?php endif;?>                   
                      <?php endforeach; ?>
                    </select> 
                  </div>
            </div>
            <div class="row">
              <div class="update ml-auto mr-auto">
                <button type="button" id="partner_submit" class="btn btn-primary btn-round">Lưu</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card card-user">
          <img src="<?php echo site_url('assets/public/avatar/partner.png')?>" />
      </div>
    </div>
  </div>
</div>