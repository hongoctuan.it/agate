
<div class="content">
  <div class="row">
  <div class="col-md-8">
      <div class="card card-user">
        <div class="card-header">
          <h5 class="card-title" id="staff_message">Đội ngũ chuyên gia</h5>
        </div>
        <div class="card-body">
          <form>
            <div class="row form-group">
              <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">
              <script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>
              <script>
              $(document).ready(function(){

              var multipleCancelButton = new Choices('#staff1', {
              removeItemButton: true,
              maxItemCount:1,
              searchResultLimit:500,
              renderChoiceLimit:500
              });


              });
              </script>
              <style>
                .mt-100 {
                  margin-top: 100px
              }

              body {
                  background: #00B4DB;
                  background: -webkit-linear-gradient(to right, #0083B0, #00B4DB);
                  background: linear-gradient(to right, #0083B0, #00B4DB);
                  color: #514B64;
                  min-height: 100vh
              }
              </style>
                  <div class="col-md-9"> 
                    <label>Ảnh PC block 1 (ngang 1398px x cao 1320px)</label>
                    <input type="file" class="form-control" id="staff1_pc" accept="image/*" onchange="preview_image(event,'output_staff1_pc')"/>
                    <img id="output_staff1_pc" width="70px" src="<?php echo site_url('assets/public/avatar/'.$staff[3]->value)?>"/>
                    <label>Ảnh Mobile block 1 (ngang 741px x cao 862px)</label>
                    <input type="file" class="form-control" id="staff1_mb" accept="image/*" onchange="preview_image(event,'output_staff1_mb')"/>
                    <img id="output_staff1_mb" width="70px" src="<?php echo site_url('assets/public/avatar/'.$staff[6]->value)?>"/>
                  </div>
                  <div class="col-md-3"> 
                    <select id="staff1" placeholder="Chọn 1 chuyên gia" multiple>
                      <?php foreach($staffs as $item):?>
                        <?php if($item->id == $staff_01->id):?>
                          <option value="<?php echo $item->id ?>" selected><?php echo $item->full_name?></option>
                        <?php else:?>
                          <option value="<?php echo $item->id ?>"><?php echo $item->full_name?></option>
                        <?php endif;?>                   
                      <?php endforeach; ?>
                    </select> 
                  </div>
            </div>
            <div class="row form-group">
              <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">
              <script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>
              <script>
              $(document).ready(function(){

              var multipleCancelButton = new Choices('#staff2', {
              removeItemButton: true,
              maxItemCount:1,
              searchResultLimit:500,
              renderChoiceLimit:500
              });


              });
              </script>
              <style>
                .mt-100 {
                  margin-top: 100px
              }

              body {
                  background: #00B4DB;
                  background: -webkit-linear-gradient(to right, #0083B0, #00B4DB);
                  background: linear-gradient(to right, #0083B0, #00B4DB);
                  color: #514B64;
                  min-height: 100vh
              }
              </style>
                  <div class="col-md-9"> 
                    <label>Ảnh PC block 2 (ngang 1398px x cao 1320px)</label>
                    <input type="file" class="form-control" id="staff2_pc" accept="image/*" onchange="preview_image(event,'output_staff2_pc')"/>
                    <img id="output_staff2_pc" width="70px" src="<?php echo site_url('assets/public/avatar/'.$staff[5]->value)?>"/>
                    <label>Ảnh Mobile block 2 (ngang 741px x cao 862px)</label>
                    <input type="file" class="form-control" id="staff2_mb" accept="image/*" onchange="preview_image(event,'output_staff2_mb')"/>
                    <img id="output_staff2_mb" width="70px" src="<?php echo site_url('assets/public/avatar/'.$staff[8]->value)?>"/>
                  </div>
                  <div class="col-md-3"> 
                    <select id="staff2" placeholder="Chọn 1 chuyên gia" multiple>
                      <?php foreach($staffs as $item):?>
                        <?php if($item->id == $staff_02->id):?>
                          <option value="<?php echo $item->id ?>" selected><?php echo $item->full_name?></option>
                        <?php else:?>
                          <option value="<?php echo $item->id ?>"><?php echo $item->full_name?></option>
                        <?php endif;?>                   
                      <?php endforeach; ?>
                    </select> 
                  </div>
            </div>
            <div class="row form-group">
              <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">
              <script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>
              <script>
              $(document).ready(function(){

              var multipleCancelButton = new Choices('#staff3', {
              removeItemButton: true,
              maxItemCount:1,
              searchResultLimit:500,
              renderChoiceLimit:500
              });


              });
              </script>
              <style>
                .mt-100 {
                  margin-top: 100px
              }

              body {
                  background: #00B4DB;
                  background: -webkit-linear-gradient(to right, #0083B0, #00B4DB);
                  background: linear-gradient(to right, #0083B0, #00B4DB);
                  color: #514B64;
                  min-height: 100vh
              }
              </style>
                  <div class="col-md-9"> 
                    <label>Ảnh PC block 3 (ngang 1398px x cao 1320px)</label>
                    <input type="file" class="form-control" id="staff3_pc" accept="image/*" onchange="preview_image(event,'output_staff3_pc')"/>
                    <img id="output_staff3_pc" width="70px" src="<?php echo site_url('assets/public/avatar/'.$staff[4]->value)?>"/>
                    <label>Ảnh Mobile block 3 (ngang 741px x cao 862px)</label>
                    <input type="file" class="form-control" id="staff3_mb" accept="image/*" onchange="preview_image(event,'output_staff3_mb')"/>
                    <img id="output_staff3_mb" width="70px" src="<?php echo site_url('assets/public/avatar/'.$staff[7]->value)?>"/>
                  </div>
                  <div class="col-md-3"> 
                    <select id="staff3" placeholder="Chọn 1 chuyên gia" multiple>
                      <?php foreach($staffs as $item):?>
                        <?php if($item->id == $staff_03->id):?>
                          <option value="<?php echo $item->id ?>" selected><?php echo $item->full_name?></option>
                        <?php else:?>
                          <option value="<?php echo $item->id ?>"><?php echo $item->full_name?></option>
                        <?php endif;?>                   
                      <?php endforeach; ?>
                    </select> 
                  </div>
            </div>
            <div class="row">
              <div class="update ml-auto mr-auto">
                <button type="button" id="staff_submit" class="btn btn-primary btn-round">Lưu</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card card-user">
          <img src="<?php echo site_url('assets/public/avatar/home_staff.png')?>" />
      </div>
    </div>
  </div>
</div>