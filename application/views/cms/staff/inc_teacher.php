<div class="content">
  <div class="row">
    <div class="col-md-8">
      <div class="card card-user">
        <div class="card-header">
          <h5 class="card-title" id="teacher_message">Teacher</h5>
        </div>
        <div class="card-body">
          <form>
            <div class="row">
              <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">
              <script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>
              <script>
              $(document).ready(function(){

              var multipleCancelButton = new Choices('#teacher', {
              removeItemButton: true,
              maxItemCount:5,
              searchResultLimit:5,
              renderChoiceLimit:5
              });


              });
              </script>
              <style>
                .mt-100 {
                  margin-top: 100px
              }

              body {
                  background: #00B4DB;
                  background: -webkit-linear-gradient(to right, #0083B0, #00B4DB);
                  background: linear-gradient(to right, #0083B0, #00B4DB);
                  color: #514B64;
                  min-height: 100vh
              }
              </style>
                  <div class="col-md-12"> 
                    <select id="teacher" placeholder="Select upto 5 tags" multiple >
                            <?php foreach($user as $item):?>
                              <?php if($item->id == $teacher_01->id || $item->id == $teacher_02->id || $item->id == $teacher_03->id):?>
                                <option value="<?php echo $item->id ?>" selected><?php echo $item->full_name?></option>
                              <?php else:?>
                                <option value="<?php echo $item->id ?>"><?php echo $item->full_name?></option>
                              <?php endif;?>                   
                            <?php endforeach; ?>
                    </select> 
                  </div>
            </div>
            <div class="row">
              <div class="update ml-auto mr-auto">
                <button type="button" id="teacher_submit" class="btn btn-primary btn-round">Lưu</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card card-user">
          <img src="<?php echo site_url('assets/public/avatar/home_banner.png')?>" />
      </div>
    </div>
  </div>
</div>