<!-- Footer -->
<footer class="footer row">
    <div class="col-lg-5 col-md-6 col-12 footer-register">
        <div>
            <img src="<?php echo site_url('assets/public/avatar/logoft.png')?>"/>
        </div>
        <div class="footer-description">
            <?php echo $footer_description?>
        </div>
        <form class="form-inline col-md-12 row">
          <input style="border-radius:10px" class="form-control  col-md-11 col-10" type="email" id="footer_reginfo_email" placeholder="Đăng ký thông tin" aria-label="Search">
          <button class="btn col-md-1 col-2" id="footer_reginfo_btn" type="button" onclick="regisinfo()"> <img src="<?php echo site_url('assets/public/avatar/reginfoemail.png')?>"/></button>
        </form>
    </div>
    <div class="col-lg-7 col-md-6 col-12 footer-sitemap">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-12">
                <div class="footer-sitemap-title">Sản phẩm</div>
                <div class="footer-sitemap-url"><a href="<?php echo site_url('agate-class')?>">Agate Class</a></div>
                <div class="footer-sitemap-url"><a href="<?php echo site_url('agate-mag/teen')?>">Agate Mag</a></div>
                <div class="footer-sitemap-url"><a href="<?php echo site_url('agate-gift')?>">Agate Gift</a></div>
                <div class="footer-sitemap-url"><a href="#">Agate Cộng Đồng</a></div>
            </div>
            <div class="col-lg-3 col-md-6 col-12">
                <div class="footer-sitemap-title">Về Agate</div>
                <div class="footer-sitemap-url"><a href="<?php echo site_url('doi-ngu')?>">Đội Ngũ</a></div>
                <div class="footer-sitemap-url"><a href="#">Sự Kiện</a></div>
                <div class="footer-sitemap-url"><a href="#">Thư Viện</a></div>
                <div class="footer-sitemap-url"><a href="#">CSR</a></div>
            </div>
            <div class="col-lg-5 col-md-6 col-12">
                <div class="footer-sitemap-title">Liên hệ</div>
                <div class="footer-sitemap-url"><a href="<?php echo site_url('doi-ngu')?>">Địa chỉ: Hồ Chí Minh</a></div>
                <div class="footer-sitemap-url"><a href="#">Phone: (272) 211-7370</a></div>
                <div class="footer-sitemap-url"><a href="#">E-Mail: support@brand.com</a></div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-12 coryright_item">
        <span class="copyright">Agate. All right reserved. © 2021</span>
    </div>
    <div class="col-lg-6 col-md-6 col-12" style="margin-bottom:10px; color:#1F3E4F">
        <span class="copyright">Chính Sách và điều khoản</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="copyright">Quyền Riêng Tư</span>
    </div>
</foorer>
</body>
<script>
    function regisinfo(){
            //Lấy ra files
            //sử dụng ajax post 
            if($("#footer_reginfo_email").val()!=""){
                var _data = {'email':$("#footer_reginfo_email").val(),"type":1}
                $.ajax({
                    url: '<?php echo site_url('dang-ky-thong-tin')?>', // gửi đến file upload.php 
                    dataType: 'text',
                    data: _data,
                    type: 'post',
                    success: function (res) {
                        alert(res);
                    }
                });
            }else{
                alert('Vui lòng nhập địa chỉ email');
            }
    }
</script>
<script src="<?php echo base_url('statics/default/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>


</html>