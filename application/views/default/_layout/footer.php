<!-- Footer -->
<footer class="footer row">
    <div class="col-lg-4 col-md-8 col-12 footer-register">
        <div>
            <img src="<?php echo site_url('assets/public/avatar/logoft.png')?>"/>
        </div>
        <div class="footer-description">
            <?php echo $footer_description?>
        </div>
        <form class="form-inline col-md-12 row footer_regis">
          <input style="border-radius:10px; background: rgba(69, 148, 192, 0.08) !important;" class="form-control  col-md-11 col-12" type="email" id="footer_reginfo_email" placeholder="Đăng ký thông tin" aria-label="Search">
          <button class="btn col-md-1 col-2" id="footer_reginfo_btn" type="button" onclick="regisinfo()"> <img src="<?php echo site_url('assets/public/avatar/reginfoemail.png')?>"/></button>
        </form>
    </div>
    <div class="col-lg-8 col-md-12 col-12 footer-sitemap">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-6">
                <div class="footer-sitemap-title">Sản phẩm</div>
                <div class="footer-sitemap-url"><a href="<?php echo site_url('chuong-trinh')?>">Agate Class</a></div>
                <div class="footer-sitemap-url"><a href="<?php echo site_url('chuong-trinh')?>">Agate Mag</a></div>
                <div class="footer-sitemap-url"><a href="<?php echo site_url('chuong-trinh')?>">Agate Gift</a></div>
                <div class="footer-sitemap-url"><a href="#">Agate Cộng Đồng</a></div>
            </div>
            <div class="col-lg-3 col-md-4 col-6">
                <div class="footer-sitemap-title">Về Agate</div>
                <div class="footer-sitemap-url"><a href="<?php echo site_url('doi-ngu')?>">Đội Ngũ</a></div>
                <div class="footer-sitemap-url"><a href="#">Sự Kiện</a></div>
                <div class="footer-sitemap-url"><a href="#">Thư Viện</a></div>
                <div class="footer-sitemap-url"><a href="#">CSR</a></div>
            </div>
            <div class="col-lg-5 col-md-4 col-12">
                <div class="footer-sitemap-title">Liên hệ</div>
                <div class="footer-sitemap-url"><a href="<?php echo site_url('doi-ngu')?>">Địa chỉ: 243A Nguyễn Thượng Hiền, Bình Thạnh, TP.HCM</a></div>
                <div class="footer-sitemap-url"><a href="#">Phone: (84) 936 614 091</a></div>
                <div class="footer-sitemap-url"><a href="#">Email: youniverse@agate.vn</a></div>
            </div>
        </div>
    </div>
    <div class="col-lg-1 col-md-1 col-12 coryright_item">
    </div>
    <div class="col-lg-5 col-md-12 col-12 coryright_item" style="margin-top:10px">
        <span class="copyright">Agate. All right reserved. © 2021</span>
    </div>
    <div class="col-lg-6 col-md-12 col-12 coryright_item" style="margin-bottom:10px;margin-top:10px; color:#1F3E4F">
        <span class="copyright">Chính Sách và điều khoản</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="copyright">Quyền Riêng Tư</span>
    </div>

</foorer>
</body>
<script>
    function regisinfo(){
            //Lấy ra files
            //sử dụng ajax post 
            if($("#footer_reginfo_email").val()!=""){
                var _data = {'email':$("#footer_reginfo_email").val(),"type":1}
                $.ajax({
                    url: '<?php echo site_url('dang-ky-thong-tin')?>', // gửi đến file upload.php 
                    dataType: 'text',
                    data: _data,
                    type: 'post',
                    // success: function (res) {
                    //     alert(res);
                    // }
                });
                alert("Đăng ký thành công!")
            }else{
                alert('Vui lòng nhập địa chỉ email');
            }
    }
</script>
<?php if($this->uri->segment(1)!="account-info"):?>
    <script src="<?php echo base_url('statics/default/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
  <?php endif;?>


</html>