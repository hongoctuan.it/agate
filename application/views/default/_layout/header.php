<!DOCTYPE html>
<html lang="vi">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:image" content="<?php echo site_url('assets/public/avatar/thumbnail.jpg')?>"/>

        <meta property="og:description" content="agate.vn" />
        
        <meta property="og:url"content="http://agate.vn/" />
        
        <meta property="og:title" content="agate.vn" />


    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="vi">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="description" content="Agate">
    <meta name="msapplication-tap-highlight" content="no">
    <link rel="shortcut icon" type="ico" href="<?php echo site_url('assets/public/avatar/logotab.png')?>" />
    <title>Agate</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>statics/default/bootstrap/dist/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <link rel="stylesheet" href="<?php echo site_url('statics/default/css/common.css'); ?>">
    <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Lexend+Deca:wght@100&family=Lexend:wght@400;700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Spartan:wght@500&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Jost:wght@500&display=swap" rel="stylesheet">
  <?php echo '<link rel="stylesheet" href="' . site_url("statics/default/css/account.css") . '">'?>

    <?php
    switch ($this->uri->segment(1)) {
        case 'chuong-trinh':
            $this->load->view('cms/_layout/datetime');
            echo '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">';

            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/class.css") . '">';
            break;
        case 'doi-ngu':
            echo '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">';
            $this->load->view('cms/_layout/datetime');
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/staff.css") . '">';
            break;
        case 'agate-gift':
          echo '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">';
          $this->load->view('cms/_layout/datetime');
          echo '<link rel="stylesheet" href="' . site_url("statics/default/css/gift.css") . '">';
          break;
        case 'agate-gift-detail':
          $this->load->view('cms/_layout/datetime');
          echo '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">';
          echo '<link rel="stylesheet" href="' . site_url("statics/default/css/gift-detail.css") . '">';
          break;
        case 'agate-mag':
          echo '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">';
          echo '<link rel="stylesheet" href="' . site_url("statics/default/css/mag.css") . '">';
          break;
        case 'agate-mag-detail':
          echo '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">';
          echo '<link rel="stylesheet" href="' . site_url("statics/default/css/mag.css") . '">';
          break;
        case 'hashtag':
          echo '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">';
          echo '<link rel="stylesheet" href="' . site_url("statics/default/css/mag.css") . '">';
          break;
        case 'search':
          echo '<link rel="stylesheet" href="' . site_url("statics/default/css/mag.css") . '">';
          break;
        case 'info_process':
          echo '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">';
          echo '<link rel="stylesheet" href="' . site_url("statics/default/css/mag.css") . '">';
            break;
        case 'account-info':
          $this->load->view('cms/_layout/datetime');
          echo '<link rel="stylesheet" href="' . site_url("statics/default/css/account.css") . '">';
          break;
        default:
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/index.css") . '">';
    }
    ?>
    <script>
      function tracking() {
        $.ajax({
          url: '<?php echo site_url('tracking?act='.$this->uri->segment(1))?>', // gửi đến file upload.php 
          dataType: 'text',
          type: 'post',
      });
      }
    </script>
          <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-JVR9Z52FNC"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-JVR9Z52FNC');
    </script>
</head>
<body class="container" onload="tracking()">
<!-- Messenger Plugin chat Code -->
<div id="fb-root"></div>
  <header>
    <nav class="navbar navbar-expand-lg navbar-expand-md navbar-light">
      <a class="navbar-brand" href="<?php echo site_url(); ?>"><img src="<?php echo site_url('assets/public/avatar/'.$logo)?>" width="53px"/></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <?php 
                switch ($this->uri->segment(1)) {
                  case 'chuong-trinh':
                    echo '<a class="nav-link" style="color:#FFB308 !important; border-bottom: 1px solid;" href="'.site_url("chuong-trinh").'">Chương Trình</a>';
                    break;
                  default:
                    echo '<a class="nav-link" href="'.site_url("chuong-trinh").'">Chương Trình</a>';
                    break;
                }
            ?>            
          </li>
          <li class="nav-item dropdown">
            <?php 
                switch ($this->uri->segment(1)) {
                  case 'agate-mag':
                    echo '<a class="nav-link" style="color:#30A0E0 !important; border-bottom: 1px solid;" href="'.site_url("agate-mag/teen").'">Magazine</a>';
                    break;
                  default:
                    echo '<a class="nav-link" href="'.site_url('agate-mag/teen').'">Magazine</a>';
                    break;
                }
              ?>   
          </li>        
          <li class="nav-item">
            <?php 
                switch ($this->uri->segment(1)) {
                  case 'agate-gift':
                    echo '<a class="nav-link" style="color:#BB521F !important; border-bottom: 1px solid;" href="'.site_url("agate-gift").'">Agate Gift</a>';
                    break;
                  default:
                    echo '<a class="nav-link" href="'.site_url('agate-gift').'">Agate Gift</a>';
                    break;
                }
              ?>      
            
          </li>
          <li class="nav-item">
            <?php 
              switch ($this->uri->segment(1)) {
                case 'doi-ngu':
                  echo '<a class="nav-link" style="color:#BB521F !important; border-bottom: 1px solid;" href="'.site_url("doi-ngu").'">Đội Ngũ</a>';
                  break;
                default:
                  echo '<a class="nav-link " href="'.site_url('doi-ngu').'">Đội Ngũ</a>';
                  break;
              }
            ?>      
          </li>
          <li class="nav-item">
            <?php 
               switch ($this->uri->segment(1)) {
                case 'chuong-trinh':
                  echo '<a href="'.site_url("chuong-trinh#course").'" class="banner_order" style="background: #FFB308 !important;">Đăng Ký Ngay</a>';
                  break;
                case 'doi-ngu':
                  echo '<a href="'.site_url("chuong-trinh#course").'" class="banner_order" style="background: #30A0E0 !important;">Đăng Ký Ngay</a>';
                  break;
                case 'agate-gift':
                  echo '<a href="'.site_url("chuong-trinh#course").'" class="banner_order" style="background: #BB521F !important;">Đăng Ký Ngay</a>';
                  break;
                case 'agate-gift-detail':
                  echo '<a href="'.site_url("chuong-trinh#course").'" class="banner_order" style="background: #BB521F !important;">Đăng Ký Ngay</a>';
                  break;
                default:
                  echo '<a href="'.site_url("chuong-trinh#course").'" class="banner_order" style="background: #30A0E0 !important;">Đăng Ký Ngay</a>';
                  break;
              }
            ?>
          </li>

          <li class="nav-item dropdown picked">
            <?php if(!isset($_SESSION['system']->token) || empty($_SESSION['system']->token)):?>
                <?php if($this->uri->segment(1) == 'agate-mag'): ?>
                    <img src="<?php echo site_url('assets/public/avatar/no-avatar.png')?>" width="32px" onclick="openNav_login()" class="account_avatar"/>
                <?php endif;?>
            <?php else:?>
                <?php if($this->uri->segment(1) == 'agate-mag'): ?>
                  <a class="avatar" href="<?php echo site_url('account-info/'.$_SESSION['system']->id) ?>"><img src="<?php echo (!empty($_SESSION['system']->img)) ? site_url('assets/public/avatar/'.$_SESSION['system']->img): site_url('assets/public/avatar/no-avatar.png')?>" width="32px"/></a>
                  <div><a href="<?php echo site_url('account-info/'.$_SESSION['system']->id) ?>"><?php echo (!empty($_SESSION['system']->name)) ? $_SESSION['system']->name: ""?></a></div>
                <?php endif;?>
            <?php endif;?>
            <?php if($this->uri->segment(2)=="teen"):?>
              <a class="nav-link dropdown-toggle" href="<?php echo site_url('chuong-tring'); ?>" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Teen's Picked
              </a>
              <div class="dropdown-menu dropdownpicked" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="<?php echo site_url('agate-mag/teen')?>" style="color:#30A0E0 !important;text-decoration-line:underline">Teen's Picked</a>
                <a class="dropdown-item" href="<?php echo site_url('agate-mag/mom')?>">Mom's Picked</a>
              </div>
            <?php elseif($this->uri->segment(2)=="mom"):?>
              <a class="nav-link dropdown-toggle" href="<?php echo site_url('chuong-tring'); ?>" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Mom's Picked
              </a>
              <div class="dropdown-menu dropdownpicked" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="<?php echo site_url('agate-mag/teen')?>">Teen's Picked</a>
                <a class="dropdown-item" href="<?php echo site_url('agate-mag/mom')?>"style="color:#30A0E0 !important;text-decoration-line:underline">Mom's Picked</a>
              </div>
            <?php endif;?>
          </li>
        </ul>        
      </div>
    </nav>
  </header>

  <?php 
    if(!empty($_SESSION['system_msg_login'])){
      echo $_SESSION['system_msg_login'];
      $_SESSION['system_msg_login'] = "";
    }
    include "login.php"
  ?>


