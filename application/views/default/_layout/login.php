

<style>
  .sidenav {
    height: 100%;
    width: 0;
    position: fixed;
    z-index: 1;
    top: 0;
    right: 0;
    overflow-x: hidden;
    transition: 0.5s;
    /* padding-top: 60px; */
    background-color: #F7F6F7;
  }
  .sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
  }

  @media screen and (max-height: 450px) {
    .sidenav {padding-top: 15px;}
    .sidenav a {font-size: 18px;}
  }


</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>statics/default/bootstrap/dist/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<div id="mySidenav" class="sidenav">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav_login()">&times;</a>
    <div class="account_form">
        <div class="login_title">Đăng Nhập</div>
        <div class="row">
            <div class="login_question col-lg-7" style="padding-left:0px;">Bạn chưa có tài khoản ?</div><a class="login_registerurl col-lg-5" onclick="changeform('register')">Đăng Ký</a>
        </div>
        <form class="login_form" action="<?php echo site_url('dang-nhap')?>" method="post">
            <div class="form-group">
                <div class="login_label">Email</div>
                <input type="email" class="login_input" id="email" name="email" placeholder="Email" value="<?php echo $this->input->cookie('remember_email',true) != "" ? $this->input->cookie('remember_email',true) : "" ?>">
            </div>
            <div class="form-group">
                <div class="login_label">Mật Khẩu</div>
                <input type="password" id="password" class="login_input" name="password" placeholder="Mật khẩu" value="<?php echo $this->input->cookie('remember_password',true) != "" ? $this->input->cookie('remember_password',true) : "" ?>">
            </div>
            <div class="row">
                <div class="form-check col-lg-6">
                    <input class="form-check-input" type="checkbox" id="flexCheckDefault" name="savepass" <?php echo $this->input->cookie('remember_email',true) != "" ? "checked" : "" ?>/>
                    <label class="login_label" for="flexCheckDefault">Lưu mật khẩu</label>
                </div>
                <!-- <a href="<?php echo site_url("quen-mat-khau");?>" class="login_registerurl col-lg-6" style="padding-top:0px">Quên mật khẩu</a> -->
            </div>
        <input type="submit" class="account_button" value="Đăng Nhập" style="margin-bottom:0px">
        </form>
    </div>
</div>
<div id="register_model" class="sidenav">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav_register()">&times;</a>
    <div class="account_form">
        <div class="login_title">Đăng Ký</div>
        <div class="row">
            <div class="login_question col-lg-7" style="padding-left:0px;">Bạn đã có tài khoản ?</div><a class="login_registerurl col-lg-5" onclick="changeform('login')">Đăng Nhập</a>
        </div>
        <form class="login_form" action="<?php echo site_url('dang-ky')?>" method="post">
            <div class="form-group">
                <div class="login_label">Họ và Tên</div>
                <input type="text" class="login_input" id="register_name" name="register_name" placeholder="Họ và tên">
            </div>
            <div class="form-group">
                <div class="login_label">Số điện thoại</div>
                <input type="text" class="login_input" id="register_phone" name="register_phone" placeholder="Số điện thoại">
            </div>
            <div class="form-group">
                <div class="login_label">Email</div>
                <input type="email" class="login_input" id="register_email" name="register_email" placeholder="Email">
            </div>
            <div class="form-group">
                <div class="login_label">Mật khẩu</div>
                <input type="password" class="login_input" id="register_password" name="password" placeholder="Mật khẩu">
            </div>
            <div class="form-group">
                <div class="login_label">Xác nhận mật khẩu</div>
                <input type="password" class="login_input" id="register_repassword" name="re_password" placeholder="Xác nhận mật khẩu">
            </div>
            <div class="form-group">
                <div class="login_label">Bạn là</div>
                <select class="login_input" id="register_type" name="register_type">
                    <option value="1">Phụ Huynh</option>
                    <option value="2">Học Sinh</option>
                </select>
            </div>
            <input type="submit" class="account_button" value="Đăng ký" style="margin-bottom:0px">
        </form>
    </div>
</div>
<script>
function changeform(name) {
  if(name=="register"){
    closeNav_login()
    openNav_register()
  }else{
    closeNav_register()
    openNav_login()
  }
}
</script>
<script>
function openNav_login() {
    
    if(screen.width < 415){
        document.getElementById("mySidenav").style.width = "300px";
        document.getElementById("register_model").style.width = "0px";
    }else{
        document.getElementById("mySidenav").style.width = "500px";
        document.getElementById("register_model").style.width = "0px";
    }
  
}
function openNav_register() {
    if(screen.width < 415){
        document.getElementById("mySidenav").style.width = "0px";
        document.getElementById("register_model").style.width = "300px";
    }else{
        document.getElementById("mySidenav").style.width = "0px";
        document.getElementById("register_model").style.width = "500px";
    }
}
function closeNav_login() {
  document.getElementById("mySidenav").style.width = "0";
}
function closeNav_register() {
  document.getElementById("register_model").style.width = "0";
}
</script>