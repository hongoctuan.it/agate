<?php echo form_open_multipart(site_url('update-password'),array('autocomplete'=>"off",'id'=>"fileUploadForm"));?>
  <?php if(isset($_SESSION['system_msg'])){
    echo $_SESSION['system_msg'];
    $_SESSION['system_msg'] = "";
  } ?>
  <div class="form-group">
    <input type="hidden" name="id" value="<?php echo $_SESSION['system']->id ?>"/>
    <label class="changepass_title" for="exampleFormControlInput1">Thay đổi mật khẩu</label>
  </div>
  <div class="form-group">
    <label for="exampleFormControlSelect1" class="changepass_item_title">Nhập mật khẩu cũ</label>
    <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Mật khẩu cũ">
  </div>
  <div class="form-group">
    <label for="exampleFormControlSelect2">Nhập mật khẩu mới</label>
    <input type="password" class="form-control" id="inputPassword" name="new_password" placeholder="Mật khẩu mới">

  </div>
  <div class="form-group">
    <label for="exampleFormControlTextarea1">Nhập lại mật khẩu mới</label>
    <input type="password" class="form-control" id="inputPassword" name="renew_password" placeholder="Mật khẩu mới">
  </div>
  <div class="form-row">
    <div class="col">
      <button type="submit" class="btn btn-primary user_info_editbtn" style="background: #30A0E0;">Lưu</button>
    </div>
    <div class="col">
      <button type="reset" class="btn btn-light user_info_editbtn">Huỷ</button>
    </div>
    
  </div>

</form>