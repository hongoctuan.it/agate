<link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="<?php echo site_url('statics/cms/js/common.js')?>"></script>

<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<style>.note-image-input{
  opacity: 1 !important;
  position: unset !important;
}
.form-group .form-control, .input-group .form-control{
  padding-top: 0px !important;
  padding-bottom: 0px !important;

}</style>
<h3 class="post_title">Tạo Bài Viết</h3>
<?php echo form_open_multipart(site_url('add-post'),array('autocomplete'=>"off",'id'=>"fileUploadForm"));?>
<input type="hidden" name="id" value="<?php echo (!empty($_SESSION['system']->id)) ? $_SESSION['system']->id: ""?>">

        <div class="col-md-12">
          <div class="form-group">
            <label>Tiêu đề bài Magazine</label>
            <input type="text" name="title" id="post_title"  class="form-control" placeholder="Tiêu đề bài Magazine" value="<?php echo isset($mag)? $mag->title : ""  ?>">
          </div>
          <div class="form-group">
            <label>Mô tả ngắn bài Magazine</label>
            <textarea type="text" name="description" id="post_description" class="form-control"> <?php echo isset($mag)? $mag->description : ""  ?></textarea>
            <script>
                      $(document).ready(function() {
                        $('#post_description').summernote({
                          height: 250,
                          toolbar: [
                            // [groupName, [list of button]]
                            ['style', ['bold', 'italic', 'underline', 'clear','fontname']],
                            ['font', ['strikethrough', 'superscript', 'subscript']],
                            ['fontsize', ['fontsize']],
                            ['color', ['color','link']],
                            ['para', ['ul', 'ol', 'paragraph']],
                            ['height', ['height']],
                            ['insert',['picture']]
                          ]
                        });
                      });
                    </script>
          </div>
          <div class="form-group">
            <label>Nội dung bài Magazine</label>
            <textarea type="text" class="form-control" id="post_content" name="content" aria-describedby="emailHelp"></textarea>
          </div>
          <script>
                      $(document).ready(function() {
                        $('#post_content').summernote({
                          height: 250,
                          toolbar: [
                            // [groupName, [list of button]]
                            ['style', ['bold', 'italic', 'underline', 'clear','fontname']],
                            ['font', ['strikethrough', 'superscript', 'subscript']],
                            ['fontsize', ['fontsize']],
                            ['color', ['color','link']],
                            ['para', ['ul', 'ol', 'paragraph']],
                            ['height', ['height']],
                            ['insert',['picture']]
                          ]
                        });
                      });
                    </script>
          <div class="row">
              <div class="col-md-2">
                <div class="form-group">
                  <label>Phân loại Magazine</label>
                  <select class="form-control" name="type" id="post_type" aria-label="Default select example">
                    <option value="1" <?php echo isset($mag->type) && $mag->type == 1 ? "selected": ""  ?>>Teen</option>
                    <option value="2" <?php echo isset($mag->type) && $mag->type == 2 ? "selected": ""  ?>>Mon</option>
                  </select>
                </div>
              </div>
              <div class="col-md-5">
                <div class="form-group">
                  <label>Hình thumbnail (Rộng 273px x cao 168px)</label>
                  <label for="mag_img_01">
                    <input type="file" class="form-control" id="mag_img_01" name="mag_img_01" accept="image/*" onchange="preview_image(event,'output_mag_image_01')"  style="display:none;"/>
                    <img id="output_mag_image_01" width="188px" height="100px" src="<?php echo isset($mag)?site_url('assets/public/avatar/'.$mag->banner):site_url('assets/public/avatar/default.png')?>"/>
                  </label>
                 </div>
              </div>
              <div class="col-md-5">
                <div class="form-group">
                  <label>Hình banner (Rộng 940px x cao 500px)</label>
                  <label for="mag_img_02">
                    <input type="file" class="form-control" id="mag_img_02" name="mag_img_02" accept="image/*" onchange="preview_image(event,'output_mag_image_02')"  style="display:none;"/>
                    <img id="output_mag_image_02" width="188px" height="100px" src="<?php echo isset($mag)?site_url('assets/public/avatar/'.$mag->banner):site_url('assets/public/avatar/default.png')?>"/>
                  </label>
                </div>
              </div>
            </div>
<!-- bat dau -->
            <div class="row">
                <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">
                <script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>
                <script>
                $(document).ready(function(){
                  var multipleCancelButton = new Choices('#agatehashtag', {
                  removeItemButton: true,
                  maxItemCount:5,
                  searchResultLimit:5,
                  renderChoiceLimit:5
                  });
                });
                </script>
                <div class="col-md-12">
                  <div class="form-group">
                    <select id="agatehashtag" name="hashtag[]" placeholder="Chọn danh sách hashtag của bài magazine" multiple>
                        <?php if(!empty($hashtag)):?>
                          <?php foreach($hashtags as $item):
                              $flag=false;
                              foreach($hashtag as $item2){
                                if($item->id == $item2->hashtag_id){
                                  $flag=true;
                                  break;
                                }
                              }
                            ?>
                            <?php if($flag==true):?>
                              <option value="<?php echo $item->id ?>" selected><?php echo $item->title?></option>
                            <?php else:?>
                              <option value="<?php echo $item->id ?>"><?php echo $item->title?></option>
                            <?php endif?>
                          <?php endforeach; ?>
                        <?php else:?>
                          <?php foreach($hashtags as $item):?>
                            
                            <option value="<?php echo $item->id ?>"><?php echo $item->title?></option>
                          <?php endforeach; ?>
                        <?php endif;?>
                      </select> 
                  </div>
                </div>
   
            </div>
<!-- ket thuc -->
          <div class="form-group" style="text-align:end">
            <button type="button" id="review_post" class="review_post"><img src="<?php echo site_url('assets/public/avatar/review.png')?>" width="32px"/></button>
            <!-- <button type="button" id="reviewpost" class="review_post" data-toggle="modal" data-target="#reviewmodal"><img src="<?php echo site_url('assets/public/avatar/review.png')?>" width="32px"/></button> -->
            <button type="submit" class="btn btn-primary save_post">Hoàn tất</button>
          </div>
        </div>
        

      </form>