<h3 class="post_title">Quản lý tin đăng</h3>

<!-- <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script> -->
<script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>

<script type="text/javascript" charset="utf-8">
                $(document).ready(function() {
                    $('#example').DataTable({
    "language": {
        "sProcessing":    "Procesando...",
        "sLengthMenu":    "Hiển thị _MENU_ trên trang",
        "sZeroRecords":   "Không tìm thấy dữ liệu",
        "sEmptyTable":    "Không tìm thấy dữ liệu",
        "sInfo":          "Hiển thị từ _START_ đến _END_ của _TOTAL_ dòng",
        "sInfoEmpty":     "Hiển thị từ 0 đến 0 của 0 dòng",
        "sInfoPostFix":   "",
        "sSearch":        "Tìm kiếm:",
        "sUrl":           "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Đầu tiên",
            "sLast":    "Cuối cùng",
            "sNext":    ">>",
            "sPrevious": "<<"
        },
    }
});
} );
</script> 
<table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Mã Tin</th>
                <th>Ảnh</th>
                <th>Tiêu đề</th>
                <th>TT Tin</th>
                <th>Ngày tạo</th>
                <th>Ngày duyệt</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($mags as $item):?>
                <tr>
                    <td><?php echo $item->id ?></td>
                    <td><img src="<?php echo site_url('assets/public/avatar/'.$item->thumbnail)?>" width="100px"/></td>
                    <td><?php echo $item->title ?></td>
                    <td>
                        <?php echo $item->active == 1 ? "<div style='color:green'>Đã duyệt</div>" :"<div style='color:red'>Chưa duyệt</div>" ?>
                    </td>
                    <td><?php echo (date("d-m-Y",$item->time));?></td>
                    <td><?php echo $item->time_active != "0" ? (date("d-m-Y",$item->time_active)) : ""; ?></td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>