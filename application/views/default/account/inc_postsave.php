<h3 class="postsave">Danh sách tin lưu</h3>
<div class="post_save_list">
    <?php foreach($mags_save as $item):?>
    <div class="post_item">
        <div class="row">
            <div class="user_info_title col-lg-4">
                <img src="<?php echo site_url('assets/public/avatar/'.$item->thumbnail)?>" width="100%"/>
            </div>
            <div class="col-lg-8">
                <div class="user_postitem_title"><a class="hashtag" href="<?php echo site_url('agate-mag-detail/'.$item->slug);?>"><?php echo $item->title?></a></div>
                <div class="user_postitem_content"><?php echo $item->description?></div>
                <img class="nextbutton" src="<?php echo site_url('assets/public/avatar/nextbtn.png')?>" />
                <span class="time">20/11/2021</span> - 
                <span>
                    <?php foreach($item->hashtag as $hashtag):?>
                        <a class="hashtag" href="<?php echo site_url('hashtag/'.$hashtag->id);?>"># <?php echo $hashtag->title ?></a>
                    <?php endforeach;?>
                </span>
            </div>
        </div>
    </div>
    <?php endforeach;?>
</div>