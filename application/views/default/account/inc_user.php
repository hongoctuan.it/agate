<!-- <div class="container"> -->
<div class="row">
      <div class="col-lg-5 col-md-5 col-12">
        <div class="user_info">
          <div>Thông tin cá nhân</div>
          <div class="row"><div class="user_info_title col-lg-3 col-md-5">Vai trò: </div>
            <div class="user_info_content col-lg-9 col-md-5">
              <?php 
                if($info->role==1) echo "Học sinh";
                else if($info->role==2) echo "Phụ huynh";
              ?>
            </div>
          </div>
          <div class="row">
            <div class="user_info_title col-lg-3 col-md-5">Giới tính: </div>
            <div class="user_info_content col-lg-9 col-md-5">
              <?php 
                if($info->role==1) echo "Nam";
                else if($info->role==0) echo "Nữ";
              ?>
            </div>
          </div>
          <div class="row">
            <div class="user_info_title col-lg-3 col-md-5">Ngày sinh: </div>
              <div class="user_info_content col-lg-9 col-md-5">
                <?php 
                  if(!empty($info->birth_day)) echo $info->birth_day;
                ?>
              </div>
            </div>
          <div class="row">
            <div class="user_info_title col-lg-3 col-md-5">Điện thoại: </div>
            <div class="user_info_content col-lg-9 col-md-5">
              <?php 
                if(!empty($info->phone)) echo $info->phone;
              ?>
            </div>
          </div>
          <div class="row">
            <div class="user_info_title col-lg-3 col-md-5">Email: </div>
            <div class="user_info_content col-lg-9 col-md-5">
              <?php 
                if(!empty($info->email)) echo $info->email;
              ?>
            </div>
          </div>
          <div class="row"><div class="user_info_title col-lg-3 col-md-5">Địa chỉ: </div>
            <div class="user_info_content col-lg-9 col-md-5">
              <?php 
                if(!empty($info->address)) echo $info->address;
              ?>
            </div>
          </div>
          <button type="button" class="btn btn-light user_info_editbtn"  data-toggle="modal" data-target="#exampleModal">Chỉnh sửa thông tin</button>
          <button type="button" id="logout_submit" class="btn btn-light user_info_editbtn">Đăng xuất</button>

        </div>
      </div>
      <div class="col-lg-7 col-md-7 col-12">
        <?php foreach($mags as $item): ?>

        <div class="post_item">
          <div class="row">
            <div class="user_info_title col-lg-4">
              <img src="<?php echo site_url('assets/public/avatar/'.$item->thumbnail)?>" width="100%"/>
            </div>
            <div class="col-lg-8">
              <a href="<?php echo site_url("agate-mag-detail/".$item->slug); ?>"><div class="user_postitem_title"><?php echo $item->title ?></div></a>
              <div class="user_postitem_content"><?php echo $item->description ?></div>
              <img class="nextbutton" src="<?php echo site_url('assets/public/avatar/nextbtn.png')?>" />
              <span class="time">20/11/2021</span> - <span>
                <?php foreach($item->hashtag as $hashtag_item): ?>
                  <a class="hashtag" href="#">#<?php echo $hashtag_item->title ?></a> 
                <?php endforeach; ?>
              </span>
            </div>
          </div>
        </div> 
        <?php endforeach; ?>

      </div>
  </div>
  <!-- </div> -->