<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<style>

.info_form {
  width: 60%;
  margin: 60px auto;
  padding: 60px 120px 80px 120px;
  text-align: center;
  border: 1px solid black;
  border-radius:10px;
}
label {
  display: block;
  position: relative;
  margin: 40px 0px;
}
.label-txt {
  position: absolute;
  top: -1.6em;
  padding: 10px;
  font-family: sans-serif;
  font-size: .8em;
  letter-spacing: 1px;
  color: rgb(120,120,120);
  transition: ease .3s;
}
.input {
  width: 100%;
  padding: 10px;
  background: transparent;
  border: none;
  outline: none;
}

.line-box {
  position: relative;
  width: 100%;
  height: 2px;
  background: #BCBCBC;
}

.line {
  position: absolute;
  width: 0%;
  height: 2px;
  top: 0px;
  left: 50%;
  transform: translateX(-50%);
  background: #8BC34A;
  transition: ease .6s;
}

.input:focus + .line-box .line {
  width: 100%;
}

.label-active {
  top: -3em;
}

button {
  display: inline-block;
  padding: 12px 24px;
  background: rgb(220,220,220);
  font-weight: bold;
  color: rgb(120,120,120);
  border: none;
  outline: none;
  border-radius: 3px;
  cursor: pointer;
  transition: ease .3s;
}

button:hover {
  background: #8BC34A;
  color: #ffffff;
}

</style>
<script>
  function preview_image(event,image) 
{
  var reader = new FileReader();
  reader.onload = function()
  {
    var output = document.getElementById(image);
    output.src = reader.result;
  }
  reader.readAsDataURL(event.target.files[0]);
}

$(document).ready(function(){
  $("#info_submit").click(function(){
    var newpassword = $( "#newpassword" ).val()
    var renewpassword = $( "#renewpassword" ).val()
    if(newpassword != renewpassword){
      alert("Mật khẩu và mật khẩu nhập lại không đúng")
    }else{
        //Lấy ra files
        var file_data = $('#info_img').prop('files')[0];
        //Xét kiểu file được upload
        var match = ["image/gif", "image/png", "image/jpg",];
        //kiểm tra kiểu file
        //khởi tạo đối tượng form data
        var form_data = new FormData();
        //thêm files vào trong form data
        form_data.append('img', file_data);
        form_data.append('phone', $( "#phone" ).val());
        form_data.append('id', $( "#id" ).val());
        form_data.append('name', $( "#name" ).val());
        form_data.append('newpassword', $( "#newpassword" ).val());
        //sử dụng ajax post
        $.ajax({
            url: '<?php echo site_url('update-info?token='.$infoLog->token)?>', // gửi đến file upload.php 
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (res) {
              alert(res);
              location.reload();
        }
      });
    }
  });
  $("#logout_submit").click(function(){
    window.location.href = "<?php echo site_url('dang-xuat')?>";
  });
});

</script>
<form class="info_form"> 
  <input type="hidden" id="id" value="<?php echo $infoLog->id ?>"/>  

  <label>
    <img id="info_out_img" height="60px" width="60px" src="<?php echo (!empty($_SESSION['system']->img)) ? site_url('assets/public/avatar/'.$_SESSION['system']->img): site_url('assets/public/avatar/no-avatar.png')?>" style="margin-bottom:20px"/>
    <input class="form-control form-control-lg" id="info_img" type="file" accept="image/*" onchange="preview_image(event,'info_out_img')"/>  
  </label>
  <label>
    <p class="label-txt">Họ tên</p>
    <input type="text" class="input" id="name" value="<?php echo $infoLog->name ?>">
    <div class="line-box">
      <div class="line"></div>
    </div>
  </label>
  <label>
    <p class="label-txt">Email</p>
    <input type="text" class="input" id="email" value="<?php echo $infoLog->email ?>" readonly>
    <div class="line-box">
      <div class="line"></div>
    </div>
  </label>
  <label>
    <p class="label-txt">Số điện thoại</p>
    <input type="text" class="input" id="phone" value="<?php echo $infoLog->phone ?>">
    <div class="line-box">
      <div class="line"></div>
    </div>
  </label>
  <label>
    <p class="label-txt">Vị trí</p>
    <input type="text" class="input" id="position" value="<?php echo $infoLog->position ?>" readonly>
    <div class="line-box">
      <div class="line"></div>
    </div>
  </label>
  <label>
    <p class="label-txt">Mật khẩu mới</p>
    <input type="text" class="input" id="newpassword">
    <div class="line-box">
      <div class="line"></div>
    </div>
  </label>
  <label>
    <p class="label-txt">Nhập lại mật khẩu mới</p>
    <input type="text" class="input" id="renewpassword">
    <div class="line-box">
      <div class="line"></div>
    </div>
  </label>
  <button type="button" class="nav_order" id="logout_submit">Đăng xuất</button> 
  <button type="button" class="nav_order" id="info_submit">Gửi</button>
</form>