<script>
  function preview_image_info(event,image) 
  {
    var reader = new FileReader();
    reader.onload = function()
    {
      var output = document.getElementById(image);
      output.src = reader.result;
    }
    reader.readAsDataURL(event.target.files[0]);
    
    if(image == "output_info_banner_image"){
      var form_data = new FormData();
      var file_data_01 = $('#upload_info_banner').prop('files')[0];
      var match = ["image/gif", "image/png", "image/jpg",];
      var form_data = new FormData();
      form_data.append('upload_info_banner', file_data_01);
      form_data.append('id', <?php echo $_SESSION['system']->id?>);
      $.ajax({
          url: '<?php echo site_url('account-update-banner')?>', // gửi đến file upload.php 
          dataType: 'text',
          cache: false,
          contentType: false,
          processData: false,
          data: form_data,
          type: 'post',
          success: function (res) {
          }
      });
    }else if(image == "output_info_image"){
      var form_data = new FormData();
      var file_data_01 = $('#upload_info_avatar').prop('files')[0];
      var match = ["image/gif", "image/png", "image/jpg",];
      var form_data = new FormData();
      form_data.append('upload_info_avatar', file_data_01);
      form_data.append('id', <?php echo $_SESSION['system']->id?>);
      $.ajax({
          url: '<?php echo site_url('account-update-avatar')?>', // gửi đến file upload.php 
          dataType: 'text',
          cache: false,
          contentType: false,
          processData: false,
          data: form_data,
          type: 'post',
          success: function (res) {
          }
      });
    }

    

  }

</script>



<div id="info_banner" id="output_banner_image" class="row">
  <img class="info_background" id="output_info_banner_image" src="<?php echo site_url('assets/public/avatar/'.$_SESSION['system']->img2)?>" style="object-fit: cover;"/>
  <label for="upload_info_avatar">
    <img class="info_avatar" id="output_info_image" src="<?php echo site_url('assets/public/avatar/'.$_SESSION['system']->img)?>" style="object-fit: cover;"/>
  </label>
  <div class="info_user">
    <div class="info_name"><?php echo $_SESSION['system']->name?></div>
    <div class="info_totalpost"><?php echo $total_mags ?> Tin Đăng</div>
  </div>
  <input type="file" id="upload_info_banner" hidden onchange="preview_image_info(event,'output_info_banner_image')"/>
  <input type="file" id="upload_info_avatar" hidden onchange="preview_image_info(event,'output_info_image')"/>
  <label class="info_bgimg" for="upload_info_banner">Sửa Ảnh Bìa</label>
</div>






<!--<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">-->
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<style>

.info_form {
  width: 60%;
  margin: 60px auto;
  padding: 60px 120px 80px 120px;
  text-align: center;
  border: 1px solid black;
  border-radius:10px;
}
/* label {
  display: block;
  position: relative;
  margin: 40px 0px;
} */
.label-txt {
  position: absolute;
  top: -1.6em;
  padding: 10px;
  font-family: sans-serif;
  font-size: .8em;
  letter-spacing: 1px;
  color: rgb(120,120,120);
  transition: ease .3s;
}
.input {
  width: 100%;
  padding: 10px;
  background: transparent;
  border: none;
  outline: none;
}

.line-box {
  position: relative;
  width: 100%;
  height: 2px;
  background: #BCBCBC;
}

.line {
  position: absolute;
  width: 0%;
  height: 2px;
  top: 0px;
  left: 50%;
  transform: translateX(-50%);
  background: #8BC34A;
  transition: ease .6s;
}

.input:focus + .line-box .line {
  width: 100%;
}

.label-active {
  top: -3em;
}

/* button {
  display: inline-block;
  padding: 12px 24px;
  background: rgb(220,220,220);
  font-weight: bold;
  color: rgb(120,120,120);
  border: none;
  outline: none;
  border-radius: 3px;
  cursor: pointer;
  transition: ease .3s;
} */
/* 
button:hover {
  background: #8BC34A;
  color: #ffffff;
} */

</style>
<script>
  function preview_image(event,image) 
{
  var reader = new FileReader();
  reader.onload = function()
  {
    var output = document.getElementById(image);
    output.src = reader.result;
  }
  reader.readAsDataURL(event.target.files[0]);
}

$(document).ready(function(){
  $("#info_submit").click(function(){
    var newpassword = $( "#newpassword" ).val()
    var renewpassword = $( "#renewpassword" ).val()
    if(newpassword != renewpassword){
      alert("Mật khẩu và mật khẩu nhập lại không đúng")
    }else{
        //Lấy ra files
        var file_data = $('#info_img').prop('files')[0];
        //Xét kiểu file được upload
        var match = ["image/gif", "image/png", "image/jpg",];
        //kiểm tra kiểu file
        //khởi tạo đối tượng form data
        var form_data = new FormData();
        //thêm files vào trong form data
        form_data.append('img', file_data);
        form_data.append('phone', $( "#phone" ).val());
        form_data.append('id', $( "#id" ).val());
        form_data.append('name', $( "#name" ).val());
        form_data.append('newpassword', $( "#newpassword" ).val());
        //sử dụng ajax post
        $.ajax({
            url: '<?php echo site_url('update-info?token='.$infoLog->token)?>', // gửi đến file upload.php 
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (res) {
              alert(res);
              location.reload();
        }
      });
    }
  });
  $("#logout_submit").click(function(){
    window.location.href = "<?php echo site_url('dang-xuat')?>";
  });
  $("#review_post").click(function(){
    var file_data_2 = $('#mag_img_02').prop('files')[0];
    var file_data_1 = $('#mag_img_01').prop('files')[0];
    var form_data = new FormData();
    form_data.append('thumbnail', file_data_1);
    form_data.append('banner', file_data_2);
    form_data.append('title', $( "#post_title").val());
    form_data.append('description',$( "#post_description" ).val());
    form_data.append('content', $( "#post_content" ).val());
    form_data.append('type', $( "#post_type" ).val());
    form_data.append('hashtag', $( "#agatehashtag" ).val());
    $.ajax({
        url: '<?php echo site_url('info-review');?>', // gửi đến file upload.php 
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function (res) {
          if(res!=""){
            alert(res)
          }else{
            window.location.href = "<?php echo site_url('info_process')?>";
          }
        }
    });
    return false;
  });
});

</script>
<style>
body {font-family: Arial;}

.tab {
  overflow: hidden;
}

.tab button {
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
  background: none;
}

.tab button.active {
  border-bottom: 2px solid #30A0E0;
  color:#30A0E0;
}

.tabcontent {
  display: none;
  padding: 6px 12px;
  border-top: none;
}

.tablinks{
  width: 20%;
}

</style>
<div class="tab">
  <button class="tablinks" onclick="openCity(event, 'user')" id="<?php echo $this->input->get('tab')!="config"? "defaultOpen" :""?>"><img src="<?php echo site_url('assets/public/avatar/usericon.png')?>" width="20px" style="margin-right:10px"/>Trang Cá Nhân</button>
  <button class="tablinks" onclick="openCity(event, 'post')"><img src="<?php echo site_url('assets/public/avatar/create.png')?>" width="20px" style="margin-right:10px"/>Tạo Bài Viết</button>
  <button class="tablinks" onclick="openCity(event, 'postmanager')"><img src="<?php echo site_url('assets/public/avatar/postlist.png')?>" width="20px" style="margin-right:10px"/>Quản Lí Bài Viết</button>
  <button class="tablinks" onclick="openCity(event, 'postsave')"><img src="<?php echo site_url('assets/public/avatar/savepost.png')?>" width="20px" style="margin-right:10px"/>Bài Viết Đã Lưu</button>
  <button class="tablinks" onclick="openCity(event, 'infoconfig')" id="<?php echo $this->input->get('tab')=="config"? "defaultOpen" :""?>"><img src="<?php echo site_url('assets/public/avatar/config.png')?>" width="20px" style="margin-right:10px"/>Thiết Lập</button>
</div>

<div id="user" class="tabcontent">
  <?php include "inc_user.php" ?>

</div>

<div id="post" class="tabcontent">
  <?php include "inc_post.php" ?>

</div>

<div id="postmanager" class="tabcontent">
  <?php include "inc_postmanager.php" ?>
</div>

<div id="postsave" class="tabcontent">
  <?php include "inc_postsave.php" ?>
</div>

<div id="infoconfig" class="tabcontent">
  <?php include "inc_config.php" ?>
</div>

<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>


<!-- Modal -->
<?php echo form_open_multipart(site_url('update-info'),array('autocomplete'=>"off",'id'=>"fileUploadForm"));?>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Chỉnh sửa thông tin</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" name="id" value="<?php echo (!empty($_SESSION['system']->id)) ? $_SESSION['system']->id: ""?>">

        <div class="form-group">
          <label for="exampleInputEmail1">Họ và Tên</label>
          <input type="text" class="form-control" id="full_name" name="full_name" placeholder="Họ và Tên" value="<?php if(!empty($info->full_name)) echo $info->full_name;?>">
        </div>
        <div class="form-row">
          <div class="col">
            <label for="exampleInputPassword1">Điện thoại</label>
            <input type="text" class="form-control" id="phone" name="phone" placeholder="Điện thoại" value="<?php if(!empty($info->phone)) echo $info->phone;?>">
          </div>
          <div class="col">
            <label for="exampleInputPassword1">Email</label>
            <input type="text" class="form-control" placeholder="Email" value="<?php if(!empty($info->email)) echo $info->email;?>" disabled>
          </div>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Địa chỉ thường trú</label>
          <input type="text" class="form-control"  name="address"  id="address" placeholder="Địa chỉ thường trú" value="<?php if(!empty($info->address)) echo $info->address;?>">
        </div>
        <div class="form-row">
          <div class="col">
            <label for="exampleInputPassword1">Vai trò</label>
            <select name="role" class="custom-select">
              <option value="1" <?php if($info->role==1) echo "selected";?>>Phụ Huynh</option>
              <option value="2" <?php if($info->role==2) echo "selected";?>>Học Sinh</option>
            </select>          
          </div>
          <div class="col">
            <label for="exampleInputPassword1">Giới Tính</label>
            <select name="sex" class="custom-select">
              <option value="1" <?php if($info->sex==1) echo "selected";?>>Nam</option>
              <option value="2" <?php if($info->sex==2) echo "selected";?>>Nữ</option>
            </select>            
          </div>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Giới thiệu bản thân</label>
          <input type="text" class="form-control" id="description" name="description" placeholder="Giới thiệu bản thân" value="<?php if(!empty($info->description)) echo $info->description;?>">
        </div>
      

      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary save_info">Lưu</button>
      </div>
    </div>
  </div>
</div>
</form>

<!-- Modal -->
<div class="modal fade" id="reviewmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog reviewp_post_modal" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title review_post_title" id="exampleModalLongTitle">Xem trước bài viết</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
    </div>
  </div>
</div>
