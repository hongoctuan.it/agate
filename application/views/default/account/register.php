<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="ico" href="<?php echo site_url('statics/default/img/logo_small.png')?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="vi">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="description" content="Agate">
    <meta name="msapplication-tap-highlight" content="no">
    
    <title>Agate</title>

    <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Inter:wght@200;300&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Inter:wght@600&display=swap" rel="stylesheet">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link href="<?php echo site_url("statics/cms/style.css")?>" rel="stylesheet" id="bootstrap-css">

<!------ Include the above in your HEAD tag ---------->

<div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->
    <!-- Icon -->
    <div class="fadeIn first">
    <a href="<?php echo site_url()?>"><img src="<?php echo site_url("assets/public/avatar/AGATE_footer.png")?>" id="icon" alt="User Icon" /></a>
    </div>

    <!-- Login Form -->
	<?php if(isset($_SESSION['system_msg'])){
				echo $_SESSION['system_msg'];unset($_SESSION['system_msg']);
	}?>
	<form class="login100-form validate-form p-b-33 p-t-5" action="<?php echo site_url('dang-ky')?>" method="post">
      <input type="email" id="email" class="fadeIn second" name="email" placeholder="Email">
      <input type="text" id="name" class="fadeIn second" name="name" placeholder="Họ và Tên">
      <input type="text" id="phone" class="fadeIn second" name="phone" placeholder="Số điện thoại">
      <input type="password" id="password" class="fadeIn third" name="password" placeholder="Mật khẩu">
      <input type="password" id="repassword" class="fadeIn third" name="repassword" placeholder="Mật khẩu nhập lại">
      <input type="submit" class="fadeIn fourth" value="Đăng ký">
    </form>

    <!-- Remind Passowrd -->
    <div id="formFooter">
      <a class="underlineHover" href="<?php echo site_url('dang-nhap');?>" style="text-decoration: none !important">Đăng nhập</a> | 
      <a class="underlineHover" href="<?php echo site_url();?>" style="text-decoration: none !important">Quay về trang chủ</a>
    </div>
  </div>
</div>