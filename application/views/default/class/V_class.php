<section>
    <?php include 'inc_banner.php';?>
</section>

<section>
    <?php include 'inc_benefit.php';?>
</section>

<section>
    <?php include 'inc_comment.php';?>
</section>

<section>
    <?php include 'inc_basic.php';?>
</section>
<section>
    <?php include 'inc_baseline.php';?>
</section>
<section>
    <?php include 'inc_staff.php';?>
</section>

<section>
    <?php include 'inc_course.php';?>
</section>
<section>
    <?php include 'inc_regisinfoclass.php';?>
</section>
<section>
    <?php include 'inc_class.php';?>
</section>
<section>
    <?php include 'inc_tag.php';?>
</section>
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body row">
        <div class="col-lg-3 col-md-3">
            <div class="lesson">
                <div class="lesson-title">BUỔI</div>
                <?php foreach($schedules as $item):?>
                  <div class="lesson-content-schedules"><?php echo $item->session?></div>
                <?php endforeach;?>
            </div>
        </div>
        <div class="col-lg-3 col-md-3">
          <div class="time">
              <div class="time-title">THỜI LƯỢNG</div>
              <?php foreach($schedules as $item):?>
                  <div class="lesson-content-time"><?php echo $item->time?></div>
              <?php endforeach;?>
          </div>
        </div>
        <div class="col-lg-6 col-md-6">
          <div class="content">
            <div class="content-title">NỘI DUNG</div>
            <?php foreach($schedules as $item):?>
                  <div class="lesson-content-des"><?php echo $item->description?></div>
              <?php endforeach;?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>