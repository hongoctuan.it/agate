<script>
    $(document).ready(function(){
        var element = document.getElementById("baseline");
        if(screen.width < 969){
            element.style.backgroundImage = "url('<?php echo site_url('assets/public/avatar/'.$class[42]->value)?>')";
        }else{
            element.style.backgroundImage = "url('<?php echo site_url('assets/public/avatar/'.$class[27]->value)?>')";
        }
    });
</script>
<div id="baseline" class="col-lg-12 col-md-12 col-12 row" style="margin:0px" data-toggle="modal" data-target="#exampleModalCenter">
    <div class="col-lg-4 col-md-12 col-12 baseline_left_01">
        <div class="baseline_item">
            <span class="baseline_item_description"><?php echo $class[24]->value?></span>
        </div>
    </div>
    <div class="col-lg-4 col-md-12 col-12  baseline_left_02">
        <div class="baseline_item">
            <span class="baseline_item_description"><?php echo $class[25]->value?></span>
        </div>
    </div>
    <div class="col-lg-4 col-md-12 col-12  baseline_left_03">
        <div class="baseline_item">
            <span class="baseline_item_description"><?php echo $class[26]->value?></span>
        </div>
    </div>
    
</div>