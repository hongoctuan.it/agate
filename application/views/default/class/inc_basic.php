<style>
    .basic_item_01{
        background-image: url("<?php echo site_url('assets/public/avatar/basic_bg_01.png')?>");
        background-repeat: no-repeat; /* Do not repeat the image */
        background-size: cover; 
        margin-top: 10px;
        margin-bottom: 10px;
        height: 311px;
    }
    .basic_item_02{
        background-image: url("<?php echo site_url('assets/public/avatar/basic_bg_02.png')?>");
        background-repeat: no-repeat; /* Do not repeat the image */
        background-size: cover; 
        margin-top: 10px;
        margin-bottom: 10px;
        height: 311px;

    }
    .basic_item_03{
        background-image: url("<?php echo site_url('assets/public/avatar/basic_bg_01.png')?>");
        background-repeat: no-repeat; /* Do not repeat the image */
        background-size: cover; 
        margin-top: 10px;
        margin-bottom: 10px;
        height: 311px;

    }
    .basic_item_04{
        background-image: url("<?php echo site_url('assets/public/avatar/basic_bg_02.png')?>");
        background-repeat: no-repeat; /* Do not repeat the image */
        background-size: cover; 
        margin-top: 10px;
        margin-bottom: 10px;
        height: 311px;

    }
</style>
<div id="basic" class="row">
    
    <div style="text-align:left" class="col-lg-4 col-md-12">
        <div class="session_name col-lg-12" style="text-align:left">AGATE</div>
        <div class="basic_title col-lg-12"><?php echo $class[40]->value ?></div>
        <div class="class-description col-lg-12"><?php echo $class[41]->value ?></div>
        <div style="text-align: left; padding-top:20px"><a href="<?php echo site_url('chuong-trinh#course')?>"><button class="btn banner_order" type="button" style="background-color: #FFB308 !important">Đăng Ký Ngay</button></a></div>

    </div>
    <div style="text-align:left;padding:0px; margin:0 auto" class="col-lg-8 col-md-12 row">
        <div class="col-lg-6 col-md-6" style="text-align:justify;padding:0px">
            <div class="col-lg-12 col-md-12">
                <div class="basic_item_01">
                    <div class="basic_item_title_01"><?php echo $class[12]->value?></div>
                    <div class="basic_description_01"><?php echo $class[15]->value?></div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12">
                <div class="basic_item_02">
                    <div class="basic_item_title_02"><?php echo $class[14]->value?></div>
                    <div class="basic_description_02"><?php echo $class[17]->value?></div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6" style="text-align:justify;padding:0px">
            <div class="col-lg-12 col-md-12">
                <div class="basic_item_03">
                    <div class="basic_item_title_01"><?php echo $class[13]->value?></div>
                    <div class="basic_description_01"><?php echo $class[16]->value?></div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12">
                <div class="basic_item_04">
                    <div class="basic_item_title_02"><?php echo $class[29]->value?></div>
                    <div class="basic_description_02"><?php echo $class[30]->value?></div>
                </div>
            </div>
        </div>
    </div>
</div>