<script>
    $(document).ready(function(){

        if(screen.width < 560){
            $("#benefit_image_01").attr("src","<?php echo site_url('assets/public/avatar/class_bg01_hi_mb.png')?>");
            $("#benefit_image_02").attr("src","<?php echo site_url('assets/public/avatar/class_bg02_hi_mb.png')?>");
            $("#benefit_image_03").attr("src","<?php echo site_url('assets/public/avatar/class_bg03_hi_mb.png')?>");
            $("#benefit_image_04").attr("src","<?php echo site_url('assets/public/avatar/class_bg04_hi_mb.png')?>");
        }
        else if(screen.width < 969){
            $("#benefit_image_01").attr("src","<?php echo site_url('assets/public/avatar/class_bg01_hi_ip.png')?>");
            $("#benefit_image_02").attr("src","<?php echo site_url('assets/public/avatar/class_bg02_hi_ip.png')?>");
            $("#benefit_image_03").attr("src","<?php echo site_url('assets/public/avatar/class_bg03_hi_ip.png')?>");
            $("#benefit_image_04").attr("src","<?php echo site_url('assets/public/avatar/class_bg04_hi_ip.png')?>");
        }
        else{
            $("#benefit_image_01").attr("src","<?php echo site_url('assets/public/avatar/chuongtrinh_01.png')?>");
            $("#benefit_image_02").attr("src","<?php echo site_url('assets/public/avatar/chuongtrinh_02.png')?>");
            $("#benefit_image_03").attr("src","<?php echo site_url('assets/public/avatar/chuongtrinh_03.png')?>");
            $("#benefit_image_04").attr("src","<?php echo site_url('assets/public/avatar/chuongtrinh_04.png')?>");
        }
    });
</script>
<div id="benefit" class="row">
    <div class="session_name col-lg-12 col-md-12 col-12">
        YOUniverse
    </div>
    <div class="session_title benifit_title col-lg-12 col-md-12 col-12">
        <?php echo $class[33]->value?>
    </div>
    <div class="col-lg-6 col-md-6 col-12 benefitpaddingleft">
        <div class="benefit_item">
        <div class="benefit_image"><img id="benefit_image_01" src="assets/public/avatar/chuongtrinh_01.png" /></div>
            <div class="benefit_title_left"><?php echo $class[2]->value?></div>
            <div class="benefit_description_left"><?php echo $class[34]->value?></div>
            
        </div>
    </div>
    <div class="col-lg-6 col-md-6 benefitpaddingright">
        <div class="benefit_item">
            <div class="benefit_image"><img id="benefit_image_02" src="<? echo site_url('assets/public/avatar/chuongtrinh_02.png')?>" /></div>
            <div class="benefit_title_right"><?php echo $class[3]->value?></div>
            <div class="benefit_description_right"><?php echo $class[35]->value?></div>

        </div>
    </div>
    <div class="col-lg-6 col-md-6 benefitpaddingleft">
        <div class="benefit_item">
            <div class="benefit_image"><img id="benefit_image_03" src="<? echo site_url('assets/public/avatar/chuongtrinh_03.png')?>"/></div>
            <div class="benefit_title_left"><?php echo $class[4]->value?></div>
            <div class="benefit_description_left"><?php echo $class[36]->value?></div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 benefitpaddingright">
        <div class="benefit_item ">
            <div class="benefit_image"><img id="benefit_image_04" src="<? echo site_url('assets/public/avatar/chuongtrinh_04.png')?>"/></div>
            <div class="benefit_title_right"><?php echo $class[5]->value?></div>
            <div class="benefit_description_right"><?php echo $class[37]->value?></div>

        </div>
    </div>
</div>