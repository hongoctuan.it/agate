<div id="comment">
    
    <div class="col-lg-12 col-md-12 col-12 row" style="margin:0px">
        <div class="class-content col-md-6  col-lg-7 col-12">
            <div class="session_name col-lg-12">Agate Astronions</div>
            <div class="comment_title"><?php echo $class[38]->value ?></div>
        </div>
        <div class="class-description col-md-6  col-lg-4 col-12">
            <div class="class-description"><?php echo $class[39]->value ?></div>
        </div>
        

        <div class="row-fluid">
            <div>
                <div class="cover-container">
                    <!-- START PANEL -->
                    <div class="cover-item" style="background-image: url('<?php echo site_url('assets/public/avatar/comment_01.png')?>')">
                        <div class="comment-item_description comment_block_01" ><?php echo $comment_01->description ?></div>
                        <div class="comment-item-user row">
                            <div class="comment-item-user col-md-4  col-lg-4 col-3 comment_user_01">
                                <img src="<?php echo site_url('assets/public/avatar/'.$comment_01->img)?>" class="comment-item-user-icon"/>
                            </div> 
                            <div class="col-md-8  col-lg-8 col-9 comment_user_des_01">
                                <div class="comment-item-user-description" ><?php echo $comment_01->user_name ?></div>
                                <div class="comment-item-user-position">@instagram <img src="<?php echo site_url('assets/public/avatar/commentrating.png'); ?>" /></div>
                            </div> 
                        </div>
                    </div>
                    <div class="cover-item" style="background-image: url('<?php echo site_url('assets/public/avatar/comment_02.png')?>')">
                        <div class="comment-item_description" style="padding-left:40px"><?php echo $comment_02->description ?></div>
                        <div class="comment-item-user row" style="padding-left:40px">
                            <div class="comment-item-user col-md-4  col-lg-3 col-3">
                                <img src="<?php echo site_url('assets/public/avatar/'.$comment_02->img)?>" class="comment-item-user-icon"/>
                            </div> 
                            <div class="col-md-8  col-lg-9 col-9 comment_user_des_02">
                                <div class="comment-item-user-description"><?php echo $comment_02->user_name ?></div>
                                <div class="comment-item-user-position">@instagram <img src="<?php echo site_url('assets/public/avatar/commentrating.png'); ?>" /></div>
                            </div> 
                        </div>
                    </div>
                    
                    <div class="cover-item" style="background-image: url('<?php echo site_url('assets/public/avatar/comment_03.png')?>')">
                        <div class="comment-item_description comment_block_03" style="padding-left:40px"><?php echo $comment_03->description ?></div>
                        <div class="comment-item-user row" style="padding-left:40px">
                            <div class="comment-item-user col-md-4  col-lg-3 col-3">
                                <img src="<?php echo site_url('assets/public/avatar/'.$comment_03->img)?>" class="comment-item-user-icon"/>
                            </div> 
                            <div class="col-md-8  col-lg-9 col-9 comment_user_des_02">
                                <div class="comment-item-user-description"><?php echo $comment_03->user_name ?></div>
                                <div class="comment-item-user-position">@instagram <img src="<?php echo site_url('assets/public/avatar/commentrating.png'); ?>" /></div>
                            </div> 
                        </div>
                    </div>
                    <!-- END PANEL -->
                </div>
            </div>
        </div>
    </div>
    
</div>