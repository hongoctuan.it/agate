<script>
    $(document).ready(function(){
        var element = document.getElementById("course_form");
        var course_description_pc = document.getElementById("course_description_pc");
        var course_description_mb = document.getElementById("course_description_mb");
        if(screen.width < 569){
            course_description_pc.style.display = "none";
            element.style.backgroundImage = "url('<?php echo site_url('assets/public/avatar/register_bg_mb.png')?>')";
        }else{
            course_description_mb.style.display = "none";
            element.style.backgroundImage = "url('<?php echo site_url('assets/public/avatar/register_bg.png')?>')";
        }
    });
    function tracking() {
        $.ajax({
          url: '<?php echo site_url('tracking?act='.$this->uri->segment(1))?>', // gửi đến file upload.php 
          dataType: 'text',
          type: 'post',
      });
    }
</script>
<div id="course" class="row">
    <div class="col-lg-2 course_left displaynonemobile">
    </div>
    <div class="col-lg-8 course_left">

        <!-- <div class="session_name col-lg-12">Nhận tư vấn về khóa học</div> -->
        <div class="session_title col-lg-12"><?php echo $class[21]->value?></div>
        <div class="course_item col-lg-12">
            <div id="course_description_pc">
                <?php echo $class[23]->value?>
            </div>
            <div id="course_description_mb">
                <?php echo $class[22]->value?>
            </div>
            <!-- <span id="course_description" class=" col-lg-12">

            </span> -->
        </div>
    </div>
    <div class="col-lg-2 col-md-0 course_right">
    </div>
    <div class="col-lg-3 col-md-2 course_right">
    </div>
    <div class=" col-lg-6 col-md-8">
        <form class="row  course_form" id="course_form">
            <div class="form-group col-lg-12 course_right_input">
                <label class="class_name">HỌ TÊN</label>

                <input type="text" class="form-control" id="course_name" aria-describedby="emailHelp" placeholder="Họ tên">
            </div>
            <div class="form-group col-lg-12 course_right_input">
                <label class="class_name">SỐ ĐIỆN THOẠI</label>

                <input type="text" class="form-control" id="course_phone" aria-describedby="emailHelp" placeholder="Số điện thoại">
            </div>
            <div class="form-group col-lg-12 course_right_input">
                <label class="class_name">EMAIL CỦA BẠN</label>
                <input type="email" class="form-control" id="course_email" aria-describedby="emailHelp" placeholder="Email của bạn">
            </div>
            <div class="form-group col-lg-12 course_right_input">
                <label class="class_name">PHỤ HUYNH/HỌC SINH</label>
                <select class="form-control" id="course_type">
                    <option value='1'>Học Sinh</option>
                    <option value="2">Phụ Huynh</option>
                </select>            
            </div>
            <div class="course_right_description col-lg-12">
                <button class="btn nav_order form-group col-lg-12 course_btn" style="margin: 0px;background-color:#FFB308 !important" type="button" onclick="regiscourse()">Đăng kí tư vấn</button>
            </div>
            
                
            
        </form>
    </div>
    <div class="col-lg-3 col-md-2 course_left course_left_padding">
    </div>
</div>
<script>
    function regiscourse(){
            if($("#course_email").val()!="" || $("#course_phone").val()!="" || $("#course_name").val() !=""){
                var _data = {'email':$("#course_email").val(),"type":2,'phone':$("#course_phone").val(),"type":3,"name":$("#course_name").val(),"course_type":$("#course_type :selected").text()}
                $.ajax({
                    url: '<?php echo site_url('dang-ky-thong-tin')?>',
                    dataType: 'text',
                    data: _data,
                    type: 'post',
                    success: function (res) {
                        $("#course_email").val("")
                        $("#course_phone").val("")
                        $("#course_name").val("")                        
                        // alert(res);
                        alert("Đăng ký thành công!")
                    }
                });
            }else{
                alert('Vui lòng điền đầy đủ thông tin: email, phone, họ tên!');
      }
    }
    function regisinfoclass(){
            if($("#regis_infoclass_email").val()!=""){
                var _data = {'email':$("#regis_infoclass_email").val(),"type":2}
                $.ajax({
                    url: '<?php echo site_url('dang-ky-thong-tin')?>',
                    dataType: 'text',
                    data: _data,
                    type: 'post',
                    // success: function (res) {
                    //     $("#regis_infoclass_email").val("")
                    //     alert(res);
                    // }
                });
                alert("Đăng ký thành công!")

            }else{
                alert('Vui lòng nhập địa chỉ email');
      }
    }
</script>