<script>
    $(document).ready(function(){
        if(screen.width < 769){
            $("#staff_img_01").attr("src","<?php echo site_url("assets/public/avatar/".$home[55]->value)?>");
            $("#staff_img_02").attr("src","<?php echo site_url("assets/public/avatar/".$home[57]->value)?>");
            $("#staff_img_03").attr("src","<?php echo site_url("assets/public/avatar/".$home[56]->value)?>");
        }else{
            $("#staff_img_01").attr("src","<?php echo site_url("assets/public/avatar/".$home[52]->value)?>");
            $("#staff_img_02").attr("src","<?php echo site_url("assets/public/avatar/".$home[54]->value)?>");
            $("#staff_img_03").attr("src","<?php echo site_url("assets/public/avatar/".$home[53]->value)?>");
        }
    });
</script>
<div id="staff" class="col-lg-12 col-md-12 col-12 row">
    <div class="session_name col-lg-12"  style="text-align:center; margin-top:50px">AGATE</div>
    <div class="staff_title col-lg-12">Đội ngũ chuyên gia</div>
    <div class="staff-item-left col-lg-12 col-md-12 col-12 row">
        <div class="col-lg-4 col-md-4 col-6 staff_item_img_01">
            <img id="staff_img_01" src="assets/public/avatar/staff1.png"/>
        </div>
        <div class="col-lg-8 col-md-8 col-6 staff-item-content-left">
                <div class="col-lg-6 col-12 clearpadding">
                    <div class="staff_item_name"><?php echo $staff_01->full_name?></div>
                    <div class="staff_item_position"><?php echo $staff_01->position?></div>
                </div>
                <div class="col-lg-9 clearpadding" >
                    <div class="staff_item_position_description"><?php echo $staff_01->position_description?></div>
                </div>
            <div class="col-lg-12 staff_left_line_01"></div>
            <div class="col-lg-12">
                <div class="staff_item_description staff_item_left_description_01"><?php echo $staff_01->description?></div>
            </div>
        </div>
        
        
    </div>
    <div class="staff-item-right clearpadding col-lg-12 col-md-12 row">
        <div class="col-lg-8 col-md-8 col-6 clearpadding staff-item-content-right" >
            <div class="col-lg-12 col-12 staff_item_right_margin clearpadding" >
                <div class="col-lg-9 col-12" style="text-align:right; padding-right:32px">
                    <div class="staff_item_name"><?php echo $staff_02->full_name?></div>
                    <div class="staff_item_position"><?php echo $staff_02->position?></div>
                </div>
                <div class="col-lg-9">
                    <div class="staff_item_position_description "><?php echo $staff_02->position_description?></div>
                </div>
            </div>
            <div class="col-lg-12 staff_left_line_02"></div>
            <div class="col-lg-12">
                <div class="staff_item_description staff_item_right_description_02"><?php echo $staff_02->description?></div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-6 staff_item_img_02 clearpadding">
            <img id="staff_img_02" src="assets/public/avatar/staff2.png"/>
        </div>
    </div>
    <div class="staff-item-left col-lg-12 col-md-12 col-12 row">
        <div class="col-lg-4 col-md-4 col-6 staff_item_img_01">
            <img id="staff_img_03" src="assets/public/avatar/staff1.png"/>
        </div>
        <div class="col-lg-8 col-md-8 col-6 staff-item-content-left">
                <div class="col-lg-6 col-12 clearpadding">
                    <div class="staff_item_name"><?php echo $staff_03->full_name?></div>
                    <div class="staff_item_position"><?php echo $staff_03->position?></div>
                </div>
                <div class="col-lg-9 clearpadding" >
                    <div class="staff_item_position_description"><?php echo $staff_03->position_description?></div>
                </div>
            <div class="col-lg-12 staff_left_line_03"></div>
            <div class="col-lg-12">
                <div class="staff_item_description staff_item_left_description_03"><?php echo $staff_03->description?></div>
            </div>
        </div>
    </div>
</div>