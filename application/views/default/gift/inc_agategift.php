<?php if(!empty($gifts)):?>
    <?php foreach($gifts as $item):?>
        <div id="gift" class="row">
            <div class="gift col-lg-5 col-md-6">
                <img src="<?php echo site_url('assets/public/avatar/').$item->img_01 ?>" width="100%"/>
            </div>
            <div class="col-lg-7 col-md-6">
                <div class="row">
                    <div class="col-lg-12 gift_item">
                        <div class="gift_item_title"><?php echo $item->type ?></div>
                        <div class="agategift_item_title"><?php echo $item->title ?></div>
                        <div class="gift_item_description"><?php echo $item->description ?></div>
                        <div style="margin-top:25px;"><a class="gift_item_url" href="<?php echo site_url('agate-gift-detail/'.$item->slug)?>">Xem thêm ></a></div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach;?>
<?php endif;?>
