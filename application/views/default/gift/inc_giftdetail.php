<script>
    function changeImage(element) {
var main_prodcut_image = document.getElementById('main_product_image');
main_prodcut_image.src = element.src;
}
</script>
<div id="gift" class="row">
    <div class="gift col-lg-5 col-md-5 col-12">
        <div>
            <div class="main_image"> <img src="<?php echo site_url('assets/public/avatar/'.$gift->img_01)?>" id="main_product_image" width="350"> </div>
            <div class="thumbnail_images">
                <ul id="thumbnail">
                    <li><img onclick="changeImage(this)" src="<?php echo site_url('assets/public/avatar/'.$gift->img_01)?>" width="70"></li>
                    <li><img onclick="changeImage(this)" src="<?php echo site_url('assets/public/avatar/'.$gift->img_02)?>" width="70"></li>
                    <li><img onclick="changeImage(this)" src="<?php echo site_url('assets/public/avatar/'.$gift->img_03)?>" width="70"></li>
                    <li><img onclick="changeImage(this)" src="<?php echo site_url('assets/public/avatar/'.$gift->img_04)?>" width="70"></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="gift col-lg-7">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 gift_item">
                <div class="gift_item_type"><?php echo $gift->type ?></div>
                <div class="gift_item_title"><?php echo $gift->title ?></div>
                <div class="gift_item_description"><?php echo $gift->description ?></div>
                <div style="text-align: left; margin-top:20px">
                    <a href="<?php echo $gift->shopee_link ?>">
                        <button class="btn shopeebtn" type="submit">Đặt hàng qua Shopee</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>