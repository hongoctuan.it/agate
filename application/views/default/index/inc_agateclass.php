		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo base_url(); ?>statics/default/assets/css/carousel.css">
<div id="agateclass">
    <div class="agateclass_image"> <a href="https://www.instagram.com/agate.journey/"><img src="<?php echo site_url('assets/public/avatar/agateclass.png'); ?>"/></a></div>
    

    <!-- Top content -->
        <?php if(!empty($classimg)):?>
            <div class="top-content">
                <div class="container-fluid">
                    <div id="carousel-example" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner row w-100 mx-auto" role="listbox" style="width:80% !important; padding:0px">
                            <?php $count = 0; foreach($classimg as $item):?>
                                <?php if($count==0):?>
                                    <div class="carousel-item col-12 col-sm-6 col-md-6 col-lg-4 active agateclass-slider-item">
                                        <div class="agateclass-slider-item-content">
                                        <a href="<?php echo $item->description ?>"><img src="<?php echo site_url('assets/public/avatar/'.$item->img); ?>" class="img-fluid mx-auto d-block" alt="img1"></a>
                                        </div>
                                    </div>
                                <?php else:?>
                                    <div class="carousel-item col-12 col-sm-6 col-md-6 col-lg-4 agateclass-slider-item">
                                        <div class="agateclass-slider-item-content">
                                            <a href="<?php echo $item->description ?>"><img src="<?php echo site_url('assets/public/avatar/'.$item->img); ?>" class="img-fluid mx-auto d-block" alt="img1"></a>
                                        </div>
                                    </div>
                                <?php endif;?>
                            <?php $count++; endforeach;?>
                        </div>
                        <a class="carousel-control-prev" href="#carousel-example" role="button" data-slide="prev">
                            <img src="assets/public/avatar/left.png"/>
                        </a>
                        <a class="carousel-control-next" href="#carousel-example" role="button" data-slide="next">
                            <img src="assets/public/avatar/right.png"/>
                        </a>
                    </div>
                </div>
            </div>
        <?php endif; ?>


         <!-- Javascript -->
		<script src="<?php echo base_url(); ?>statics/default/assets/js/jquery-migrate-3.0.0.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="<?php echo base_url(); ?>statics/default/assets/js/wow.min.js"></script>
        <script src="<?php echo base_url(); ?>statics/default/assets/js/scripts.js"></script>
    

</div>
