<style>
    .gift_image{
        background-image: url("<?php echo site_url('assets/public/avatar/'.$home[6]->value)?>");
        background-repeat: no-repeat; /* Do not repeat the image */
        background-size: cover; /* Resize the background image to cover the entire container */
        height: 584px;
        padding-top: 20px;
    }
</style>

<div id="gift" class="col-lg-12 col-md-12 col-12 row">

    <div class="gift_image col-lg-5">
    </div>
    <div class="gift_content col-lg-7">
        <div class="row">
            <div class="col-lg-12 gift_item clearpadding">
                <div class="section_name col-lg-12">AGATE GIFT</div>
                <div class="agategift_item_title"><?php echo $gift->title?></div>
                <div class="agategift_item_description"><?php echo $gift->description?></div>
                <!-- <div style="margin-top:15px"><a class="gift_item_url" href="<?php echo $gift->slug?>">Xem thêm ></a></div> -->
            </div>
        </div>
    </div>
</div>