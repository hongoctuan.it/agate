<div id="agatemag">
    <div class="section_name col-lg-12"  style="text-align:center">AGATE MAG</div>
    <div class="agatemag_title">Chuyên trang thông tin</div>
    <div class="row">
        <div class="col-lg-6 agatemag-item">
            <div class="agatemag_bg"></div>
            <img src="assets/public/avatar/<?php echo $mag_01->img?>" />
            <div class="agate-hashtag col-lg-12">
                <?php if(!empty($mag_02->hashtag)):?>
                    <?php foreach($mag_02->hashtag as $item):?>
                        <a href="#<?php echo site_url('hashtag/'.$item->id)?>">#<?php echo $item->title?></a>
                    <?php endforeach;?>
                <?php endif;?>
            </div>
            <div class="agatemag-item-title">
                <?php echo $mag_01->title?>
            </div>
            <div class="agatemag-item-description">
                <?php echo $mag_01->description?>
            </div>
            <div class="agate-url col-lg-12" style="margin-top:-370px">
                <a href="#">Đọc Tiếp ></a>
            </div>
        </div>
        <div class="col-lg-6 agatemag-item">
            <div class="agatemag_bg"></div>
            <img src="assets/public/avatar/<?php echo $mag_02->img?>" />
            <div class="agate-hashtag col-lg-12">
                <?php if(!empty($mag_01->hashtag)):?>
                    <?php foreach($mag_01->hashtag as $item):?>
                        <a href="#<?php echo site_url('hashtag/'.$item->id)?>">#<?php echo $item->title?></a>
                    <?php endforeach;?>
                <?php endif;?>
                
            </div>
            <div class="agatemag-item-title">
                <?php echo $mag_02->title?>
            </div>
            <div class="agatemag-item-description">
                <?php echo $mag_02->description?>
            </div>
            <div class="agate-url col-lg-12" style="margin-top:-370px">
                <a href="#">Đọc Tiếp ></a>
            </div>
        </div>
    </div>
    
</div>