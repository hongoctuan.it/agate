<div id="banner" class="row">
    <div class="col-lg-6 col-md-12 col-12 banner_item">
        <div class="section_name">AGATE CLASS</div>
        <div class="banner_title"><?php echo $home[0]->value?></div>
        <div class="banner_description"><?php echo $home[1]->value?></div>
        <div class="banner_order_btn">
            <a href="<?php echo site_url("chuong-trinh#course")?>" class="banner_order" style="background: #30A0E0;">Đăng Ký Ngay</a>
        </div>   
    </div>
    <img class="banner_img col-lg-6" src="<?php echo site_url('assets/public/avatar/'.$home[7]->value)?>" width="100%"/>
    <div class="col-lg-12 col-md-12 row banner-menu">
        <div class="col-lg-3 col-md-6 col-6 banner_item_01">
            <p class="banner_item_content_01"><?php echo $home[2]->value?></p>
        </div>
        <div class="col-lg-3 col-md-6 col-6 banner_item_02">
            <p class="banner_item_content_02"><?php echo $home[3]->value?></p>
        </div>
        <div class="col-lg-3 col-md-6 col-6 banner_item_03">
            <p class="banner_item_content_03"><?php echo $home[4]->value?></p>
        </div>
        <div class="col-lg-3 col-md-6 col-6 banner_item_04">
            <p class="banner_item_content_04"><?php echo $home[5]->value?></p>
        </div>
    </div>
</div>