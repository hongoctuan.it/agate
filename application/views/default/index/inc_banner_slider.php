<script src="<?php echo base_url(); ?>statics/default/assets/js/jssor.slider.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_SlideshowTransitions = [
              {$Duration:1200,x:-0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2}
            ];

            var jssor_1_options = {
              $AutoPlay: 1,
              $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $ThumbnailNavigatorOptions: {
                $Class: $JssorThumbnailNavigator$,
                $Orientation: 2,
                $NoDrag: true
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 1440;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
    <style>
        /* jssor slider loading skin spin css */
        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-009-spin {
            from {
                transform: rotate(0deg);
            }

            to {
                transform: rotate(360deg);
            }
        }


        .jssora061 {display:block;position:absolute;cursor:pointer;}
        .jssora061 .a {fill:none;stroke:#fff;stroke-width:360;stroke-linecap:round;}
        .jssora061:hover {opacity:.8;}
        .jssora061.jssora061dn {opacity:.5;}
        .jssora061.jssora061ds {opacity:.3;pointer-events:none;}
    </style>
<div id="banner">
    <div class="container col-lg-12 col-md-12 col-12">
        <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1440px;height:700px;overflow:hidden;visibility:hidden;">
            <!-- Loading Screen -->
            <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:1440px;height:700px;text-align:center;background-color:rgba(0,0,0,0.7);">
                <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="<?php echo site_url('assets/public/avatar/spin.png')?>" />
            </div>
            <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1440px;height:700px;overflow:hidden;">
                <div>
                    <div class="banner_content" style="position: absolute; text-align: left;width:50%; padding-top:300px;">
                        <div class="section_name com_name">AGATE</div>     
                        <div class="banner_title"><?php echo $home[0]->value?></div>
                        <div class="banner_description"><?php echo $home[1]->value?></div>
                        <div style="text-align: left;padding-top:40px">
                            <a href="<?php echo site_url("chuong-trinh#course")?>" class="banner_order banner_orderbtn" style="background: #30A0E0;">Đăng Ký Ngay</a>
                        </div>   
                    </div>
                    <img src="assets/public/avatar/banner_01.png" alt="Los Angeles" style="width:95%;">
                </div>
                <!-- <div>
                    <img data-u="image" src="../img/gallery/980x380/043.jpg" />
                    <div data-u="thumb">Slide Description 002</div>
                </div> -->
                
            </div>
            <!-- Thumbnail Navigator -->

            <!-- Arrow Navigator -->
            <div data-u="arrowleft" class="jssora061" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <path class="a" d="M11949,1919L5964.9,7771.7c-127.9,125.5-127.9,329.1,0,454.9L11949,14079"></path>
                </svg>
            </div>
            <div data-u="arrowright" class="jssora061" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <path class="a" d="M5869,1919l5984.1,5852.7c127.9,125.5,127.9,329.1,0,454.9L5869,14079"></path>
                </svg>
            </div>
        </div>
        <script type="text/javascript">jssor_1_slider_init();</script>

    </div>
    <div class="row banner-menu">
        <div class="col-lg-3 col-md-3 col-6 banner_item_01">
            <p class="banner_item_content_01"><?php echo $home[2]->value?></p>
        </div>
        <div class="col-lg-3 col-md-3 col-6 banner_item_02">
            <p class="banner_item_content_02"><?php echo $home[3]->value?></p>
        </div>
        <div class="col-lg-3 col-md-3 col-6 banner_item_03">
            <p class="banner_item_content_03"><?php echo $home[4]->value?></p>
        </div>
        <div class="col-lg-3 col-md-3 col-6 banner_item_04">
            <p class="banner_item_content_04"><?php echo $home[5]->value?></p>
        </div>
    </div>
</div>