<style>
    #class-img-01{
        background-image: url("<?php echo site_url('assets/public/avatar/'.$class->img_01)?>");
        background-repeat: no-repeat; /* Do not repeat the image */
        background-size: cover; /* Resize the background image to cover the entire container */
    }
    #class-img-02{
        background-image: url("<?php echo site_url('assets/public/avatar/'.$class->img_02)?>");
        background-repeat: no-repeat; /* Do not repeat the image */
        background-size: cover; /* Resize the background image to cover the entire container */

    }
    
    #class-img-03{
        background-image: url("<?php echo site_url('assets/public/avatar/'.$class->img_03)?>");
        background-repeat: no-repeat; /* Do not repeat the image */
        background-size: cover; /* Resize the background image to cover the entire container */
    }
</style>
<div id="class">
    <div class="row">
        <div class="class-content col-md-12  col-lg-4">
            <div class="section_name class_session_name col-lg-12">AGATE IMAGE</div>
            <div class="class-title"><?php echo $class->title?></div>
            <div class="class-description"><?php echo $class->description?></div>
            <div class="class_btn">
                <button type="button" class="class_nextbtn" style="background:none; border:none" onclick="previouschangesize()"><img src="assets/public/avatar/classpreviousbtn.png"/></button>
                <button type="button" class="class_previousbtn" style="background:none; border:none" onclick="nextchangesize()"><img src="assets/public/avatar/classnextbtn.png"/></button>
            </div>
        </div>
        <div class="col-md-12  col-lg-8 col-12 item_img_block">
            <div id="class-img-01"></div>
            <div id="class-img-02"></div>
            <div id="class-img-03"></div>
        </div>
    </div>
    
</div>

<script>
function previouschangesize() {
    var item_01 = document.getElementById("class-img-01");
    var item_02 = document.getElementById("class-img-02");
    var item_03 = document.getElementById("class-img-03");
    var item_img_01 = document.getElementById("class-img-01_img");
    var item_img_02 = document.getElementById("class-img-02_img");
    var item_img_03 = document.getElementById("class-img-03_img");
    if(screen.width < 415){
        if(item_01.offsetWidth > 100 && item_02.offsetWidth < 100 && item_03.offsetWidth < 100){
           
            item_01.style.width = "60px";
            item_01.style.height = "57px";
            item_01.style.marginTop = "108px";

            item_02.style.width = "60px";
            item_02.style.height = "57px";
            item_02.style.marginTop = "108px";
            item_02.style.marginLeft = "5px";

            item_03.style.width = "175px";
            item_03.style.height = "166px";
            item_03.style.marginTop = "0px";
        }
        else if(item_01.offsetWidth < 100 && item_02.offsetWidth > 100 && item_03.offsetWidth < 100){
            item_01.style.width = "175px";
            item_01.style.height = "166px";
            item_01.style.marginTop = "0px";

            item_02.style.width = "60px";
            item_02.style.height = "57px";
            item_02.style.marginTop = "108px";
            item_02.style.marginLeft = "5px";

            item_03.style.width = "60px";
            item_03.style.height = "57px";
            item_03.style.marginTop = "108px";
        }else{      
            item_01.style.width = "60px";
            item_01.style.height = "57px";
            item_01.style.marginTop = "108px";

            item_02.style.width = "175px";
            item_02.style.height = "166px";
            item_02.style.marginTop = "0px";
            item_02.style.marginLeft = "5px";

            item_03.style.width = "60px";
            item_03.style.height = "57px";
            item_03.style.marginTop = "108px";    
        }
    }else if(screen.width < 969){
        if(item_01.offsetWidth > 200 && item_02.offsetWidth < 200 && item_03.offsetWidth < 200){
           item_01.style.width = "158px";
           item_01.style.height = "150px";
           item_01.style.marginTop = "215px";

           item_02.style.width = "158px";
           item_02.style.height = "150px";
           item_02.style.marginTop = "215px";
           item_02.style.marginLeft = "5px";

           item_03.style.width = "406px";
           item_03.style.height = "386px";
           item_03.style.marginTop = "-15px";
       }
       else if(item_01.offsetWidth < 200 && item_02.offsetWidth > 200 && item_03.offsetWidth < 200){
           item_01.style.width = "406px";
           item_01.style.height = "386px";
           item_01.style.marginTop = "-15px";

           item_02.style.width = "158px";
           item_02.style.height = "150px";
           item_02.style.marginTop = "215px";
           item_02.style.marginLeft = "5px";

           item_03.style.width = "158px";
           item_03.style.height = "150px";
           item_03.style.marginTop = "215px";
       }else{      
           item_01.style.width = "158px";
           item_01.style.height = "150px";
           item_01.style.marginTop = "215px";

           item_02.style.width = "406px";
           item_02.style.height = "386px";
           item_02.style.marginTop = "-15px";
           item_02.style.marginLeft = "5px";

           item_03.style.width = "158px";
           item_03.style.height = "150px";
           item_03.style.marginTop = "215px";
       }
    }
    else{
        if(item_01.offsetWidth > 300 && item_02.offsetWidth < 300 && item_03.offsetWidth < 300){
            item_01.style.width = "159px";
            item_01.style.height = "150px";
            item_01.style.marginTop = "210px";

            item_02.style.width = "159px";
            item_02.style.height = "150px";
            item_02.style.marginTop = "210px";
            item_02.style.marginLeft = "20px";

            
            item_03.style.width = "323px";
            item_03.style.height = "305px";
            item_03.style.marginLeft = "20px";
            item_03.style.marginTop = "55px";
        }
        else if(item_01.offsetWidth < 300 && item_02.offsetWidth > 300 && item_03.offsetWidth < 300){
            item_01.style.width = "305px";
            item_01.style.height = "289px";
            item_01.style.marginTop = "71px";

            item_02.style.width = "159px";
            item_02.style.height = "150px";
            item_02.style.marginTop = "210px";
            item_02.style.marginLeft = "20px";

            
            item_03.style.width = "159px";
            item_03.style.height = "150px";
            item_03.style.marginLeft = "20px";
            item_03.style.marginTop = "210px";
        }else{
            
            item_01.style.width = "159px";
            item_01.style.height = "150px";
            item_01.style.marginTop = "210px";

            item_02.style.width = "323px";
            item_02.style.height = "305px";
            item_02.style.marginTop = "55px";
            item_02.style.marginLeft = "20px";

            item_03.style.width = "159px";
            item_03.style.height = "150px";
            item_03.style.marginTop = "210px";
        }
    }
   

    
}
// List View
function nextchangesize() {
    
    var item_01 = document.getElementById("class-img-01");
    var item_02 = document.getElementById("class-img-02");
    var item_03 = document.getElementById("class-img-03");
    var item_img_01 = document.getElementById("class-img-01_img");
    var item_img_02 = document.getElementById("class-img-02_img");
    var item_img_03 = document.getElementById("class-img-03_img");
    if(screen.width < 415){
        if(item_01.offsetWidth > 100 && item_02.offsetWidth < 100 && item_03.offsetWidth < 100){
            item_01.style.width = "60px";
            item_01.style.height = "57px";
            item_01.style.marginTop = "108px";

            item_02.style.width = "175px";
            item_02.style.height = "166px";
            item_02.style.marginTop = "0px";
            item_02.style.marginLeft = "5px";

            item_03.style.width = "60px";
            item_03.style.height = "57px";
            item_03.style.marginTop = "108px";  
        }
        else if(item_01.offsetWidth < 100 && item_02.offsetWidth > 100 && item_03.offsetWidth < 100){
            item_01.style.width = "60px";
            item_01.style.height = "57px";
            item_01.style.marginTop = "108px";

            item_02.style.width = "60px";
            item_02.style.height = "57px";
            item_02.style.marginTop = "108px";
            item_02.style.marginLeft = "5px";

            item_03.style.width = "175px";
            item_03.style.height = "166px";
            item_03.style.marginTop = "0px";
        }else{      
            
            item_01.style.width = "175px";
            item_01.style.height = "166px";
            item_01.style.marginTop = "0px";

            item_02.style.width = "60px";
            item_02.style.height = "57px";
            item_02.style.marginTop = "108px";
            item_02.style.marginLeft = "5px";

            item_03.style.width = "60px";
            item_03.style.height = "57px";
            item_03.style.marginTop = "108px";
        }
    }else if(screen.width < 960){
        if(item_01.offsetWidth > 200 && item_02.offsetWidth < 200 && item_03.offsetWidth < 200){
           item_01.style.width = "158px";
           item_01.style.height = "150px";
           item_01.style.marginTop = "215px";

           item_02.style.width = "406px";
           item_02.style.height = "386px";
           item_02.style.marginTop = "-15px";
           item_02.style.marginLeft = "5px";

           item_03.style.width = "158px";
           item_03.style.height = "150px";
           item_03.style.marginTop = "215px";
        }
        else if(item_01.offsetWidth < 200 && item_02.offsetWidth > 200 && item_03.offsetWidth < 200){
           item_01.style.width = "158px";
           item_01.style.height = "150px";
           item_01.style.marginTop = "215px";

           item_02.style.width = "158px";
           item_02.style.height = "150px";
           item_02.style.marginTop = "215px";
           item_02.style.marginLeft = "5px";

           item_03.style.width = "406px";
           item_03.style.height = "386px";
           item_03.style.marginTop = "-15px";
        }else{      
            item_01.style.width = "406px";
           item_01.style.height = "386px";
           item_01.style.marginTop = "-15px";

           item_02.style.width = "158px";
           item_02.style.height = "150px";
           item_02.style.marginTop = "215px";
           item_02.style.marginLeft = "5px";

           item_03.style.width = "158px";
           item_03.style.height = "150px";
           item_03.style.marginTop = "215px";
        }
    }
    else{
        if(item_01.offsetWidth > 300 && item_02.offsetWidth < 300 && item_03.offsetWidth < 300){

            item_01.style.width = "159px";
            item_01.style.height = "150px";
            item_01.style.marginTop = "210px";

            item_02.style.width = "323px";
            item_02.style.height = "305px";
            item_02.style.marginTop = "55px";
            item_02.style.marginLeft = "20px";

            item_03.style.width = "159px";
            item_03.style.height = "150px";
        }
        else if(item_01.offsetWidth < 300 && item_02.offsetWidth > 300 && item_03.offsetWidth < 300){
            item_01.style.width = "159px";
            item_01.style.height = "150px";
            item_01.style.marginTop = "210px";

            item_02.style.width = "159px";
            item_02.style.height = "150px";
            item_02.style.marginTop = "210px";
            item_02.style.marginLeft = "20px";

            
            item_03.style.width = "323px";
            item_03.style.height = "305px";
            item_03.style.marginLeft = "20px";
            item_03.style.marginTop = "55px";
        }else{
            item_01.style.width = "305px";
            item_01.style.height = "289px";
            item_01.style.marginTop = "71px";

            item_02.style.width = "159px";
            item_02.style.height = "150px";
            item_02.style.marginTop = "210px";
            item_02.style.marginLeft = "20px";

            
            item_03.style.width = "159px";
            item_03.style.height = "150px";
            item_03.style.marginLeft = "20px";
            item_03.style.marginTop = "210px";
        
        }
    }
}


</script>
