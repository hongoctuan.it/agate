<div id="comment">
    
    <div class="col-lg-12 col-md-12 col-12 row" style="margin:0px">
        <div class="class-content col-md-6  col-lg-7 col-12">
            <div class="section_name">Agate Astronions</div>
            <div class="comment_title"><?php echo $comment_title ?></div>
        </div>
        <div class="class-description col-md-6  col-lg-4 col-12">
            <div class="class-description"><?php echo $comment_description ?></div>
        </div>
        <div class="row-fluid">
            <div>
                <div class="cover-container">
                    <!-- START PANEL -->
                    <div class="cover-item" style="background-image: url('<?php echo site_url('assets/public/avatar/comment_01.png')?>')">
                        <div class="comment-item_description"><?php echo $comment_01->description ?></div>
                        <div class="comment-item-user row">
                            <div class="comment-item-user col-md-4  col-lg-2 col-3 comment_one_icon" style="margin-left:-45px">
                                <img src="<?php echo site_url('assets/public/avatar/'.$comment_01->img)?>" class="comment-item-user-icon"/>
                            </div> 
                            <div class="col-md-8  col-lg-9 col-9 comment_one_des" style="margin-left:15px">
                                <div class="comment-item-user-description"><?php echo $comment_01->user_name ?></div>
                                <div class="comment-item-user-position">@instagram <img src="<?php echo site_url('assets/public/avatar/commentrating.png'); ?>" /></div>
                            </div> 
                        </div>
                    </div>
                    <div class="cover-item" style="background-image: url('<?php echo site_url('assets/public/avatar/comment_02.png')?>')">
                        <div class="comment-item_description" style="padding-left:40px"><?php echo $comment_02->description ?></div>
                        <div class="comment-item-user row" style="padding-left:40px">
                            <div class="comment-item-user col-md-4  col-lg-2 col-3">
                                <img src="<?php echo site_url('assets/public/avatar/'.$comment_02->img)?>" class="comment-item-user-icon"/>
                            </div> 
                            <div class="col-md-8  col-lg-9 col-9">
                                <div class="comment-item-user-description"><?php echo $comment_02->user_name ?></div>
                                <div class="comment-item-user-position">@instagram <img src="<?php echo site_url('assets/public/avatar/commentrating.png'); ?>" /></div>
                            </div> 
                        </div>
                    </div>
                    
                    <div class="cover-item" style="background-image: url('<?php echo site_url('assets/public/avatar/comment_03.png')?>')">
                        <div class="comment-item_description" style="padding-left:40px"><?php echo $comment_03->description ?></div>
                        <div class="comment-item-user row" style="padding-left:40px">
                            <div class="comment-item-user col-md-4  col-lg-2 col-3">
                                <img src="<?php echo site_url('assets/public/avatar/'.$comment_03->img)?>" class="comment-item-user-icon"/>
                            </div> 
                            <div class="col-md-8  col-lg-9 col-9">
                                <div class="comment-item-user-description"><?php echo $comment_03->user_name ?></div>
                                <div class="comment-item-user-position">@instagram <img src="<?php echo site_url('assets/public/avatar/commentrating.png'); ?>" /></div>
                            </div> 
                        </div>
                    </div>
                    <!-- END PANEL -->
                </div>
            </div>
        </div>
    </div>
    
</div>