<style>
    .high_image{
        background-image: url("<?php echo site_url("assets/public/avatar/".$home[10]->value)?>");
        background-repeat: no-repeat;
        background-size: contain; /* Resize the background image to cover the entire container */
        height: 358px;
    }    
</style>
<div id="hightlights" class="row">
     <div class="col-lg-6 col-md-12 col-12 highlights_intro">
         <div class="section_name">Giới Thiệu</div>
         <div class="hightlights_title"><?php echo $home[15]->value?></div>
         <div class="hightlights_description"><?php echo $home[16]->value?></div>
    </div>
    <div class="col-lg-2 col-md-12 col-12" style="padding:0px">
    </div>
    <div class="high_image col-lg-4 col-md-12 row" style="padding:0px">
    </div>
    <div class="high-image-logo">
        <img src="assets/public/avatar/introduce_logo.png"/>
    </div>
    <div class="high_content col-lg-10 col-md-12 row">
            <a href="<?php echo $home[41]->value?>" style="color:white !important">
            <div class="col-lg-6 col-md-12  col-12 high_item_01">
                <div class="high_item_title"><?php echo $home[11]->value?>
                    <?php if($home[45]->value=='true'):?>
                        <img src="<?php echo site_url('assets/public/avatar/upcomming.png')?>" />&nbsp;
                    <?php endif;?>
                </div>
                <div class="high_item_description">
                        <?php echo $home[28]->value?>
                    </div>
                <div style="text-align:left"><a class="high_item_url" href="<?php echo $home[41]->value?>"><?php echo $home[37]->value?></a></div>
            </div>
            </a>
            <a href="<?php echo $home[42]->value?>" style="color:white !important">
            <div class="col-lg-6 col-md-12  col-12 high_item_02">
                <div class="high_item_title" style="margin-top: 48px;"><?php echo $home[12]->value?>
                <?php if($home[46]->value=='true'):?>
                        <img src="<?php echo site_url('assets/public/avatar/upcomming.png')?>" />&nbsp;
                    <?php endif;?> 
                </div>
                <div class="high_item_description">   
                    <?php echo $home[29]->value?></div>
                <div><a class="high_item_url" href="<?php echo $home[42]->value?>"><?php echo $home[38]->value?></a></div>
            </div>
            </a>
            <a href="<?php echo $home[43]->value?>" style="color:white !important">
            <div class="col-lg-6 col-md-12  col-12 high_item_03">
                <div class="high_item_title" style="margin-top: 48px;"><?php echo $home[13]->value?><?php if($home[47]->value=='true'):?>
                            <img src="<?php echo site_url('assets/public/avatar/upcomming.png')?>" />&nbsp;
                        <?php endif;?>  
                </div>
                <div class="high_item_description">  
                    <?php echo $home[30]->value?></div>
                <div><a class="high_item_url" href="<?php echo $home[43]->value?>"><?php echo $home[39]->value?></a></div>
            </div>
            </a>
            <a href="<?php echo $home[44]->value?>" style="color:white !important">
            <div class="col-lg-6 col-md-12  col-12 high_item_04" >
                <div class="high_item_title" style="margin-top: 48px;"><?php echo $home[14]->value?>
                <?php if($home[48]->value=='true'):?>
                            <img src="<?php echo site_url('assets/public/avatar/upcomming.png')?>" />&nbsp;
                        <?php endif;?>  
                    </div>
                <div class="high_item_description">
                      
                    <?php echo $home[31]->value?></div>
                <div><a class="high_item_url" href="<?php echo $home[44]->value?>"><?php echo $home[40]->value?></a></div>
            </div>
            </a>
    </div>
</div>