<div id="partner" class="row">
    <div class="partner_title col-lg-12">Đối Tác</div>
    <div class="partner_content col-lg-12">
        <div class="table">
            <div class="row" style="justify-content: center;">
            <div class="col-6">
                <a href="https://psychub.vn/" target="_blank">
                <div class="cell">
                    <img src="assets/public/avatar/<?php echo $partner_01->img?>"/>
                </div>
                <div class="cell">
                    <div class="partner_name"><?php echo $partner_01->name?></div>
                    <div class="partner_position"><?php echo $partner_01->description?></div>
                </div>
            </div>
            <div class="col-6">
                <a href="https://menthy.vn/" target="_blank">
                <div class="cell">
                    <img src="assets/public/avatar/<?php echo $partner_02->img?>"/>
                </div>
                <div class="cell">
                    <div class="partner_name"><?php echo $partner_02->name?></div>
                    <div class="partner_position"><?php echo $partner_02->description?></div>
                </div>
                </a>
            </div>
            </div>
        </div>
    </div>
</div>