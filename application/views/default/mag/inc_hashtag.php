<div id="mag_hashtag" class="row">
    <div class="hashtag_title col-lg-8">Trending Hashtags</div>
    <div class="col-lg-3 col-4 magposts_line" style="padding: 0px">
        <img src="<?php echo site_url('assets/public/avatar/magpost_line2.png')?>" style="margin-top:24px"/>
    </div>
    <div class="hashtag_content col-lg-12">
        <?php foreach($trend_hashtag as $item):?>
            <div class="hashtag_item"><a href="<?php echo site_url('hashtag/'.$item->id); ?>"><?php echo $item->title?></a></div>
        <?php endforeach;?>
    </div>
</div>