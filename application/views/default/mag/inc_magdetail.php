
<div class="row-fluid">
    <div>
        <div class="cover-container">
            <!-- START PANEL -->
            <?php if(isset($mags[0]->slug)):?>
            <div class="cover-item" style="background-image: url('<?php echo site_url('assets/public/avatar/'.$mags[0]->banner)?>')">
              <div class="banner_content col-lg-9">
                  <a href="<?php echo site_url('agate-mag-detail/'.$mags[0]->slug)?>">
                    <div class="banner_content_title">
                      <?php echo $mags[0]->title?>
                    </div>
                    <div class="banner_content_description">
                      <?php echo $mags[0]->description?>
                    </div>
                    <div class="magposts_nextbtn"><img src="<?php echo site_url('assets/public/avatar/nextbtn.png'); ?>"/></div>
                  </a>
                  <div class="banner_content_hashtag"><?php echo date('d/m/Y',$mags[0]->time)?> - 
                    <?php if(!empty($mags[0]->hashtag)):?>
                      <?php foreach($mags[0]->hashtag as $item):?>
                        <a class="banner_content_hashtag_url" href="<?php echo site_url('hashtag/'.$item->id)?>">#<?php echo $item->title ?></a>
                      <?php endforeach;?>
                    <?php endif;?>
                  </div>
              </div>
            </div>
            <?php endif;?>;
            <?php if(isset($mags[1]->slug)):?>
            <a href="<?php echo site_url('agate-mag-detail/'.$mags[1]->slug)?>">
            <div class="cover-item" style="background-image: url('<?php echo site_url('assets/public/avatar/'.$mags[1]->banner)?>')">
              <div class="banner_content col-lg-9">
                  <div class="banner_content_title">
                    <?php echo $mags[1]->title?>
                  </div>
                  <div class="banner_content_description">
                    <?php echo $mags[1]->description?>
                  </div>
                  <div class="magposts_nextbtn"><img src="<?php echo site_url('assets/public/avatar/nextbtn.png'); ?>"/></div>
                  </a>
                  <div class="banner_content_hashtag"><?php echo date('d/m/Y',$mags[1]->time)?> - 
                    <?php if(!empty($mags[1]->hashtag)):?>
                      <?php foreach($mags[1]->hashtag as $item):?>
                        <a class="banner_content_hashtag_url" href="<?php echo site_url('hashtag/'.$item->id)?>">#<?php echo $item->title ?></a>
                      <?php endforeach;?>
                    <?php endif;?>
                  </div>
                </div>
              </div>
            <?php endif;?>
            <?php if(isset($mags[2]->slug)):?>
              <a href="<?php echo site_url('agate-mag-detail/'.$mags[2]->slug)?>">
              <div class="cover-item" style="background-image: url('<?php echo site_url('assets/public/avatar/'.$mags[2]->banner)?>')">
              <div class="banner_content col-lg-9">
                  <div class="banner_content_title">
                    <?php echo $mags[2]->title?>
                  </div>
                  <div class="banner_content_description">
                    <?php echo $mags[2]->description?>
                  </div>
                  <div class="magposts_nextbtn"><img src="<?php echo site_url('assets/public/avatar/nextbtn.png'); ?>"/></div>
                  </a>
                  <div class="banner_content_hashtag"><?php echo date('d/m/Y',$mags[2]->time)?> - 
                    <?php if(!empty($mags[2]->hashtag)):?>
                      <?php foreach($mags[2]->hashtag as $item):?>
                        <a class="banner_content_hashtag_url" href="<?php echo site_url('hashtag/'.$item->id)?>">#<?php echo $item->title ?></a>
                      <?php endforeach;?>
                    <?php endif;?>
                  </div>
                </div>
              </div>
            <?php endif;?>
            <!-- END PANEL -->
        </div>
    </div>
</div>
<div class="magline col-lg-12" >
  <img src="<?php echo site_url('assets/public/avatar/magline.png')?>"/>
</div>