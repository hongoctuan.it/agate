<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="col-lg-12 row" id="magposts">
  <div class="col-lg-5 col-md-4 col-6 magposts_block_title">
    Bài viết mới nhất
  </div>
  <div class="col-lg-4 col-md-3 col-1 magposts_line">
    <img src="<?php echo site_url('assets/public/avatar/magpost_line.png')?>"/>
  </div>
  <div class="col-lg-3 col-md-2 col-4 magpost_all" style="text-align:right">
    Xem tất cả <img src="<?php echo site_url('assets/public/avatar/arrow.png'); ?>"/>
  </div>
  <div class="col-lg-12 col-md-3 col-7" style="text-align:right">
  <button class="btn active listbtn" onclick="listView()"><img src="<?php echo site_url('assets/public/avatar/list.png')?>" width="32px" /></button> 
  <button class="btn girdbtn" onclick="gridView()"><img src="<?php echo site_url('assets/public/avatar/gird.png')?>" width="32px"/></button>
  </div>
</div>
<br>
<?php if(count($mags) > 3):?>
    <?php for($i = 3 ; $i< count($mags);$i++):?>
      <?php if($i%3==0):?>
          <div class="col-12 col-lg-12 col-md-12">
            <div class="column row">
              <a class="col-lg-5 mag_post_img" href="<?php echo site_url('agate-mag-detail/'.$mags[$i]->slug)?>">
                <div class="col-lg-12 mag_post_bg">
                  <img class="mag_post_bg_img" src="<?php echo site_url('assets/public/avatar/'.$mags[$i]->thumbnail)?>" width="100%"/>
                </div>
              </a>
              <div class="col-lg-7">
                <a href="<?php echo site_url('agate-mag-detail/'.$mags[$i]->slug)?>">
                <div class="magposts_title"><?php echo $mags[$i]->title ?></div>
                </a>
                <div class="magposts_description"><?php echo $mags[$i]->description ?></div>
                <a href="<?php echo site_url('agate-mag-detail/'.$mags[$i]->slug)?>">
                <div class="magposts_nextbtn"><img class="magposts_nextbtn_img" src="<?php echo site_url('assets/public/avatar/nextbtn.png'); ?>"/></div>
                </a>
                <div class="magposts_hashtag"><?php echo date('d/m/Y',$mags[$i]->time)?> - 
                  <?php if(!empty($mags[$i]->hashtag)):?>
                    <?php foreach($mags[$i]->hashtag as $item):?>
                     <a href="<?php echo site_url('hashtag/'.$item->id)?>">#<?php echo $item->title ?></a>
                    <?php endforeach;?>
                  <?php endif;?>
                </div>
              </div>
          </div>
          <?php if(count($mags)<=4) echo "</div>"; ?>
      <?php endif;?>
      <?php if($i%3==1):?>
            <div class="column row" id="<?php echo ($i==(floor(count($mags)/10)*10))?"viewmore":""?>">
              <a class="col-lg-5 mag_post_img" href="<?php echo site_url('agate-mag-detail/'.$mags[$i]->slug)?>">
                <div class="col-lg-12 mag_post_bg">
                  <img class="mag_post_bg_img" src="<?php echo site_url('assets/public/avatar/'.$mags[$i]->thumbnail)?>" width="100%"/>
                </div>
              </a>
              <div class="col-lg-7">
              <a href="<?php echo site_url('agate-mag-detail/'.$mags[$i]->slug)?>">
                <div class="magposts_title"><?php echo $mags[$i]->title ?></div>
      </a>
                <div class="magposts_description"><?php echo $mags[$i]->description ?></div>
                <a href="<?php echo site_url('agate-mag-detail/'.$mags[$i]->slug)?>">
                <div class="magposts_nextbtn"><img class="magposts_nextbtn_img" src="<?php echo site_url('assets/public/avatar/nextbtn.png'); ?>"/></div>
      </a>
                <div class="magposts_hashtag"><?php echo date('d/m/Y',$mags[$i]->time)?> - 
                  <?php if(!empty($mags[$i]->hashtag)):?>
                    <?php foreach($mags[$i]->hashtag as $item):?>
                      <a href="<?php echo site_url('hashtag/'.$item->id)?>">#<?php echo $item->title ?></a>
                    <?php endforeach;?>
                  <?php endif;?>
                </div>
              </div>
          </div>
          <?php if(count($mags)<=5) echo "</div>"; ?>
      <?php endif;?>
      <?php if($i%3==2):?>
        <div class="column row">
          <a class="col-lg-5 mag_post_img" href="<?php echo site_url('agate-mag-detail/'.$mags[$i]->slug)?>">
            <div class="col-lg-12 mag_post_bg">
              <img class="mag_post_bg_img" src="<?php echo site_url('assets/public/avatar/'.$mags[$i]->thumbnail)?>" width="100%"/>
            </div>
          </a>
          <div class="col-lg-7">
            <a href="<?php echo site_url('agate-mag-detail/'.$mags[$i]->slug)?>">
            <div class="magposts_title"><?php echo $mags[$i]->title ?></div>
      </a>
            <div class="magposts_description"><?php echo $mags[$i]->description ?></div>
            <a href="<?php echo site_url('agate-mag-detail/'.$mags[$i]->slug)?>">
            <div class="magposts_nextbtn"><img class="magposts_nextbtn_img" src="<?php echo site_url('assets/public/avatar/nextbtn.png'); ?>"/></div>
      </a>
            <div class="magposts_hashtag"><?php echo date('d/m/Y',$mags[$i]->time)?> - 
              <?php if(!empty($mags[$i]->hashtag)):?>
                <?php foreach($mags[$i]->hashtag as $item):?>
                  <a href="<?php echo site_url('hashtag/'.$item->id)?>">#<?php echo $item->title ?></a>
                <?php endforeach;?>
              <?php endif;?>
            </div>
          </div>
         
        </div>
      </div>
      <?php endif;?>
    <?php endfor;?>
<?php endif;?>
<?php if($more==1): ?>
  <div class="viewmore">
    <?php if($type=="mom"):?>
      <a href="<?php echo site_url('agate-mag/mom?page='.$page.'#viewmore')?>">
        <img src="<?php echo site_url('assets/public/avatar/viewmore.png'); ?>"/>
      </a>
    <?php elseif($type=="teen"):?>
      <a href="<?php echo site_url('agate-mag/teen?page='.$page.'#viewmore')?>">
        <img src="<?php echo site_url('assets/public/avatar/viewmore.png'); ?>"/>
      </a>
    <?php endif;?>
  </div>
<?php endif?>
</div>

<script>

$(document).ready(function(){
    var element = document.getElementById("course_form");
    var course_description_pc = document.getElementById("course_description_pc");
    var course_description_mb = document.getElementById("course_description_mb");
    if(screen.width < 769){
        course_description_pc.style.display = "none";
        element.style.backgroundImage = "url('<?php echo site_url('assets/public/avatar/register_bg_mb.png')?>')";
    }else{
        course_description_mb.style.display = "none";
        element.style.backgroundImage = "url('<?php echo site_url('assets/public/avatar/register_bg.png')?>')";
    }
});
function tracking() {
    $.ajax({
      url: '<?php echo site_url('tracking?act='.$this->uri->segment(1))?>', // gửi đến file upload.php 
      dataType: 'text',
      type: 'post',
  });
}

// Get the elements with class="column"
var elements = document.getElementsByClassName("column");
var magposts_title = document.getElementsByClassName("magposts_title");
var magposts_hashtag = document.getElementsByClassName("magposts_hashtag");
var magposts_nextbtn_img = document.getElementsByClassName("magposts_nextbtn_img");
var magposts_description = document.getElementsByClassName("magposts_description");
var mag_post_bg_img = document.getElementsByClassName("mag_post_bg_img");
// Declare a loop variable

var i;

// List View
function listView() {
  for (i = 0; i < elements.length; i++) {
    elements[i].style.width = "100%";
    elements[i].style.padding = "0px";
    elements[i].childNodes[1].classList.remove("col-lg-12"); 
    elements[i].childNodes[3].classList.remove("col-lg-12"); 
    elements[i].style.padding = "0px";
    elements[i].style.margin = "0px";
    elements[i].style.marginBottom = "25px";
    elements[i].style.paddingBottom = "30px";
    elements[i].style.borderTopLeftRadius = "24px";
    elements[i].style.borderTopRightRadius = "24px";
  }
  for (i = 0; i < magposts_title.length; i++) {
    magposts_title[i].style.fontSize = "24px";
    magposts_title[i].style.paddingTop = "40px";
    magposts_title[i].style.lineHeight = "revert";
  }
  for (i = 0; i < magposts_hashtag.length; i++) {
    magposts_hashtag[i].style.top = "-40px";
  }
  for (i = 0; i < magposts_nextbtn_img.length; i++) {
    magposts_nextbtn_img[i].style.width = "40px";
    magposts_nextbtn_img[i].style.top = "-21px";
  }
  for (i = 0; i < magposts_description.length; i++) {
    magposts_description[i].style.fontSize = "14px";
  }
  for (i = 0; i < mag_post_bg_img.length; i++) {
    mag_post_bg_img[i].style.paddingTop = "30px";
  }
}

// Grid View
function gridView() {
  for (i = 0; i < elements.length; i++) {
    elements[i].style.width = "30%";
    elements[i].style.padding = "0px";
    // elements[i].style.margin = "11px";
    elements[i].style.setProperty("margin", "10px", "important");
    elements[i].childNodes[1].classList.add("col-lg-12"); 
    elements[i].childNodes[3].classList.add("col-lg-12"); 
    elements[i].style.float = "left";
    elements[i].style.borderTopLeftRadius = "150px";
    elements[i].style.borderTopRightRadius = "150px";
  }
  for (i = 0; i < magposts_title.length; i++) {
    magposts_title[i].style.fontSize = "15px";
    magposts_title[i].style.paddingTop = "10px";
    magposts_title[i].style.lineHeight = "revert";
  }
  for (i = 0; i < magposts_hashtag.length; i++) {
    magposts_hashtag[i].style.top = "-16px";
  }
  for (i = 0; i < magposts_nextbtn_img.length; i++) {
    magposts_nextbtn_img[i].style.width = "31px";
    magposts_nextbtn_img[i].style.top = "-17px";
  }
  for (i = 0; i < magposts_description.length; i++) {
    magposts_description[i].style.fontSize = "12px";
  }
  for (i = 0; i < mag_post_bg_img.length; i++) {
    mag_post_bg_img[i].style.paddingTop = "15px";
  }
}
</script>