<div id="savepost" class="row col-lg-12" style="padding:0px">
    <div class="hashtag_title col-lg-8">Bài viết đã lưu</div>
    <div class="col-lg-3 col-4 magposts_line" style="padding:0px">
        <img src="<?php echo site_url('assets/public/avatar/magpost_line2.png')?>" style="margin-top:24px"/>
    </div>
    <?php if(count($savemags)>0):?>
        <?php foreach($savemags as $item):?>
                <div class="row" style="padding-top:20px;">
                    <div class="col-lg-4" style="padding-left:0px">
                        <a href="<?php echo site_url('agate-mag-detail/'.$item->slug)?>">
                            <div class="col-lg-12 mag_post_bg">
                                <img src="<?php echo site_url('assets/public/avatar/'.$item->thumbnail)?>" width="200%"/>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-8" style="padding-left:0px">
                    </div>
                    <div class="col-lg-12" style="padding-left:0px">
                        <div class="mag_savepost_hashtag"><?php echo date('d/m/Y',$item->time)?> - 
                        <?php if(!empty($item->hashtag)):?>
                            <?php foreach($item->hashtag as $items):?>
                                <a href="<?php echo site_url('hashtag/'.$items->id)?>">#<?php echo $items->title ?></a>
                            <?php endforeach;?>
                        <?php endif;?>
                        </div>
                        <a href="<?php echo site_url('agate-mag-detail/'.$item->slug)?>">

                        <div class="mag_savepost_title"><?php echo $item->title ?></div>
                            </a>
                    </div>
                    
                </div>
        <?php endforeach;?>
        <?php if($type=='mom'):?>
            <a href="<?php echo site_url('agate-mag/mom')?>" class="banner_order savepost_viewall" style="background: #30A0E0 !important;">Xem tất cả</a>
        <?php else:?>
            <a href="<?php echo site_url('agate-mag/teen')?>" class="banner_order savepost_viewall" style="background: #30A0E0 !important;">Xem tất cả</a>
        <?php endif;?>
    <?php endif;?>
   

    
</div>