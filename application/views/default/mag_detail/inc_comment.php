<script>
    $(document).ready(function(){
        $("#comment_submit").click(function(){
            var login = $(this).attr('data-id')
            var comment_content = $( "#comment_content" ).val()
            if(login == 0 || comment_content == ""){
                alert("Bạn chưa đăng nhập hoặc chưa nhập nội dung bình luận.")
                return false;
            }else{
                return true;
            }
            
        });
    });
</script>
<div id="comment" class="col-lg-12">
    <div class="hashtag_title col-lg-12">
        <div class="row">
            <div class="col-lg-7 col-md-7 col-12 comment_title">Bạn thấy bài viết như thế nào</div>
            <div class="col-lg-5 col-md-5 col-12" style="text-align:right">
                    <img onclick="rating(1)" id="rating01" src="<?php echo site_url('assets/public/avatar/starempty.png'); ?>" />
                    <img onclick="rating(2)" id="rating02" src="<?php echo site_url('assets/public/avatar/starempty.png'); ?>" />
                    <img onclick="rating(3)" id="rating03" src="<?php echo site_url('assets/public/avatar/starempty.png'); ?>" />
                    <img onclick="rating(4)" id="rating04" src="<?php echo site_url('assets/public/avatar/starempty.png'); ?>" />
                    <img onclick="rating(5)" id="rating05" src="<?php echo site_url('assets/public/avatar/starempty.png'); ?>" />
            </div>
            <div class="col-lg-12 col-md-12 col-12" style="margin-bottom:30px">
                <?php if(!empty($mag->hashtag)):?>
                    <?php foreach($mag->hashtag as $item):?>
                        <span class="hashtag_item"><a href="<?php echo site_url('hashtag/'.$item->id)?>">#<?php echo $item->title ?></a></span>
                    <?php endforeach;?>
                <?php endif;?>
            </div>
            <div class="col-lg-12 col-md-12 col-12">
                <button onclick="sendrating(<?php echo $mag->id?>)" class="btn banner_order magdetail_sendrating" style="background: #30A0E0 !important;padding-top: 7px !important;padding-bottom: 7px !important;">
                    Gửi Đánh Giá
                </button>
            </div>
            <div class="col-lg-12 col-md-12 col-12">
            <?php 
                $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' ? 'https' : 'http';
                $full_url = $protocol."://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            ?>
                <div class="share_title">Chia Sẻ Bài Viết</div>
                <div>
                   
                    <a class="share_btn" href="https://facebook.com/sharer/sharer.php?u=http://agate.vn/agate-mag/1" target="_blank" rel="nofollow" class="fa fa-facebook"><img src="<?php echo site_url('assets/public/avatar/fb.png'); ?>" width="150px"/></a>
                    <a class="share_btn" href="https://www.instagram.com/create/select/"><img src="<?php echo site_url('assets/public/avatar/itg.png'); ?>" width="150px"/></a>
                    <img class="share_btn" onclick="copyurl()" src="<?php echo site_url('assets/public/avatar/copy.png'); ?>" width="150px"/>
                </div>
            </div>
        </div>
        
    </div>
</div>
<script>
    let temp=0
    function rating(star){
        temp = star
        if(star == 1){
            $('#rating01').attr('src','<?php echo site_url('assets/public/avatar/starfull.png'); ?>');
            $('#rating02').attr('src','<?php echo site_url('assets/public/avatar/starempty.png'); ?>');
            $('#rating03').attr('src','<?php echo site_url('assets/public/avatar/starempty.png'); ?>');
            $('#rating04').attr('src','<?php echo site_url('assets/public/avatar/starempty.png'); ?>');
            $('#rating05').attr('src','<?php echo site_url('assets/public/avatar/starempty.png'); ?>');
        }else if(star == 2){
            $('#rating01').attr('src','<?php echo site_url('assets/public/avatar/starfull.png'); ?>');
            $('#rating02').attr('src','<?php echo site_url('assets/public/avatar/starfull.png'); ?>');
            $('#rating03').attr('src','<?php echo site_url('assets/public/avatar/starempty.png'); ?>');
            $('#rating04').attr('src','<?php echo site_url('assets/public/avatar/starempty.png'); ?>');
            $('#rating05').attr('src','<?php echo site_url('assets/public/avatar/starempty.png'); ?>');
        }else if(star == 3){
            $('#rating01').attr('src','<?php echo site_url('assets/public/avatar/starfull.png'); ?>');
            $('#rating02').attr('src','<?php echo site_url('assets/public/avatar/starfull.png'); ?>');
            $('#rating03').attr('src','<?php echo site_url('assets/public/avatar/starfull.png'); ?>');
            $('#rating04').attr('src','<?php echo site_url('assets/public/avatar/starempty.png'); ?>');
            $('#rating05').attr('src','<?php echo site_url('assets/public/avatar/starempty.png'); ?>');
        }else if(star == 4){
            $('#rating01').attr('src','<?php echo site_url('assets/public/avatar/starfull.png'); ?>');
            $('#rating02').attr('src','<?php echo site_url('assets/public/avatar/starfull.png'); ?>');
            $('#rating03').attr('src','<?php echo site_url('assets/public/avatar/starfull.png'); ?>');
            $('#rating04').attr('src','<?php echo site_url('assets/public/avatar/starfull.png'); ?>');
            $('#rating05').attr('src','<?php echo site_url('assets/public/avatar/starempty.png'); ?>');
        }else{
            $('#rating01').attr('src','<?php echo site_url('assets/public/avatar/starfull.png'); ?>');
            $('#rating02').attr('src','<?php echo site_url('assets/public/avatar/starfull.png'); ?>');
            $('#rating03').attr('src','<?php echo site_url('assets/public/avatar/starfull.png'); ?>');
            $('#rating04').attr('src','<?php echo site_url('assets/public/avatar/starfull.png'); ?>');
            $('#rating05').attr('src','<?php echo site_url('assets/public/avatar/starfull.png'); ?>');
        }
    }
    function sendrating(magid){
        if(temp==0){
            alert("Vui lòng chọn sao!")
        }else{
            _data = {'mag_id':magid, 'star': temp}
            $.ajax({
                url: '<?php echo site_url('agate-mag/rating')?>',
                dataType: 'text',
                data: _data,
                type: 'post',
                success: function (res) {
                    $('#exampleModal').modal('toggle');
                    $('#exampleModal').modal('show');
                    $('#exampleModal').modal('hide');
                }
            });
        }
    }
    function copyurl(){
        var $temp = $("<input>");
        var $url = $(location).attr('href');
        $("body").append();
        $temp.val($url).select();
        document.execCommand("copy");
        $temp.remove();
        alert("Đã sao chép link!");
    }
</script>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <!-- <button type="button"  aria-label="Close"> -->
          <img class="close closerating" data-dismiss="modal" src="<?php echo site_url('assets/public/avatar/closebtn.png'); ?>" />
        <!-- </button> -->
      </div>
      <div class="modal-body">
      <a href="https://facebook.com/sharer/sharer.php?u=http://agate.vn/agate-mag/1" target="_blank" rel="nofollow" class="fa fa-facebook"><button type="button" class="btn banner_order">Chia sẻ bài viết</button></a>
      </div>
    </div>
  </div>
</div>