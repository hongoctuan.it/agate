<div id="banner" class="col-lg-12">
    <div class="hashtag_content col-lg-12">
        <?php if(!empty($mag->hashtag)):?>
            <?php foreach($mag->hashtag as $item):?>
                <span class="hashtag_item"><a href="<?php echo site_url('hashtag/'.$item->id)?>">#<?php echo $item->title?></a></span>
            <?php endforeach;?>
        <?php endif;?>
    </div>
</div>