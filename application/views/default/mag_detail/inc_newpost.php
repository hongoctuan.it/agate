<script>
    $(document).ready(function(){
        $("#comment_submit").click(function(){
            var login = $(this).attr('data-id')
            var comment_content = $( "#comment_content" ).val()
            if(login == 0 || comment_content == ""){
                alert("Bạn chưa đăng nhập hoặc chưa nhập nội dung bình luận.")
                return false;
            }else{
                return true;
            }
            
        });
    });
</script>
<div id="newpost" class="col-lg-12">
    <div class="row" style="margin-bottom:30px">
        <div class="col-lg-6 col-md-6 col-12 newpost_title">Bài viết mới nhất</div>
        <div class="col-lg-3 col-md-1 col-5 magdetail_line">
            <img src="<?php echo site_url('assets/public/avatar/magpost_line.png')?>"/>
        </div>
        <div class="col-lg-2 col-md-3 col-12" style="text-align:right; line-height:50px">
            <?php if($mag->type==1):?>
                <div class="magpost_all" style="text-align:right">
                    <a href="<?php echo site_url("agate-mag-detail/teen");?>">
                        Xem tất cả <img src="<?php echo site_url('assets/public/avatar/arrow.png'); ?>"/>
                    </a>
                </div>
            <?php else:?>
                <div class="magpost_all" style="text-align:right">
                    <a href="<?php echo site_url("agate-mag-detail/mom");?>">
                        Xem tất cả <img src="<?php echo site_url('assets/public/avatar/arrow.png'); ?>"/>
                    </a>
                </div>            
            <?php endif;?>
        </div>
    </div>
    <?php foreach($recommand as $item):?>
        <div class="row newpost_tiem">
            <a class="col-lg-4 magdetail_post_img" href="<?php echo site_url('agate-mag/'.$item->slug)?>">
                <div class="col-lg-12 magdetail_post_bg">
                <img src="<?php echo site_url('assets/public/avatar/'.$item->thumbnail)?>" width="100%"/>
                </div>
            </a>
            <div class="col-lg-8">
                <a href="<?php echo site_url('agate-mag-detail/'.$item->slug)?>">
                    <div class="magposts_title"><?php echo $item->title ?></div>
                    <div class="magposts_description"><?php echo $item->description ?></div>
                    <div class="magposts_nextbtn"><img src="<?php echo site_url('assets/public/avatar/nextbtn.png'); ?>"/></div>
                </a>
                <div class="magposts_hashtag"><?php echo date('d/m/Y',1636469333)?> - 
                    <?php foreach($item->hashtag as $item):?>
                        <a href="<?php echo site_url('hashtag/'.$item->id)?>">#<?php echo $item->title?></a>
                    <?php endforeach;?>
                </div>
            </div>
            </a>
        </div>
    <?php endforeach;?>
</div>