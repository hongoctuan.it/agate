<div id="banner">
    <div class="banner_title">Đối Tác Chuyên Môn</div>
        <div id="partner" class="row">
                <div class=" col-lg-12 col-md-12 col-12 row">
                    <div class="col-lg-6 col-md-6 col-6" style="text-align: center;">
                        <a href="https://menthy.vn/" target="_blank">
                        <img src="assets/public/avatar/<?php echo $partner_01->img?>"/>
                        <div class="partner_name"><?php echo $partner_01->name?></div>
                        <div class="partner_position"><?php echo $partner_01->description?></div>
                        </a>
                    </div>
                    <div class="col-lg-6 col-md-6 col-6" style="text-align: center;">
                        <a href="https://psychub.vn/" target="_blank">
                        <img src="assets/public/avatar/<?php echo $partner_02->img?>"/>
                        <div class="partner_name"><?php echo $partner_02->name?></div>
                        <div class="partner_position"><?php echo $partner_02->description?>
                        </a>
                    </div>
                </div>
        </div>
    </div>
</div>