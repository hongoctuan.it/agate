<div id="founder" class="row">
    <div class="session_name col-lg-12">AGATE</div>

    <div class="founder_title col-lg-12">Đội Ngũ Sáng Lập</div>
    <?php foreach($founders as $item):?>
        <div class="founder_content col-lg-4 col-md-6 col-6">
            <div class="founder_item">
                <img src="<?php echo site_url('assets/public/avatar/'.$item->thumbnail)?>" width="100%"/>
                <div class="founder_item_title"><?php echo $item->full_name?></div>
                <div class="founder_position"><?php echo $item->position?></div>
                <div class="founder_description"><?php echo $item->position_description?></div>
            </div>
        </div>
    <?php endforeach;?>
</div>