<script>
    $(document).ready(function(){
        if(screen.width > 769){
            $("#staff_img_01").attr("src","<?php echo site_url("assets/public/avatar/".$staff[3]->value)?>");
            $("#staff_img_02").attr("src","<?php echo site_url("assets/public/avatar/".$staff[5]->value)?>");
            $("#staff_img_03").attr("src","<?php echo site_url("assets/public/avatar/".$staff[4]->value)?>");
        }else{
            $("#staff_img_01").attr("src","<?php echo site_url("assets/public/avatar/".$staff[6]->value)?>");
            $("#staff_img_02").attr("src","<?php echo site_url("assets/public/avatar/".$staff[8]->value)?>");
            $("#staff_img_03").attr("src","<?php echo site_url("assets/public/avatar/".$staff[7]->value)?>");
        }
    });
</script>
<div id="staff" class="row">
    <div class="session_name col-lg-12">AGATE</div>
    <div class="staff_title col-lg-12"><?php echo $staff[2]->value?></div>
    <!-- <div class="staff_description col-lg-12"><?php echo $staff[3]->value?></div> -->
            <div class="staff-item-left col-lg-12 col-md-12 col-12 row">
                <div class="col-lg-4 col-md-4 col-6 staff_item_img_0">
                    <img id="staff_img_01" src="assets/public/avatar/staff1.png" width="100%" />
                </div>
                <div class="col-lg-8 col-md-8 col-6 staff-item-content-left">
                    <div class="col-lg-12 staff_title_block" style="padding:0px">
                        <div class="col-lg-6" style="padding:0px">
                            <div class="staff_item_name"><?php echo $staffs[0]->full_name?></div>
                            <div class="staff_item_position"><?php echo $staffs[0]->position?></div>
                        </div>
                        <div class="col-lg-9 staff_title_block" style="padding:0px">
                            <div class="staff_item_position_description"><?php echo $staffs[0]->position_description?></div>
                        </div>
                    </div>
                    <div class="col-lg-12 staff_itme_line" style="border-bottom: solid 1px #FFB308;"></div>
                    <div class="col-lg-12" style="text-align:left; padding:0px">
                        <div class="staff_item_description_left staff_line_01" style="text-align:left"><?php echo $staffs[0]->description?></div>
                    </div>
                </div>
            </div>
            <div class="staff-item-right col-lg-12 col-md-12 col-12 row">
                <div class="col-lg-8 col-md-8 col-6 staff-item-content-right clearpadding">
                    <div class="col-lg-12 staff_item_right_margin">
                        <div class="col-lg-9" style="padding-right:23px">
                            <div class="staff_item_name"><?php echo $staffs[1]->full_name?></div>
                            <div class="staff_item_position"><?php echo $staffs[1]->position?></div>
                        </div>
                        <div class="col-lg-9" style="padding:0px">
                            <div class="staff_item_position_description"><?php echo $staffs[1]->position_description?></div>
                        </div>
                    </div>
                    <div class="col-lg-12 staff_itme_line" style="border-bottom: solid 1px #30A0E0;"></div>
                    <div class="col-lg-12" style="padding:0px">
                        <div class="staff_item_description_right staff_line_02" style="text-align:right"><?php echo $staffs[1]->description?></div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-6 staff_item_img_02">
                    <img id="staff_img_02" src="assets/public/avatar/staff2.png"/>
                </div>
            </div>
            <div class="staff-item-left col-lg-12 col-md-12 col-12 row">
                <div class="col-lg-4 col-md-4 col-6 staff_item_img_03">
                    <img id="staff_img_03" src="assets/public/avatar/staff3.png"/>
                </div>
                <div class="col-lg-8 col-md-8 col-6 staff-item-content-left">
                    <div class="col-lg-12 staff_title_block" style="padding:0px">
                        <div class="col-lg-6" style="padding:0px">
                            <div class="staff_item_name"><?php echo $staffs[2]->full_name?></div>
                            <div class="staff_item_position"><?php echo $staffs[2]->position?></div>
                        </div>
                        <div class="col-lg-9 " style="padding:0px">
                            <div class="staff_item_position_description"><?php echo $staffs[2]->position_description?></div>
                        </div>
                    </div>
                    <div class="col-lg-12 staff_itme_line" style="border-bottom: solid 1px #BB521F;"></div>
                    <div class="col-lg-12" style="text-align:left; padding:0px">
                        <div class="staff_item_description staff_item_description_left staff_line_03" style="text-align:left"><?php echo $staffs[2]->description?></div>
                    </div>
                </div>
            </div>
    
   
</div>