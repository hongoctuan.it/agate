<div id="teacher" class="row">
    <div class="session_name col-lg-12">AGATE</div>
    <div class="teacher_title col-lg-12">Đội ngũ sáng lập</div>
    <div class="teacher_item col-lg-4 col-md-4">
        <img src="assets/public/avatar/<?php echo $teacher_01->img?>"/>
        <div class="teacher_item_name"><?php echo $teacher_01->full_name?></div>
        <div class="teacher_item_position"><?php echo $teacher_01->position?></div>
    </div>
    <div class="teacher_item col-lg-4 col-md-4">
        <img src="assets/public/avatar/<?php echo $teacher_02->img?>"/>
        <div class="teacher_item_name"><?php echo $teacher_02->full_name?></div>
        <div class="teacher_item_position"><?php echo $teacher_02->position?></div>
    </div>
    <div class="teacher_item col-lg-4 col-md-4">
        <img src="assets/public/avatar/<?php echo $teacher_03->img?>"/>
        <div class="teacher_item_name"><?php echo $teacher_03->full_name?></div>
        <div class="teacher_item_position"><?php echo $teacher_03->position?></div>
    </div>
</div>