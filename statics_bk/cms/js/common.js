function preview_image(event,image) 
{
  var reader = new FileReader();
  reader.onload = function()
  {
    var output = document.getElementById(image);
    output.src = reader.result;
  }
  reader.readAsDataURL(event.target.files[0]);
}
